var decision=true;

//captcha generator function

function captcha(){
    var first= Math.ceil(Math.random() * 10) + 5;
    var second= Math.ceil(Math.random() * 5);
    if(first%second == 0){
        var operator= Math.ceil(Math.random() * 4) ;
    }
    else{
        var operator= Math.ceil(Math.random() * 3) ;
    }
    switch(operator){
        case 1:
        document.getElementsByClassName("captchasideheading")[0].innerHTML= first + "  +  " + second  + "  =  ";
        cap = first + second;
        break;
        case 2:
        document.getElementsByClassName("captchasideheading")[0].innerHTML= first + "  -  " + second + "  =  ";
        cap = first - second;
        break;
        case 3:
        document.getElementsByClassName("captchasideheading")[0].innerHTML= first + "  *  " + second + "   =  ";
        cap = first * second;
        break;
        case 4:
        document.getElementsByClassName("captchasideheading")[0].innerHTML= first + "  /  " + second + "  =  ";
        cap = Math.floor(first/second);
    }
};

//hide alert 

function hide(tempname){
    if(tempname == "firstname"){
        document.getElementsByClassName("namealerticon")[0].style.visibility= "hidden";
    }
    if(tempname == "lastname"){
        document.getElementsByClassName("namealerticon")[1].style.visibility= "hidden";
        }
    if(tempname == "email"){
        document.getElementsByClassName("alerticon")[0].style.visibility= "hidden";
    }
    if(tempname == "password"){
        document.getElementsByClassName("alerticon")[1].style.visibility= "hidden";
    }
    if(tempname == "re-enter password"){
        document.getElementsByClassName("alerticon")[2].style.visibility= "hidden";
    }
    if(tempname == "gender"){
        document.getElementsByClassName("alerticon")[3].style.visibility= "hidden";
    }
    if(tempname == "phone1"){
        document.getElementsByClassName("alerticon")[4].style.visibility= "hidden";
    }
    if(tempname == "phone2"){
        document.getElementsByClassName("alerticon")[5].style.visibility= "hidden";
    }
    if(tempname == "current address"){
        document.getElementsByClassName("alerticon")[6].style.visibility= "hidden";
    }
    if(tempname == "permanent address"){
        document.getElementsByClassName("alerticon")[7].style.visibility= "hidden";
    }
    if(tempname == "state"){
        document.getElementsByClassName("alerticon")[8].style.visibility= "hidden";
    }
    if(tempname == "country"){
        document.getElementsByClassName("alerticon")[9].style.visibility= "hidden";
    }
    if(tempname == "captcharesult"){
        document.getElementsByClassName("captchaalerticon")[0].style.visibility= "hidden";
    }
}

//validate name function

function validatename(tempname){
    if(tempname == "firstname"){
        tempvalue = document.forms["registerform"][tempname].value;
        var nameformat = /^[a-z|A-Z]+[" "]?[a-z|A-Z]*$/;
        if (nameformat.test(tempvalue) == false){
            document.getElementsByClassName("namealerticon")[0].style.visibility= "visible";
            document.forms["registerform"][tempname].style.borderColor="rgb(255,99,71)";
            return false;
        }
        else if(nameformat.test(tempvalue) == true)
        {
            document.getElementsByClassName("namealerticon")[0].style.visibility= "hidden";
            document.forms["registerform"][tempname].style.borderColor="rgb(204, 204, 204)";
            return true;
        }
    }
    if(tempname == "lastname"){
        tempvalue = document.forms["registerform"][tempname].value;
        var nameformat = /^[a-z|A-Z]+$/;
        if (nameformat.test(tempvalue) == false){
            document.getElementsByClassName("namealerticon")[1].style.visibility= "visible";
            document.forms["registerform"][tempname].style.borderColor="rgb(255,99,71)";
            return false;
        }
        else if(nameformat.test(tempvalue) == true)
        {
            document.getElementsByClassName("namealerticon")[1].style.visibility= "hidden";
            document.forms["registerform"][tempname].style.borderColor="rgb(204, 204, 204)";
            return true;
        }
        }
}

//validate email function

function validateemail(){
    var tempvalue = document.forms["registerform"]["email"].value;
    var emailformat = /^\w+([\.+-]?\w+)*@(\w)+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(emailformat.test(tempvalue) == false){
        document.getElementsByClassName("alerticon")[0].style.visibility= "visible";
        document.forms["registerform"]["email"].style.borderColor="rgb(255,99,71)";
        return false;
    }
    else if(emailformat.test(tempvalue) == true){
        document.getElementsByClassName("alerticon")[0].style.visibility= "hidden";
        document.forms["registerform"]["email"].style.borderColor="rgb(204,204,204)";
        return true;
    }
}

//validate password function

function validatepassword(){
    var tempvalue = document.forms["registerform"]["password"].value;
    var passwordformat = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/;
    if(passwordformat.test(tempvalue) == false){
        document.getElementsByClassName("alerticon")[1].style.visibility= "visible";
        document.forms["registerform"]["password"].style.borderColor="rgb(255,99,71)";
        return false;
    }
    else if(passwordformat.test(tempvalue) == true){
        document.getElementsByClassName("alerticon")[1].style.visibility= "hidden";
        document.forms["registerform"]["password"].style.borderColor="rgb(204,204,204)";
        return true;
    }
}

//validate re-enter password function

function validatereenterpassword(){
    var tempvalue1 = document.forms["registerform"]["re-enter password"].value;
    var tempvalue2 = document.forms["registerform"]["password"].value;
    if(tempvalue1 != tempvalue2 || tempvalue1 == "")
    {
        document.getElementsByClassName("alerticon")[2].style.visibility= "visible";
        document.forms["registerform"]["re-enter password"].style.borderColor="rgb(255,99,71)";
        return false;
    }
    else if(tempvalue1 == tempvalue2)
    {
        document.getElementsByClassName("alerticon")[2].style.visibility= "hidden";
        document.forms["registerform"]["re-enter password"].style.borderColor="rgb(204,204,204)";
        return true;
    }
}

//validate gender function

function validategender(){
    var tempvalue = document.forms["registerform"]["gender"].value;
    if (tempvalue == "") {
        document.getElementsByClassName("alerticon")[3].style.visibility= "visible";
        return false;
    }
    else{
        document.getElementsByClassName("alerticon")[3].style.visibility= "hidden";
        return true;
    }
}

////validate phone number function

function validatephonenumber(){
    var tempvalue = document.forms["registerform"]["phone1"].value;
    var phonenumberformat = /^[2-9][0-9]{9}$/;
    if(phonenumberformat.test(tempvalue) == false){
        document.getElementsByClassName("alerticon")[4].style.visibility= "visible";
        document.forms["registerform"]["phone1"].style.borderColor="rgb(255,99,71)";
        return false;
    }
    else if(phonenumberformat.test(tempvalue) == true){
        document.getElementsByClassName("alerticon")[4].style.visibility= "hidden";
        document.forms["registerform"]["phone1"].style.borderColor="rgb(204,204,204)";
        return true;
    }
}

//validate alternate phone number function

function validatealtphonenumber(){
    var tempvalue = document.forms["registerform"]["phone2"].value;
    var phonenumberformat = /^[2-9][0-9]{9}$/;
    if(phonenumberformat.test(tempvalue) == true || tempvalue == ""){
        document.getElementsByClassName("alerticon")[5].style.visibility= "hidden";
        document.forms["registerform"]["phone2"].style.borderColor="rgb(204,204,204)";
        return true;
    }
    else if(phonenumberformat.test(tempvalue) == false){
        document.getElementsByClassName("alerticon")[5].style.visibility= "visible";
        document.forms["registerform"]["phone2"].style.borderColor="rgb(255,99,71)";
        return false;
    }
}

//validate local address function

function validatelocaladdress(){
    var tempvalue = document.forms["registerform"]["current address"].value;
    if(tempvalue.trim() == ""){
        document.getElementsByClassName("alerticon")[6].style.visibility= "visible";
        document.forms["registerform"]["current address"].style.borderColor="rgb(255,99,71)";
        return false;
    }
    else if(tempvalue.trim() != ""){
        document.getElementsByClassName("alerticon")[6].style.visibility= "hidden";
        document.forms["registerform"]["current address"].style.borderColor="rgb(204,204,204)";
        return true;
    }
}
//validate permanent address function

function validatepermanentaddress(){
    var tempvalue = document.forms["registerform"]["permanent address"].value;
    if (tempvalue.trim() == "" && tempvalue!=""){
        document.getElementsByClassName("alerticon")[7].style.visibility= "visible";
        document.forms["registerform"]["current address"].style.borderColor="rgb(255,99,71)";
        return false;
    }
    else if(tempvalue.trim() != ""){
        document.getElementsByClassName("alerticon")[7].style.visibility= "hidden";
        document.forms["registerform"]["current address"].style.borderColor="rgb(204,204,204)";
        return true;
    }
}
//validate state function

function validatestate(){
    var tempvalue = document.forms["registerform"]["state"].value;
    if(tempvalue == ""){
        document.getElementsByClassName("alerticon")[8].style.visibility= "visible";
        document.forms["registerform"]["state"].style.borderColor="rgb(255,99,71)";
        return false;
    }
    else if(tempvalue != ""){
        document.getElementsByClassName("alerticon")[8].style.visibility= "hidden";
        document.forms["registerform"]["state"].style.borderColor="rgb(204,204,204)";
        return true;
    }
}

//validate country function

function validatecountry(){
    var tempvalue = document.forms["registerform"]["country"].value;
    if(tempvalue == ""){
        document.getElementsByClassName("alerticon")[9].style.visibility= "visible";
        document.forms["registerform"]["country"].style.borderColor="rgb(255,99,71)";
        return false;
    }
    else if(tempvalue != ""){
        document.getElementsByClassName("alerticon")[9].style.visibility= "hidden";
        document.forms["registerform"]["country"].style.borderColor="rgb(204,204,204)";
        return true;
    }
}

//validate captcha function

function validatecaptcha(){
    var tempvalue = document.forms["registerform"]["captcharesult"].value;
    if(tempvalue != cap ){
        document.getElementsByClassName("captchaalerticon")[0].style.visibility= "visible";
        document.forms["registerform"]["captcharesult"].style.borderColor="rgb(255,99,71)";
        return false;
    }
    else if(tempvalue == cap){
        document.getElementsByClassName("captchaalerticon")[0].style.visibility= "hidden";
        document.forms["registerform"]["captcharesult"].style.borderColor="rgb(204,204,204)";
        return true;
    }
}

//main validation function 

function validateform(firstname, lastname){
    var arr = new Array();
    arr[0]=validatename(firstname);
    arr[1]=validatename(lastname);
    arr[2]=validateemail();
    arr[3]=validatepassword();
    arr[4]=validatereenterpassword();
    arr[5]=validategender();
    arr[6]=validatephonenumber();
    arr[7]=validatealtphonenumber()
    arr[8]=validatelocaladdress();
    arr[9]=validatestate();
    arr[10]=validatecountry();
    arr[11]=validatecaptcha();
    for(var i=0; i<arr.length; i++)
    {
        if(arr[i]==false){
            decision=false;
            break;
        }
        else if(arr[i]==true)
        {
            decision=true;
            continue;
        }
    }
    if(decision==true)
    {
        alert("successfully registered");
    }
    return decision;
}