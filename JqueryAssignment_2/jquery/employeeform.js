var res=0, decision = false,phonearr=[],addressarr=[];
$(document).ready(function(){
    $(".submitbutton").on("click",function(){
        var arr= [validatefirstname(),validatelastname(),validateemail(),validatephonenumber(),validatepan(),
            validateaadhar(),validateaddress(),validatecountry(),validatestate(),validatecity(),validatepincode(),
            dynamicvalidate(),validatecaptcha()]
        for(var i=0; i<arr.length; i++)
        {
            if(arr[i]==false){decision=false;break;}
            else{decision = true;}
        }
        if(decision==true){
            alert("successfully registered");
            displaydata();
        }
        return false;
    })
    $('.Firstnamediv input').on("change keyup",validatefirstname);
    $('.Lastnamediv input').on("change keyup",validatelastname);
    $('.Emaildiv input').on("change keyup",validateemail);
    $('.Aadhardiv input').on("change keyup",validateaadhar);
    $('.Pandiv input').on("change keyup",validatepan);
    $('.Phonediv input').on("change keyup",validatephonenumber);
    $('.Countrydiv select').on("change keyup ",validatecountry);
    $('.Statediv select').on("change keyup ",validatestate);
    $('.Citydiv select').on("change keyup ",validatecity);
    $('.Addressdiv textarea').on("change keyup",validateaddress);
    $('.Pindiv input').on("change keyup",validatepincode);
    $('#captchafielddiv input').on("change keyup",validatecaptcha);
    $(document).on("change keyup", ".Altphonediv", function(){
        idvalue = $(this).attr('class').split(" ");
        dynamicphone(idvalue[2]);
    })
    $(document).on("change keyup", ".altfieldsetaddress .Addressdiv", function(){
        idvalue = $(this).closest(".altfieldsetaddress").attr('class').split(" ");
        dynamicaddress(idvalue[1]);
    })  
    $(document).on("change keyup", ".altfieldsetaddress .Pindiv", function(){
        idvalue = $(this).closest(".altfieldsetaddress").attr('class').split(" ");
        dynamicpincode(idvalue[1]);
    })  
    $(document).on("change keyup", ".altfieldsetaddress .Countrydiv , .altfieldsetaddress .Statediv , .altfieldsetaddress .Citydiv", function(){
        idvalue = $(this).closest(".altfieldsetaddress").attr('class').split(" ");
        dynamiccountrystatecity(idvalue[1],this);
    })  
    generateCaptcha();
    load_json_data('Country');
    $(".refreshicon").click(generateCaptcha);
    $(".addphonenumberbutton").on("click",phonenumberadd);
    $(".addaddressbutton").on("click",addressadd);
    $(".imgremovebutton").on("click",removeimage);
    $("#imginp").change(function(){readurl(this);});
    $("body").on("click",".deladdressbutton",function(){deleteaddress(this);})
    $("body").on("click",".delphonenumberbutton",function(){ deletephonenumber(this);})
    $(document).on("focus", ".Countrydiv select", function(){
        if($(this).find('select').val()!=undefined){
        var tempid = $(this).closest("fieldset").attr("class").split(" ");
        load_json_data('Country',this);
        $("."+tempid[1]).find(".Statediv select").html('<option value="" >Select State</option>'); 
        $("."+tempid[1]).find(".Citydiv select").html('<option value="" >Select City</option>'); 
        }
    })
    $(document).on('change',".Countrydiv select", function(){dynamicstate(this);})
    $(document).on('change', ".Statediv select", function(){dynamiccity(this);})
})
function dynamiccity(caller){
    var tempid = $(caller).closest("fieldset").attr("class").split(" ");
    var parent_name = $(caller).val();
    if(parent_name != ""){load_json_data('City',caller, parent_name);}
    else if(parent_name == ""){$("."+tempid[1]).find(".Citydiv select").html('<option value="" >Select City</option>');}
}
function dynamicstate(caller){
    var tempid = $(caller).closest("fieldset").attr("class").split(" ");
    var parent_name = $(caller).val();
    if(parent_name != ""){
        load_json_data('State',caller, parent_name);
        $("."+tempid[1]).find(".Citydiv select").html('<option value="" >Select City</option>');   
    }
    else if(parent_name == ""){
        $("."+tempid[1]).find(".Statediv select").html('<option value="" >Select State</option>');
        $("."+tempid[1]).find(".Citydiv select").html('<option value="" >Select City</option>');
    } 
}
function removeimage(){
    $(".profileimage").find("#profilephoto").attr("src","images/profile.jpg");
    $("#imginp").val("");
}
function deletephonenumber(caller){ 
    var temp=$(caller).parents(".Altphonediv").attr('class').split(" ");
    for(var i =0;i< phonearr.length;i++)
    {
        if(temp[2] == phonearr[i])
        {
            phonearr.splice(i,1);
        }
    }
    $(caller).parents(".Altphonediv").remove();
}
function deleteaddress(caller){
    var temp=$(caller).parents(".altfieldsetaddress").attr('class').split(" ");
    for(var i =0;i< addressarr.length;i++)
    {
        if(temp[1] == addressarr[i])
        {
            addressarr.splice(i,1);
        }
    }
    $(caller).parents(".altfieldsetaddress").remove();
}
function addressadd(){
    var addressfieldcode = $('#fakeaddressdiv').html();
    var newid = generateid();
    var codestring = "<fieldset class='altfieldsetaddress"+" "+ newid +"'>" + addressfieldcode +"</fieldset>";
    $(".captchafieldset").before(codestring);
    addresscontentchange(newid);
    addressarr.push(newid);
}
function addresscontentchange(newid){
    $("."+ newid).find('.Addressdiv textarea').attr('name','address'+ newid);
    $("."+ newid).find('.Countrydiv select').attr('name','country'+ newid);
    $("."+ newid).find('.Statediv select').attr('name','state'+ newid);
    $("."+ newid).find('.Citydiv select').attr('name','city'+ newid);
    $("."+ newid).find('.Pindiv select').attr('name','pincode'+ newid);
}
function phonenumberadd(){
    var phonefieldcode = $("#fakephonediv").html();
    var newid = generateid();
    var codestring = "<div class='inputfielddiv Altphonediv"+" "+ newid + "'>" + phonefieldcode + "</div>";
    $(".Pandiv").before(codestring);
    phonecontentchange(newid);
    phonearr.push(newid);
}
function phonecontentchange(newid){$("."+ newid).find('input').attr('name','altphone'+ newid);}
function generateid() {
    var result ='id';
    var d = new Date();
    var milsec = d.getMilliseconds().toString();
    var day = d.getDate().toString(), month = (d.getMonth()+1).toString(), year = d.getFullYear().toString(),
        hours = d.getHours().toString(), minutes= d.getMinutes().toString(), seconds = d.getSeconds().toString();
    var time =  hours + minutes + seconds + milsec + day + month + year;
    return result += time;
 }
function captchaGenerator(){
    var op = ['+', '-', '*', '/'];
    var opindex = Math.floor((Math.random() * 4));
    var operator = op[opindex];
    var first = Math.ceil((Math.random() * 10))+10;
    var second = Math.ceil((Math.random() * 5));
    if (opindex == 3) {
          while (second == 0 || first % second != 0) {
          first = Math.ceil((Math.random() * 10)+20);
          second = Math.ceil((Math.random() * 5));
          }
    }
    switch (opindex) {
       case 0:res = first + second;break;
       case 1:res = first - second;break;
       case 2:res = first * second;break;
       case 3:res = first / second;
    }
    var captchaString = " "+ first +" " + operator +" "+ second ;
    var captchaValue = res;
    return {captchaString: captchaString, captchaValue: res};
 }
function generateCaptcha(){
    var data = captchaGenerator();
    var captchaString = data.captchaString;
    var captchaValue = data.captchaValue;
    generateCanvas(captchaString);
}
function generateCanvas(captchaString){
    var canv = $(".captchacanvas")[0];
    var ctx = canv.getContext("2d");
    canv.width = 300;
    canv.height = 150;
    ctx.font = "40px Georgia";
    ctx.fillStyle = "black";
    ctx.fillText(captchaString, 87.5, 85);
 }
 function repeatcodecaptcha(temp1, temp2, temp3, temp4, show, color, display, msg, decide){
    temp1.css("visibility",show);
    temp2.css("border-color",color);
    temp3.css("visibility",display);
    temp4.html(msg);
    return decide;
 }
 function validatecaptcha(){
    var content = $("#captchafielddiv ").find('input').val();
    var temp1=$("#captchafielddiv").find(".alerticon"),temp2 = $("#captchafielddiv").find("input"), temp3 =  $("#captchafielddiv").find(".hide"),temp4= $("#captchafielddiv").find(".errmsg");
    if(content == "" ){
        return repeatcodecaptcha(temp1, temp2, temp3, temp4, "visible", "rgb(255,99,71)", "visible", "Value cannot be empty.", false);
    }
    else if(content != res ){
        return repeatcodecaptcha(temp1, temp2, temp3, temp4, "visible", "rgb(255,99,71)", "visible", "Entered value is invalid.", false);
    }
    else if(content == res){
        return repeatcodecaptcha(temp1, temp2, temp3, temp4, "hidden", "rgb(204,204,204)", "hidden", "", true);
    }
}
 function readurl(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#profilephoto').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}

function load_json_data(id,caller, parent_name)
{
    var html_code = '';
    if(caller == undefined){var tempid = null; }
    else{var tempid = $(caller).closest("fieldset").attr("class").split(" ");}
    $.getJSON('json/country_state_city.json', function(data){
    html_code += '<option value="" >Select '+id+'</option>';
    $.each(data, function(key, value){
        if(id == 'Country')
        {
            if(value.parent_name == 'null')
            {
                html_code += '<option value="'+value.name+'">'+value.name+'</option>';
                if(tempid == null){$("."+id+"div select").html(html_code);}
                else{$("."+tempid[1]).find("."+id+"div select").html(html_code);}
            }
        }
        else 
        {
            if(value.parent_name == parent_name)
            {
                html_code += '<option value="'+value.name+'">'+value.name+'</option>';
                $("."+tempid[1]).find("."+id+"div select").html(html_code);
            }
        }
    });
    });
 }
 function validatefieldsetrepeat(temp1, temp2, temp3, temp4, temp5, temp6, show, display, msg, color,decide,classname){
    temp1.css("visibility",show);
    temp2.css("visibility",display);
    temp3.html(msg);
    if(classname == "Addressdiv"){temp4.css("border-color",color);}
    else if (classname == "Countrydiv"|| classname == "Statediv"|| classname == "Citydiv"){temp5.css("border-color",color);}
    else {temp6.css("border-color",color);}
    return decide;
 }
 function validatefieldset(regex,content,idname,classname,contentlength){
     var temp1= $("."+idname).find("."+classname+" "+".alerticon"), temp2 = $("."+idname).find("."+classname+" "+".hide"),temp3= $("."+idname).find("."+classname+" "+".errmsg");
     var temp4 =$("."+idname).find("."+classname+" "+"textarea"),temp5 =  $("."+idname).find("."+classname+" "+"select"), temp6 = $("."+idname).find("."+classname+" "+"input");
    if(contentlength>100){
        return validatefieldsetrepeat(temp1, temp2, temp3, temp4, temp5, temp6, "visible","visible","Input entered too long , maximum limit 100 characters","rgb(255,99,71)",false,classname);
    }
    else if(content == ""){
        return validatefieldsetrepeat(temp1, temp2, temp3, temp4, temp5, temp6, "visible","visible","Value cannot be empty","rgb(255,99,71)",false,classname);
    }
    else if(content != "" && regex.test(content) == false){
        return validatefieldsetrepeat(temp1, temp2, temp3, temp4, temp5, temp6, "visible","visible","Entered value is invalid, hover over info icon to know the format","rgb(255,99,71)",false,classname);
    }
    else if(content != "" && regex.test(content) == true){
        return validatefieldsetrepeat(temp1, temp2, temp3, temp4, temp5, temp6, "hidden","hidden","","rgb(204,204,204)",true,classname);
    } 
 }
 function dynamicvalidate(){
     var temp1=[], temp2=[],temp3=[],temp4=[];
     var dynamicphonereturn, dynamicaddressreturn, dynamicpincodereturn , dynamiccountrystatecityreturn  ;
    for (var i =0; i < phonearr.length; i++)
    {
        temp1[i]=dynamicphone(phonearr[i]);
        if(temp1[i]==false){dynamicphonereturn = false;}
    }
    for( var j=0; j < addressarr.length; j++)
    {
        temp2[j]=dynamicaddress(addressarr[j]);
        temp3[j]=dynamicpincode(addressarr[j]);
        temp4[j]=dynamiccountrystatecity(addressarr[j]);
        if(temp2[j]==false){dynamicaddressreturn = false;}
        if(temp3[j]==false){dynamicpincodereturn = false;}
        if(temp4[j]==false){dynamiccountrystatecityreturn = false;}
    }
    if(dynamicphonereturn == false || dynamicaddressreturn == false || dynamicpincodereturn == false || dynamiccountrystatecityreturn == false){
        return false;
    }
    else{
        return true;
    }
 }
 function dynamicphone(id){
    var dynamicregex = /^[2-9][0-9]{9}$/;
    var content = $("."+id).find('input').val().trim();
    return validatefieldset(dynamicregex,content,'basicinfodiv', id, content.length)
 }
 function dynamicaddress(id){
    var dynamicaddressregex = /^[a-zA-Z0-9\s,'/.-]*$/;
    var content = $("."+id).find('.Addressdiv textarea').val().trim();
    return validatefieldset(dynamicaddressregex,content, id,'Addressdiv', content.length);
 }
 function dynamiccountrystatecity(id,caller){
    var dynamicregex = /^[a-z|A-Z]+([" "]?[a-z|A-Z]+)*$/;
    if($(caller).attr("class")=="inputfielddiv Countrydiv" || caller == undefined){
    var content1 = $("."+id).find('.Countrydiv select').val().trim();
    var temp1 = validatefieldset(dynamicregex,content1, id,'Countrydiv');
    }
    if($(caller).attr("class")=="inputfielddiv Statediv"  || caller == undefined){
    var content2 = $("."+id).find('.Statediv select').val().trim();
    var temp2 = validatefieldset(dynamicregex,content2, id,'Statediv');
    }
    if($(caller).attr("class")=="inputfielddiv Citydiv" || caller == undefined){
    var content3 = $("."+id).find('.Citydiv select').val().trim();
    var temp3 = validatefieldset(dynamicregex,content3, id,'Citydiv');
    }
    if(temp1 == false || temp2 == false || temp3 == false){return false;}
    else{return true;}
 }
 function dynamicpincode(id){
    var dynamicpincoderegex = /^[0-9]{6}$/;
    var content = $("."+id).find('.Pindiv input').val().trim();
    return validatefieldset(dynamicpincoderegex,content, id,'Pindiv', content.length);
 }
 function validatefirstname(){
    var firstnameregex = /^[a-z|A-Z]+[" "]?[a-z|A-Z]*$/;
    var content = $(".Firstnamediv input").val().trim();
    return validatefieldset(firstnameregex, content,"basicinfodiv","Firstnamediv",content.length);
}
function validatelastname(){
    var lastnameregex = /^[a-z|A-Z]+$/;
    var content = $(".Lastnamediv input").val().trim();
    return validatefieldset(lastnameregex, content,"basicinfodiv","Lastnamediv",content.length);
}
function validateemail(){
    var emailregex = /^\w+([\.+-]?\w+)*@(\w)+([\.-]?\w+)*(\.\w{2,3})+$/;
    var content = $(".Emaildiv input").val().trim();
    return validatefieldset(emailregex,content,"basicinfodiv","Emaildiv",content.length);
}
function validatephonenumber(){
    var phoneregex = /^[2-9][0-9]{9}$/;
    content = $(".Phonediv input").val().trim();
    return validatefieldset(phoneregex, content, "basicinfodiv","Phonediv", content.length);
}
function validateaadhar(){
    var aadharregex = /^[0-9]{4}[" "-][0-9]{4}[-][0-9]{4}$/;
    var content = $(".Aadhardiv input").val().trim();
    return validatefieldset(aadharregex,content,"basicinfodiv","Aadhardiv",content.length);
}
function validatepan(){
    var panregex = /^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}$/;
    var content = $(".Pandiv input").val().trim();
    return validatefieldset(panregex,content,"basicinfodiv","Pandiv",content.length);
}
function validatecountry(){
    var content = $(".fieldsetaddressdiv .Countrydiv select option:selected").val();
    var countryregex = /^[a-z|A-Z]+([" "]?[a-z|A-Z]+)*$/
    return validatefieldset(countryregex, content, "fieldsetaddressdiv", "Countrydiv");
}
function validatestate(){
    var content = $(".fieldsetaddressdiv .Statediv select option:selected").val();
    var stateregex = /^[a-z|A-Z]+([" "]?[a-z|A-Z]+)*$/
    return validatefieldset(stateregex, content, "fieldsetaddressdiv", "Statediv");
}
function validatecity(){
    var content = $(".fieldsetaddressdiv .Citydiv select option:selected").val();
    var cityregex = /^[a-z|A-Z]+([" "]?[a-z|A-Z]+)*$/
    return validatefieldset(cityregex, content, "fieldsetaddressdiv", "Citydiv");
}
function validateaddress(){
    var addressregex =/^[a-zA-Z0-9\s,'/.-]*$/;
    var content = $(".fieldsetaddressdiv .Addressdiv textarea").val().trim();
    return validatefieldset(addressregex, content, "fieldsetaddressdiv", "Addressdiv", content.length);
}
function validatepincode(){
    var pincoderegex =/^[0-9]{6}$/;
    var content = $(".fieldsetaddressdiv .Pindiv input").val().trim();
    return validatefieldset(pincoderegex, content, "fieldsetaddressdiv", "Pindiv", content.length);
}

function displaydata(){
    var countryname=[];
    $("#dispname").text($(".Firstnamediv input").val() +"  " + $(".Lastnamediv input").val());
    $("#dispemail").text($(".Emaildiv input").val());
    $("#dispphone").text($(".Phonediv input").val());
    $("#disppan").text($(".Pandiv input").val());
    $("#dispaadhar").text($(".Aadhardiv input").val());
    for(var i=phonearr.length - 1; i>=0; i--)
    {
        $('.row #dispphone').parent().after('<div class="row"><div class="col">Alternative phone no.</div><div class="col">'+$("."+phonearr[i]).find('input').val().trim()+'</div> </div>');
    }
    var combineaddress = $(".fieldsetaddressdiv .Addressdiv textarea").val() + " , " + $(".fieldsetaddressdiv .Citydiv select").val() +" , " +$(".fieldsetaddressdiv .Statediv select").val() +" , "+ 
    $(".fieldsetaddressdiv .Countrydiv select").val()+" , "+ $(".fieldsetaddressdiv .Pindiv input").val();
    $("#dispaddress").text(combineaddress);
    for(var i = addressarr.length-1; i>=0; i--){
        var addressvalue = $("."+addressarr[i]+" "+".Addressdiv"+" "+"textarea").val().trim();
        var countryname = $("."+addressarr[i]+" "+".Countrydiv"+" "+"select").val();
        var statename = $("."+addressarr[i]+" "+".Statediv"+" " +"select").val();
        var cityname = $("."+addressarr[i]+" "+".Citydiv"+" "+"select").val();
        var pinvalue = $("."+addressarr[i]+" "+".Pindiv"+" "+"input").val().trim();
        var combineaddress = addressvalue +","+cityname+","+statename+","+countryname+","+pinvalue;
        $('.row #dispaddress').parent().after('<div class="row"><div class="col">Alt address: </div><div class="col wrap-address">'+combineaddress+'</div></div>');
    }
    $("#image").attr("src",$("#profilephoto").attr("src"));
    $(".formdiv").addClass("hidden");
    $(".headingdiv").addClass("hidden");
    $("#dispdata").removeClass("hide");
}