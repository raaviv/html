<cfif NOT isDefined("session.sLoginUserDetails") OR NOT session.sLoginUserDetails["role"] EQ 'worker'>
	<cflocation url = "https://assignmentpractice.com/chaloGarage/common/index.cfm">
</cfif>
<cfset qWorkOrderDetails = application.dbQuery.getWorkOrderDetailsFromDB(session.sLoginUserDetails['userID'])/>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/updateWorkOrderStyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script type = "text/javascript"
         src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>
      <script src="jquery/workOrderJquery.js"></script>
    </head>
    <body>
		<cfinclude  template="includes/header.cfm">
		<div class="imageContainer" style="background-image: url(../common/images/bike_cars.jpg); height: auto;">
		<div class="container">
		<div class="row">
            <div class="column">
                <div class="containerBox">
                    <div class="card cardShadow" id="wizard">
					<div class="viewWorkOrderDetail">
						<div class="headingDiv">
							<h3 class="mainHeading"><span><img class="icon" src="../common/images/icons/editIcon.png"></span>Work Order Details</span>
							</h3>
						</div>
							<cfoutput query = "qWorkOrderDetails" group = "workOrderID">
		                    <div class="nameDiv" id="workOrderIDDiv">
		                        <label class="sideHeading">Work Order ID:</label>
								<label class="headingValue">#qWorkOrderDetails.workOrderID#</label>
		                    </div>

		                    <div class="nameDiv" id="CustomerNameDiv">
		                        <label class="sideHeading">Customer:</label>
								<label class="headingValue"> #qWorkOrderDetails.firstName# #qWorkOrderDetails.LastName#</label>
		                    </div>

		                    <div class="nameDiv" id="vehicleDiv">
		                        <label class="sideHeading">Vehicle:</label>
								<label class="headingValue"> #qWorkOrderDetails.vehicleCompany# #qWorkOrderDetails.vehicleModel#</label>
		                    </div>

		                    <div class="nameDiv" id="productDiv">
			                    <div class="sideHeading">
		                        <label >Products Brought:</label>
		                        </div>
		                        <div class="headingValue">
		                        <cfoutput>
								<label >#qWorkOrderDetails.productName# ,</label>
								</cfoutput>
								</div>
		                    </div>

							<div class="nameDiv" id="workOrderStatusDiv">
		                        <label class="sideHeading">Work Order Status </label>
								 <select class="selectDiv" name="workOrderStatus" id="workOrderStatus" >
                                       <option value="#qWorkOrderDetails.workOrderStatusValue#">#qWorkOrderDetails.workOrderStatusValue#</option>
                                 </select>
		                    </div>


		                    <div class="nameDiv" id="addressDiv">
			                    <div class="sideHeading">
		                        <label > Customer Address:</label>
		                        </div>
		                        <div class="headingValue">
								<label >#qWorkOrderDetails.AddressLane1# , #qWorkOrderDetails.AddressLane2# ,#qWorkOrderDetails.city# , #qWorkOrderDetails.state# ,
								#qWorkOrderDetails.country# , #qWorkOrderDetails.pinCode#</label>
								</div>
		                    </div>
		                    <button type="submit" id="UpdateStatusButton">Update Work Order Status</button>
		                    </cfoutput>
					</div>
					</div>
				</div>
			</div>
		</div>
		</div>
		</div>
			<cfinclude  template="includes/footer.cfm">
			</body>
