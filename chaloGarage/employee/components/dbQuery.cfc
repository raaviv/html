<!---
  --- dbQuery
  --- -------
  ---
  --- this component contains the queries required that are used in the rest of components
  ---
  --- author: mindfire
  --- date:   4/15/20
  --->
<cfcomponent hint="this component contains the queries required that are used in the rest of components" accessors="true" output="false" persistent="false">
	<cffunction name="getEmailFromDb" returntype="query" output="false" access="remote"
				description="this function is used to return query with email present in database">
		<cfargument name="inputEmail" type="string" required="true">

		 <cfquery name = "qEmailDetails" datasource = "chaloGarage">
	                SELECT	email
	                FROM 	[dbo].[communication]
	                WHERE 	email=<cfqueryparam value="#arguments.inputEmail#" cfsqltype="CF_SQL_VARCHAR" />
	            </cfquery>
	        <cfreturn qEmailDetails />
	</cffunction>


	<cffunction name="getPhoneNumberFromDb" returntype="query" output="false" access="remote"
				description="this function is used to return query with phone number present in database">
		<cfargument name="inputPhoneNumber" type="string" required="true">

		 <cfquery name = "qPhoneNumberDetails" datasource = "chaloGarage">
                SELECT	phoneNumber
                FROM 	[dbo].[communication]
                WHERE 	phoneNumber=<cfqueryparam value="#arguments.inputPhoneNumber#"
						cfsqltype="CF_SQL_NUMERIC" />
            </cfquery>
	        <cfreturn qPhoneNumberDetails />
	</cffunction>


	<cffunction name="getUserNameFromDb" returntype="query" output="false" access="remote"
				description="this function is used to return query with user name present in database">
		<cfargument name="inputUserName" type="string" required="true">

		 <cfquery name = "qUserNameDetails" datasource = "chaloGarage">
                SELECT	userName
                FROM 	[dbo].[user]
                WHERE 	userName=<cfqueryparam value="#arguments.inputUserName#" cfsqltype="CF_SQL_VARCHAR" />;
            </cfquery>
	        <cfreturn qUserNameDetails />
	</cffunction>


	<cffunction name = "getUserIDFromDb" access = "remote" output = "false" returnType = "query"
				description = "this function is used to retrive the userId based on unique userName">
	 	<cfargument name="inputUserName" type="string" required="true">

        <cfquery name="qGetUserID"  datasource = "chaloGarage">
			SELECT	userID
			FROM 	[dbo].[user]
			WHERE 	userName=<cfqueryparam value = "#arguments.inputUserName#" CFSQLType = "CF_SQL_VARCHAR" />
        </cfquery>
        <cfreturn qGetUserID />
</cffunction>


<cffunction name = "dataInsertion" access = "remote" output = "false" returntype = "boolean"
			description = "this function is used to insert data into database">
				<cfargument name = "companyID" type="string" required= "true"/>
				<cfargument name = "personTypeID" type="string" required="true"/>
	<cfset var isCommit=false />
	<cftransaction>
		<cftry>
	        <cfquery name = "qDataInsert">
				INSERT INTO	[dbo].[user] (firstName, lastName, userName, password, personTypeID, companyID)
				VALUES	(
							<cfqueryparam value = "#trim(form.firstname)#" CFSQLType = "CF_SQL_VARCHAR" />,
							<cfqueryparam value = "#trim(form.lastname)#" CFSQLType = "CF_SQL_VARCHAR" />,
							<cfqueryparam value = "#trim(form.username)#" CFSQLType = "CF_SQL_VARCHAR" />,
							<cfqueryparam value = "#form.inputPassword#" CFSQLType = "CF_SQL_VARCHAR" />,
							<cfqueryparam value = "#trim(arguments.personTypeID)#" CFSQLType = "CF_SQL_BIGINT" />,
							<cfqueryparam value = "#trim(arguments.companyID)#" CFSQLType = "CF_SQL_BIGINT" />
						)
			</cfquery>
			<cfset var quserID = application.dbQuery.getUserIDFromDb(form.username)>
			<cfquery name= "qInsertCommDetails">
				INSERT INTO	[dbo].[communication] (phoneNumber, email, userID)
				VALUES	(
							<cfqueryparam value = "#trim(form.phonenumber)#" CFSQLType = "CF_SQL_NUMERIC" />,
							<cfqueryparam value = "#trim(form.email)#" CFSQLType = "CF_SQL_VARCHAR" />,
							<cfqueryparam value = "#quserID.userID#" CFSQLType = "CF_SQL_BIGINT" />
						)
	        </cfquery>
			<cftransaction action="commit" />
			<cfset isCommit=true />
			<cfcatch type="database">
				<cftransaction action="rollback" />
				<cfset isCommit=false />
				<cflog  text="error : #cfcatch.detail#">
			</cfcatch>
		</cftry>
		</cftransaction>
		<cfreturn isCommit>
</cffunction>


<cffunction name = "productDataInsertion" access = "remote" output = "false" returntype = "boolean"
			description = "this function is used to insert product data into database">
				<cfargument name = "isPart" type="string" required= "true"/>
				<cfargument name = "vehicleTypeID" type="string" required="true"/>
	<cfset var isCommit=false />
	<cftransaction>
		<cftry>
	        <cfquery name = "qProductDataInsert">
				INSERT INTO	[dbo].[product] (productName, productDescription, duration, price, isPart, empRequired, vehicleTypeID, companyID)
				VALUES	(
							<cfqueryparam value = "#trim(form.productName)#" CFSQLType = "CF_SQL_VARCHAR" />,
							<cfqueryparam value = "#trim(form.productDescription)#" CFSQLType = "CF_SQL_VARCHAR" />,
							<cfqueryparam value = "#trim(form.productDuration)#" CFSQLType = "CF_SQL_INTEGER" />,
							<cfqueryparam value = "#trim(form.productCost)#" CFSQLType = "CF_SQL_INTEGER" />,
							<cfqueryparam value = "#trim(arguments.isPart)#" CFSQLType = "CF_SQL_BIT" />,
							<cfqueryparam value = "#trim(form.employeeRequired)#" CFSQLType = "CF_SQL_INTEGER" />,
							<cfqueryparam value = "#trim(arguments.vehicleTypeID)#" CFSQLType = "CF_SQL_BIGINT" />,
							<cfqueryparam value = "1" CFSQLType = "CF_SQL_BIGINT" />
						)
			</cfquery>
			<cftransaction action="commit" />
			<cfset isCommit=true />
			<cfcatch type="database">
				<cftransaction action="rollback" />
				<cfset isCommit=false />
				<cflog  text="error : #cfcatch.detail#">
			</cfcatch>
		</cftry>
		</cftransaction>
		<cfreturn isCommit>
</cffunction>


<cffunction name = "updateProductData" access = "remote" output = "false" returntype = "boolean"
			description = "this function is used to update product data into database">
				<cfargument name="productID" type="string" required="true"/>
				<cfargument name = "isPart" type="string" required= "true"/>
				<cfargument name = "vehicleTypeID" type="string" required="true"/>
	<cfset var isCommit=false />
	<cftransaction>
		<cftry>
	        <cfquery name = "qProductDataUpdate">
				UPDATE	 	[dbo].[product]
				SET			productName			=	<cfqueryparam value = "#trim(form.productName)#" CFSQLType = "CF_SQL_VARCHAR" />,
							productDescription	=	<cfqueryparam value = "#trim(form.productDescription)#" CFSQLType = "CF_SQL_VARCHAR" />,
							duration			=	<cfqueryparam value = "#trim(form.productDuration)#" CFSQLType = "CF_SQL_INTEGER" />,
							price				=	<cfqueryparam value = "#trim(form.productCost)#" CFSQLType = "CF_SQL_INTEGER" />,
							isPart				=	<cfqueryparam value = "#trim(arguments.isPart)#" CFSQLType = "CF_SQL_BIT" />,
							empRequired			=	<cfqueryparam value = "#trim(form.employeeRequired)#" CFSQLType = "CF_SQL_INTEGER" />,
							vehicleTypeID		=	<cfqueryparam value = "#trim(arguments.vehicleTypeID)#" CFSQLType = "CF_SQL_BIGINT" />,
							companyID			=	<cfqueryparam value = "1" CFSQLType = "CF_SQL_BIGINT" />
				WHERE		productID			=	<cfqueryparam value = "#arguments.productID#" CFSQLType = "CF_SQL_BIGINT" />
			</cfquery>
			<cftransaction action="commit" />
			<cfset isCommit=true />
			<cfcatch type="database">
				<cftransaction action="rollback" />
				<cfset isCommit=false />
				<cflog  text="error : #cfcatch.detail#">
			</cfcatch>
		</cftry>
		</cftransaction>
		<cfreturn isCommit>
</cffunction>



<cffunction name="getUserDetailsFromDb" returntype="query" output="false" access="remote"
			description="this function is used to return query with user details present in database">
		<cfargument name="inputUserName" type="string" required="true">
		<cfargument name="inputPassword" type="string" required="true">
		<cfquery name="qLoginUserDetails" datasource="chaloGarage">
				SELECT		firstName, lastName, userID, userName, password, personCategory
				FROM 		[dbo].[user]
				INNER JOIN	[dbo].[personType]
				ON 			[user].personTypeID = personType.personTypeID
				WHERE 		[user].userName = <cfqueryparam value = "#arguments.inputUserName#"
							cfsqltype="CF_SQL_VARCHAR" />
				AND 		[user].password = <cfqueryparam value = "#arguments.inputPassword#"
							cfsqltype="CF_SQL_VARCHAR" />
			</cfquery>
	        <cfreturn qLoginUserDetails />
	</cffunction>

<cffunction name="getUserProfileDetailsFromDb" returntype="query" output="false" access="remote"
			description="this function is used to return query with user profile details present in database">
		<cfargument name="userID" type="numeric" required="true">

		<cfquery name="qUserProfileDetails" datasource="chaloGarage">
				SELECT		firstName, lastName, userName, password, phoneNumber, altPhoneNumber, email, altEmail
				FROM 		[dbo].[user]
				INNER JOIN	[dbo].[communication] as comm
				ON 			[user].userID = comm.userID
				WHERE 		[user].userID = <cfqueryparam value = "#arguments.userID#"
							cfsqltype="CF_SQL_BIGINT" />
			</cfquery>
	        <cfreturn qUserProfileDetails />
	</cffunction>

<cffunction name="getUserAddressDetailsFromDb" returntype="query" output="false" access="remote"
			description="this function is used to return query with user address details present in database">
		<cfargument name="userID" type="numeric" required="true">

		<cfquery name="qUserAddressDetails" datasource="chaloGarage">
				SELECT		addressLane1, addressLane2, city, state, country, pinCode, isPrimary
				FROM 		[dbo].[user]
				INNER JOIN	[dbo].[address]
				ON 			[user].userID = address.userID
				WHERE 		[user].userID = <cfqueryparam value = "#arguments.userID#"
							cfsqltype="CF_SQL_BIGINT" />
			</cfquery>
	        <cfreturn qUserAddressDetails />
	</cffunction>

<cffunction name = "updateProfileIntoDb" access = "remote" output = "false" returntype = "boolean" description = "this function is used to update profile data into database">
		<cfargument name = "userID" type = "numeric" required = "true" />
		<cfargument name = "primaryAddressLine1" type="string" required = "true"/>
		<cfargument name = "primaryAddressLine2" type="string" required = "true"/>
		<cfargument name = "primaryCountry" type="string" required = "true"/>
		<cfargument name = "primaryState" type="string" required = "true"/>
		<cfargument name = "primaryCity" type="string" required = "true"/>
		<cfargument name = "primaryPincode" type="string" required = "true"/>
		<cfargument name = "hasAlternateAddress" type="boolean" required = "true"/>
		<cfargument name = "alternateAddressLine1" type="string" required="false"/>
		<cfargument name = "alternateAddressLine2" type="string" required="false"/>
        <cfargument name = "alternateCountry" type="string" required="false"/>
        <cfargument name = "alternateState" type="string" required="false"/>
        <cfargument name = "alternateCity" type="string" required="false"/>
        <cfargument name = "alternatePincode" type="string" required="false"/>
		<cfset variables.userAddress = application.dbQuery.getUserAddressDetailsFromDb(session.sLogInUserDetails.userID)>
		<cfset var isCommit=false />
		<cftransaction>
		<cftry>
	        <cfquery name = "qProfileUpdate">
				UPDATE 		[dbo].[user]
				SET 		firstName =	<cfqueryparam value = "#trim(form.firstName)#" CFSQLType = "CF_SQL_VARCHAR" />,
							lastName  =	<cfqueryparam value = "#trim(form.lastName)#" CFSQLType = "CF_SQL_VARCHAR" />,
							userName  =	<cfqueryparam value = "#trim(form.userName)#" CFSQLType = "CF_SQL_VARCHAR" />,
							password  =	<cfqueryparam value = "#trim(form.password)#" CFSQLType = "CF_SQL_VARCHAR" />
				WHERE		userID    = <cfqueryparam value = "#arguments.userID#" CFSQLType = "CF_SQL_BIGINT" />
			</cfquery>
			<cfquery name= "qProfileUpdateCommDetails">
				UPDATE 		[dbo].[communication]
				SET			phoneNumber = <cfqueryparam value = "#trim(form.phoneNumber)#" CFSQLType = "CF_SQL_NUMERIC" />,
							email	    = <cfqueryparam value = "#trim(form.email)#" CFSQLType = "CF_SQL_VARCHAR" />,
							altPhoneNumber = <cfif form.altPhoneNumber EQ ''>
												<cfqueryparam value = "#trim(form.altPhoneNumber)#" CFSQLType = "CF_SQL_NUMERIC" null="yes"/>,
											<cfelse>
												<cfqueryparam value = "#trim(form.altPhoneNumber)#" CFSQLType = "CF_SQL_NUMERIC" null="no"/>,
											</cfif>
							altEmail = <cfif form.altEmail EQ ''>
												<cfqueryparam value = "#trim(form.altEmail)#" CFSQLType = "CF_SQL_VARCHAR" null="yes"/>
											<cfelse>
												<cfqueryparam value = "#trim(form.altEmail)#" CFSQLType = "CF_SQL_VARCHAR" null="no"/>
											</cfif>
				WHERE		userID      = <cfqueryparam value = "#arguments.userID#" CFSQLType = "CF_SQL_BIGINT" />
	        </cfquery>
	        <cfif variables.userAddress.recordCount EQ 0>
	        	<cfquery name= "qProfileInsertPrimaryAddressDetails">
				INSERT INTO	[dbo].[address] (addressLane1, addressLane2, country, state, city, pinCode, isPrimary, userID)
				VALUES	(
							<cfqueryparam value = "#arguments.primaryAddressLine1#" CFSQLType = "CF_SQL_VARCHAR">,
							<cfif arguments.primaryAddressLine2 EQ ''>
								<cfqueryparam value = "#arguments.primaryAddressLine2#" CFSQLType = "CF_SQL_VARCHAR" null="yes">,
							<cfelse>
								<cfqueryparam value = "#arguments.primaryAddressLine2#" CFSQLType = "CF_SQL_VARCHAR" null="no">,
							</cfif>
							<cfqueryparam value = "#arguments.primaryCountry#" CFSQLType = "CF_SQL_VARCHAR">,
							<cfqueryparam value = "#arguments.primaryState#" CFSQLType = "CF_SQL_VARCHAR">,
							<cfqueryparam value = "#arguments.primaryCity#" CFSQLType = "CF_SQL_VARCHAR">,
							<cfqueryparam value = "#arguments.primaryPincode#" CFSQLType = "CF_SQL_NUMERIC">,
							<cfqueryparam value = "1" CFSQLType ="CF_SQL_BIT">,
							<cfqueryparam value = "#arguments.userID#" CFSQLType = "CF_SQL_BIGINT" />
						)
	        	</cfquery>
	        	<cfquery name= "qProfileInsertAlternateAddressDetails">
				INSERT INTO	[dbo].[address] (addressLane1, addressLane2, country, state, city, pinCode, isPrimary, userID)
				VALUES	(
							<cfqueryparam value = "#arguments.alternateAddressLine1#" CFSQLType = "CF_SQL_VARCHAR">,
							<cfif arguments.primaryAddressLine2 EQ ''>
								<cfqueryparam value = "#arguments.alternateAddressLine2#" CFSQLType = "CF_SQL_VARCHAR" null="yes">,
							<cfelse>
								<cfqueryparam value = "#arguments.alternateAddressLine2#" CFSQLType = "CF_SQL_VARCHAR" null="no">,
							</cfif>
							<cfqueryparam value = "#arguments.alternateCountry#" CFSQLType = "CF_SQL_VARCHAR">,
							<cfqueryparam value = "#arguments.alternateState#" CFSQLType = "CF_SQL_VARCHAR">,
							<cfqueryparam value = "#arguments.alternateCity#" CFSQLType = "CF_SQL_VARCHAR">,
							<cfqueryparam value = "#arguments.alternatePincode#" CFSQLType = "CF_SQL_NUMERIC">,
							<cfqueryparam value = "0" CFSQLType ="CF_SQL_BIT">,
							<cfqueryparam value = "#arguments.userID#" CFSQLType = "CF_SQL_BIGINT" />
						)
	        	</cfquery>
			<cfelseif variables.userAddress.recordCount EQ 1>
				<cfquery name = "qProfileUpdatePrimaryAddressDetails">
		        UPDATE 		[dbo].[address]
		        SET			addressLane1 = <cfqueryparam value = "#arguments.primaryAddressLine1#" CFSQLType = "CF_SQL_VARCHAR">,
		        			addressLane2 = <cfif arguments.primaryAddressLine2 EQ ''>
												<cfqueryparam value = "#arguments.primaryAddressLine2#" CFSQLType = "CF_SQL_VARCHAR" null="yes">,
											<cfelse>
												<cfqueryparam value = "#arguments.primaryAddressLine2#" CFSQLType = "CF_SQL_VARCHAR" null="no">,
											</cfif>
							country		 = <cfqueryparam value = "#arguments.primaryCountry#" CFSQLType = "CF_SQL_VARCHAR">,
							state		 = <cfqueryparam value = "#arguments.primaryState#" CFSQLType = "CF_SQL_VARCHAR">,
							city		 = <cfqueryparam value = "#arguments.primaryCity#" CFSQLType = "CF_SQL_VARCHAR">,
							pinCode		 = <cfqueryparam value = "#arguments.primaryPincode#" CFSQLType = "CF_SQL_NUMERIC">,
							isPrimary	 = <cfqueryparam value = "1" CFSQLType ="CF_SQL_BIT">
				WHERE		userID		 = <cfqueryparam value = "#arguments.userID#" CFSQLType = "CF_SQL_BIGINT" />
	        	</cfquery>
	        	<cfif arguments.hasAlternateAddress EQ true>
		        	<cfquery name= "qProfileInsertAlternateAddressDetails">
							INSERT INTO	[dbo].[address] (addressLane1, addressLane2, country, state, city, pinCode, isPrimary, userID)
							VALUES	(
										<cfqueryparam value = "#arguments.alternateAddressLine1#" CFSQLType = "CF_SQL_VARCHAR">,
										<cfif arguments.alternateAddressLine2 EQ ''>
											<cfqueryparam value = "#arguments.alternateAddressLine2#" CFSQLType = "CF_SQL_VARCHAR" null="yes">,
										<cfelse>
											<cfqueryparam value = "#arguments.alternateAddressLine2#" CFSQLType = "CF_SQL_VARCHAR" null="no">,
										</cfif>
										<cfqueryparam value = "#arguments.alternateCountry#" CFSQLType = "CF_SQL_VARCHAR">,
										<cfqueryparam value = "#arguments.alternateState#" CFSQLType = "CF_SQL_VARCHAR">,
										<cfqueryparam value = "#arguments.alternateCity#" CFSQLType = "CF_SQL_VARCHAR">,
										<cfqueryparam value = "#arguments.alternatePincode#" CFSQLType = "CF_SQL_NUMERIC">,
										<cfqueryparam value = "0" CFSQLType ="CF_SQL_BIT">,
										<cfqueryparam value = "#arguments.userID#" CFSQLType = "CF_SQL_BIGINT" />
									)
	        		</cfquery>
		        </cfif>

			<cfelseif variables.userAddress.recordCount EQ 2>
				<cfquery name = "qProfileUpdatePrimaryAddressDetails">
		        UPDATE 		[dbo].[address]
		        SET			addressLane1 = <cfqueryparam value = "#arguments.primaryAddressLine1#" CFSQLType = "CF_SQL_VARCHAR">,
		        			addressLane2 = <cfif arguments.primaryAddressLine2 EQ ''>
												<cfqueryparam value = "#arguments.primaryAddressLine2#" CFSQLType = "CF_SQL_VARCHAR" null="yes">,
											<cfelse>
												<cfqueryparam value = "#arguments.primaryAddressLine2#" CFSQLType = "CF_SQL_VARCHAR" null="no">,
											</cfif>
							country		 = <cfqueryparam value = "#arguments.primaryCountry#" CFSQLType = "CF_SQL_VARCHAR">,
							state		 = <cfqueryparam value = "#arguments.primaryState#" CFSQLType = "CF_SQL_VARCHAR">,
							city		 = <cfqueryparam value = "#arguments.primaryCity#" CFSQLType = "CF_SQL_VARCHAR">,
							pinCode		 = <cfqueryparam value = "#arguments.primaryPincode#" CFSQLType = "CF_SQL_NUMERIC">,
							isPrimary	 = <cfqueryparam value = "1" CFSQLType ="CF_SQL_BIT">
				WHERE		userID		 = <cfqueryparam value = "#arguments.userID#" CFSQLType = "CF_SQL_BIGINT" />
				AND 		isPrimary	 = 1;
	        	</cfquery>

	        	<cfquery name = "qProfileUpdateAlternateAddressDetails">
		        UPDATE 		[dbo].[address]
		        SET			addressLane1 = <cfqueryparam value = "#arguments.alternateAddressLine1#" CFSQLType = "CF_SQL_VARCHAR">,
		        			addressLane2 = <cfif arguments.alternateAddressLine2 EQ ''>
												<cfqueryparam value = "#arguments.alternateAddressLine2#" CFSQLType = "CF_SQL_VARCHAR" null="yes">,
											<cfelse>
												<cfqueryparam value = "#arguments.alternateAddressLine2#" CFSQLType = "CF_SQL_VARCHAR" null="no">,
											</cfif>
							country		 = <cfqueryparam value = "#arguments.alternateCountry#" CFSQLType = "CF_SQL_VARCHAR">,
							state		 = <cfqueryparam value = "#arguments.alternateState#" CFSQLType = "CF_SQL_VARCHAR">,
							city		 = <cfqueryparam value = "#arguments.alternateCity#" CFSQLType = "CF_SQL_VARCHAR">,
							pinCode		 = <cfqueryparam value = "#arguments.alternatePincode#" CFSQLType = "CF_SQL_NUMERIC">,
							isPrimary	 = <cfqueryparam value = "0" CFSQLType ="CF_SQL_BIT">
				WHERE		userID		 = <cfqueryparam value = "#arguments.userID#" CFSQLType = "CF_SQL_BIGINT" />
				AND			isPrimary 	 = 0;
	        	</cfquery>

			</cfif>
			<cftransaction action="commit" />
			<cfset isCommit=true />
			<cfcatch type="database">
				<cftransaction action="rollback" />
				<cfset isCommit=false />
				<cflog  text="error : #cfcatch.detail#">
			</cfcatch>
		</cftry>
		</cftransaction>
		<cfreturn isCommit>
</cffunction>

<cffunction name="getCompanyDetails" access="remote" output="false" returntype="query" >
	<cfquery name="qCompanyDetails">
		SELECT		companyName
		FROM 		[dbo].[company]
	</cfquery>
	<cfreturn qCompanyDetails />
</cffunction>

<cffunction name="getCompanyID" access="remote" output="false" returntype="query" >
	<cfargument name="companyName" type="string" required="true"/>
	<cfquery name="qCompanyIDDetails">
		SELECT		companyID
		FROM 		[dbo].[company]
		WHERE		companyName=<cfqueryparam value="#arguments.companyName#" cfsqltype="CF_SQL_VARCHAR" />
	</cfquery>
	<cfreturn qCompanyIDDetails />
</cffunction>

<cffunction name="getPersonTypeID" access="remote" output="false" returntype="query" >
	<cfargument name="personType" type="string" required="true"/>
	<cfquery name="qPersonTypeIDDetails">
		SELECT		personTypeID
		FROM 		[dbo].[personType]
		WHERE		personCategory=<cfqueryparam value="#arguments.personType#" cfsqltype="CF_SQL_VARCHAR" />
	</cfquery>
	<cfreturn qPersonTypeIDDetails />
</cffunction>

<cffunction name="getProductNameFromDb" access="remote" output="false" returntype="query" >
	<cfargument name="productName" type="string" required="true"/>
	<cfquery name="qProductNameDetails">
		SELECT		productName,productID,price,vehicleCategory, isPart
		FROM 		[dbo].[product]
		INNER JOIN	[dbo].[vehicleType]
		ON			product.vehicleTypeID = vehicleType.vehicleTypeID
		WHERE		productName=<cfqueryparam value="#arguments.productName#" cfsqltype="CF_SQL_VARCHAR" />
	</cfquery>
	<cfreturn qProductNameDetails />
</cffunction>


<cffunction name="getProductFromDb" access="remote" output="false" returntype="query" >
	<cfquery name="qProductDetails">
		SELECT		productName,productID,price,vehicleCategory, isPart
		FROM 		[dbo].[product]
		INNER JOIN	[dbo].[vehicleType]
		ON			product.vehicleTypeID = vehicleType.vehicleTypeID
	</cfquery>
	<cfreturn qProductDetails />
</cffunction>

<cffunction name="getProductBySearchFromDb" access="remote" output="false" returntype="query" >
	<cfargument name="searchValue" type="string" required="true">
	<cfquery name="qProductBySearchDetails">
		SELECT		productName,productID,price,vehicleCategory, isPart
		FROM 		[dbo].[product]
		INNER JOIN	[dbo].[vehicleType]
		ON			product.vehicleTypeID = vehicleType.vehicleTypeID
		WHERE		productName 	LIKE	'%#arguments.searchValue#%'
	</cfquery>
	<cfreturn qProductBySearchDetails />
</cffunction>

<cffunction name="getProductByIDFromDb" access="remote" output="false" returntype="query" >
	<cfargument name="productID" type="string" required="true">
	<cfquery name="qProductByIDDetails">
		SELECT		productName, productDescription, isPart, vehicleCategory, price, duration, empRequired
		FROM 		[dbo].[product]
		INNER JOIN	[dbo].[vehicleType]
		ON			product.vehicleTypeID = vehicleType.vehicleTypeID
		WHERE		productID = <cfqueryparam value="#arguments.productID#" cfsqltype="CF_SQL_BIGINT" />
	</cfquery>
	<cfreturn qProductByIDDetails />
</cffunction>

<cffunction name="deleteProductFromDb" access="remote" output="false" returntype="boolean" >
	<cfargument name="productID" type="string" required="true"/>
	<cfset var isCommit=false />
	<cftransaction>
		<cftry>
			<cfquery name="qDeleteProduct">
				DELETE
				FROM 		[dbo].[product]
				WHERE		productID =<cfqueryparam value="#arguments.productID#" cfsqltype="CF_SQL_BIGINT" />
			</cfquery>
			<cftransaction action="commit" />
			<cfset isCommit=true />
			<cfcatch type="database">
				<cftransaction action="rollback" />
				<cfset isCommit=false />
				<cflog  text="error : #cfcatch.detail#">
			</cfcatch>
		</cftry>
		</cftransaction>
		<cfreturn isCommit>
</cffunction>

<cffunction name="getWorkOrderDetailsFromDB" access="remote" output="false" returntype="query" >
	<cfargument name="userID" type="string" required="true"/>
	<cfquery name="qWorkOrderDetailsFromDb">
			SELECT wo.workOrderID, us.firstName, us.lastName, pd.productName, vd.vehicleCompany, vd.vehicleModel, wos.workOrderStatusValue, ad.addressLane1 , ad.addressLane2, ad.city, ad.state, ad.country, ad.pinCode
			FROM		[dbo].[workOrder] as wo
			LEFT JOIN	[dbo].[vehicle] as vh
			ON			vh.vehicleID = wo.vehicleID
			LEFT JOIN   [dbo].[user] as us
			ON			us.userID = vh.userID
			LEFT JOIN	[dbo].[workOrderStatus] as wos
			ON			wo.workOrderStatusID = wos.workOrderStatusID
			LEFT JOIN	[dbo].[vehicleDetail] as vd
			ON			vd.vehicleDetailsID = vh.vehicleDetailsID
			LEFT JOIN	[dbo].[address] as ad
			ON			wo.addressID = ad.addressID
			LEFT JOIN	[dbo].[userWorkOrder] as uwo
			ON			wo.workOrderID = uwo.workOrderID
			LEFT JOIN	[dbo].[user] as usr
			ON			usr.userID = uwo.userID
			LEFT JOIN	[dbo].[productWorkOrder] as pwo
			ON			wo.workOrderID = pwo.workOrderID
			LEFT JOIN	[dbo].[product] as pd
			ON			pd.productID = pwo.productID
			WHERE		usr.userID = <cfqueryparam value="#arguments.userID#" cfsqltype="CF_SQL_BIGINT" />
			AND			endTime		IS NULL
	</cfquery>
	<cfreturn qWorkOrderDetailsFromDb />
</cffunction>

<cffunction name="getRecentWorkOrderDetailsFromDb" access="remote" output="false" returntype="query" >
	<cfquery name="qRecentWorkOrderDetailsFromDb">
			SELECT		wo.workOrderID, us.firstName, us.lastName, pd.productName, vd.vehicleCompany, vd.vehicleModel, wos.workOrderStatusValue, ad.addressLane1 , ad.addressLane2, ad.city, ad.state, ad.country, ad.pinCode
			FROM		[dbo].[workOrder] as wo
			LEFT JOIN	[dbo].[vehicle] as vh
			ON			vh.vehicleID = wo.vehicleID
			LEFT JOIN   [dbo].[user] as us
			ON			us.userID = vh.userID
			LEFT JOIN	[dbo].[workOrderStatus] as wos
			ON			wo.workOrderStatusID = wos.workOrderStatusID
			LEFT JOIN	[dbo].[vehicleDetail] as vd
			ON			vd.vehicleDetailsID = vh.vehicleDetailsID
			LEFT JOIN	[dbo].[address] as ad
			ON			wo.addressID = ad.addressID
			LEFT JOIN	[dbo].[productWorkOrder] as pwo
			ON			wo.workOrderID = pwo.workOrderID
			LEFT JOIN	[dbo].[product] as pd
			ON			pd.productID = pwo.productID
			ORDER BY	startTime		DESC
	</cfquery>
	<cfreturn qRecentWorkOrderDetailsFromDb />
</cffunction>

<cffunction name="getWorkOrderByIDFromDb" access="remote" output="false" returntype="query" >
	<cfargument name="workOrderID" type="string" required="true" />
	<cfquery name="qWorkOrderByIDFromDb">
			SELECT		usr.firstName, usr.lastName, wos.workOrderStatusValue, ad.addressLane1 , ad.addressLane2, ad.city, ad.state, ad.country, ad.pinCode
			FROM		[dbo].[workOrder] as wo
			LEFT JOIN	[dbo].[workOrderStatus] as wos
			ON			wo.workOrderStatusID = wos.workOrderStatusID
			LEFT JOIN	[dbo].[address] as ad
			ON			wo.addressID = ad.addressID
			LEFT JOIN	[dbo].[userWorkOrder] as uwo
			ON			wo.workOrderID = uwo.workOrderID
			LEFT JOIN	[dbo].[user] as usr
			ON			usr.userID = uwo.userID
			WHERE		wo.workOrderID = <cfqueryparam value="#arguments.workOrderID#" cfsqltype="CF_SQL_BIGINT" />
	</cfquery>
	<cfreturn qWorkOrderByIDFromDb />
</cffunction>

<cffunction name="getWorkOrderStatusFromDb" access="remote" output="false" returntype="query" >
	<cfquery name="qWorkOrderStatusFromDb">
			SELECT		workOrderStatusValue
			FROM		[dbo].[workOrderStatus]
	</cfquery>
	<cfreturn qWorkOrderStatusFromDb />
</cffunction>

<cffunction name="getworkOrderStatusIDFromDb" access="remote" output="false" returntype="query" >
	<cfargument name="workOrderStatusValue" type="string" required="true"/>
	<cfquery name="qworkOrderStatusIDFromDb">
			SELECT		workOrderStatusID
			FROM		[dbo].[workOrderStatus]
			WHERE		workOrderStatusValue = <cfqueryparam value = "#arguments.workOrderStatusValue#" CFSQLType = "CF_SQL_VARCHAR" />
	</cfquery>
	<cfreturn qworkOrderStatusIDFromDb />
</cffunction>

<cffunction name = "updateWorkOrderStatusToDb" access = "remote" output = "false" returntype = "boolean"
			description = "this function is used to update product data into database">
				<cfargument name="workOrderStatusValue" type="string" required="true"/>
				<cfargument name = "workOrderID" type="string" required="true"/>
	<cfset var isCommit=false />
	<cfset var qworkOrderStatusID = application.dbQuery.getworkOrderStatusIDFromDb(arguments.workOrderStatusValue)>
	<cftransaction>
		<cftry>
	        <cfquery name = "qUpdateWorkOrderStatus">
				UPDATE	 	[dbo].[workOrder]
				SET			workOrderStatusID		=	<cfqueryparam value = "#qworkOrderStatusID.workOrderStatusID#" CFSQLType = "CF_SQL_BIGINT" />
				WHERE		workOrderID			=	<cfqueryparam value = "#arguments.workOrderID#" CFSQLType = "CF_SQL_BIGINT" />
			</cfquery>
			<cftransaction action="commit" />
			<cfset isCommit=true />
			<cfcatch type="database">
				<cftransaction action="rollback" />
				<cfset isCommit=false />
				<cflog  text="error : #cfcatch.detail#">
			</cfcatch>
		</cftry>
		</cftransaction>
		<cfreturn isCommit>
</cffunction>


</cfcomponent>