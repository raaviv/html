<!---
  --- productManagement
  --- -----------------
  ---
  --- this component contains methods for adding , updating and deleting products.
  ---
  --- author: mindfire
  --- date:   5/4/20
  --->
<cfcomponent hint="this component contains methods for adding , updating and deleting products." accessors="true" output="false" persistent="false">

<cffunction name="addProductValidation" access="remote" output="false" return type="Struct" returnformat = "json">
		<cfargument name = "productName" type = "string" required = "true" />
		<cfargument name = "productDescription" type = "string" required = "false" />
		<cfargument name = "productType" type = "string" required = "true" />
		<cfargument name = "vehicleType" type = "string" required = "true" />
		<cfargument name = "productCost" type = "string" required = "true" />
		<cfargument name = "productDuration" type = "string" required = "false" />
		<cfargument name = "employeeRequired" type = "string" required = "true" />
		<cfset var errorMessages = {} />
		<cfif arguments.productType EQ 'part'>
			<cfset variables.isPart = 1>
		<cfelseif arguments.productType EQ 'service'>
			<cfset variables.isPart = 0>
		</cfif>
		<cfif arguments.vehicleType EQ 'bike'>
			<cfset variables.vehicleTypeID = 2>
		<cfelseif argumets.vehicleType EQ 'car'>
			<cfset variables.vehicleTypeID = 1>
		</cfif>
		<!---Validate Product Name--->
		<cfif len(arguments.productName) gt 30>
			<cfset errorMessages.productName = "Value entered is too long , limit is of 30 characters " />
		<cfelseif arguments.productName EQ '' OR NOT isValid('regex',arguments.productName,'[a-z|A-Z]+([" "]?[a-z|A-Z])*') >
			<cfset errorMessages.productName = 'Please provide a valid product Name'/>
		<cfelse>
			<cftry>
				<cfset var qProductNameDetails = application.dbQuery.getProductNameFromDb(arguments.productName)>
	            <cfcatch type="database">
		            <cflog  text="error : #cfcatch.detail#">
            		<cfset errorMessages.productName="Some Error occurred while checking the email. Please, try after sometime!!"/>
	            </cfcatch>
			</cftry>
            <cfif qProductNameDetails.recordCount EQ 1>
                <cfset errorMessages.productName = "This Product is already present!!">
            </cfif>
		</cfif>

		<!---Validate Product description--->
		<cfif len(arguments.productDescription) gt 100>
			<cfset errorMessages.productDescription = "Value entered is too long , limit is of 100 characters " />
		<cfelseif arguments.productDescription NEQ '' AND NOT isValid("regex", arguments.productDescription, "[a-zA-Z0-9\s,'/.-]*")>
			<cfset errorMessages.productDescription = "Please provide a valid Product Description" />
		</cfif>

		<!---Validate productType --->
		<cfif len(arguments.productType) gt 25>
			<cfset errorMessages.productType = "Value entered is too long , limit is of 25 characters " />
		<cfelseif arguments.productType EQ ''OR NOT isValid("regex", arguments.productType, "[a-z|A-Z]+")>
			<cfset errorMessages.productType = "Please provide a valid product Type" />
		</cfif>

		<!---Validate vehicleType --->
		<cfif len(arguments.vehicleType) gt 25>
			<cfset errorMessages.vehicleType = "Value entered is too long , limit is of 25 characters " />
		<cfelseif arguments.vehicleType EQ ''OR NOT isValid("regex", arguments.vehicleType, "[a-z|A-Z]+")>
			<cfset errorMessages.vehicleType = "Please provide a valid vehicle Type" />
		</cfif>

		<!---Validate employeeRequired --->
		<cfif len(arguments.employeeRequired) gt 10>
			<cfset errorMessages.employeeRequired = "Value entered is too long , limit is of 10 characters " />
		<cfelseif arguments.employeeRequired EQ ''OR NOT isValid("regex", arguments.employeeRequired, "[1-9][0-9]*")>
			<cfset errorMessages.employeeRequired = "Please provide a valid value" />
		</cfif>

		<!---Validate productCost --->
		<cfif len(arguments.productCost) gt 10>
			<cfset errorMessages.productCost = "Value entered is too long , limit is of 10 characters " />
		<cfelseif arguments.productCost EQ ''OR NOT isValid("regex", arguments.productCost, "[1-9][0-9]+")>
			<cfset errorMessages.productCost = "Please provide a valid product Cost" />
		</cfif>

		<!---Validate productDuration --->
		<cfif len(arguments.productDuration) gt 360>
			<cfset errorMessages.productDuration = "Value entered is too long , limit is of 360 characters " />
		<cfelseif arguments.productDuration NEQ ''AND NOT isValid("regex", arguments.productDuration, "[1-9][0-9]*")>
			<cfset errorMessages.productDuration = "Please provide a valid product Duration" />
		</cfif>

		<!---checking the validation error message and inserting user data into db --->
		<cfif StructCount(errorMessages) eq 0>
			 <cfset var res = application.dbQuery.productDataInsertion(variables.isPart, variables.vehicleTypeID) />
			 <cfif res eq false>
				 <cfset errorMessages.productDuration ='Addition of product is not Successful, try again!!'/>
			</cfif>
		</cfif>

		<cfreturn errorMessages />

</cffunction>


<cffunction name="deleteProduct" access="remote" output="false" return type="boolean">
	<cfargument name="productID" type="string" required="true" />
	<cfset var isCommit = application.dbQuery.deleteProductFromDb(arguments.productID)>
	<cfreturn isCommit />
</cffunction>

<cffunction name="getProductBySearch" access="remote" output="false" return type="query" returnformat="json">
	<cfargument name="searchValue" type="string" required="true" />
	<cfif arguments.searchValue EQ ''>
		<cfset var qProductDetails = application.dbQuery.getProductFromDb()>
	<cfelse>
		<cfset var qProductDetails = application.dbQuery.getProductBySearchFromDb(arguments.searchValue)>
	</cfif>
	<cfreturn qProductDetails />
</cffunction>


<cffunction name="getProductByID" access="remote" output="false" return type="query" returnformat="json">
	<cfargument name="productID" type="string" required="true" />
	<cfset var qProductDetails = application.dbQuery.getProductByIDFromDb(arguments.productID)>
	<cfreturn qProductDetails />
</cffunction>



<cffunction name="updateProductValidation" access="remote" output="false" return type="Struct" returnformat = "json">
		<cfargument name = "productID" type = "string" required = "true" />
		<cfargument name = "previousProductName" type = "string" required = "true" />
		<cfargument name = "productName" type = "string" required = "true" />
		<cfargument name = "productDescription" type = "string" required = "false" />
		<cfargument name = "productType" type = "string" required = "true" />
		<cfargument name = "vehicleType" type = "string" required = "true" />
		<cfargument name = "productCost" type = "string" required = "true" />
		<cfargument name = "productDuration" type = "string" required = "false" />
		<cfargument name = "employeeRequired" type = "string" required = "true" />
		<cfset var errorMessages = {} />
		<cfif arguments.productType EQ 'part'>
			<cfset variables.isPart = 1>
		<cfelseif arguments.productType EQ 'service'>
			<cfset variables.isPart = 0>
		</cfif>
		<cfif arguments.vehicleType EQ 'bike'>
			<cfset variables.vehicleTypeID = 2>
		<cfelseif argumets.vehicleType EQ 'car'>
			<cfset variables.vehicleTypeID = 1>
		</cfif>
		<!---Validate Product Name--->
		<cfif len(arguments.productName) gt 30>
			<cfset errorMessages.productName = "Value entered is too long , limit is of 30 characters " />
		<cfelseif arguments.productName EQ '' OR NOT isValid('regex',arguments.productName,'[a-z|A-Z]+([" "]?[a-z|A-Z])*') >
			<cfset errorMessages.productName = 'Please provide a valid product Name'/>
		<cfelse>
			<cftry>
				<cfset var qProductNameDetails = application.dbQuery.getProductNameFromDb(arguments.productName)>
	            <cfcatch type="database">
		            <cflog  text="error : #cfcatch.detail#">
            		<cfset errorMessages.productName="Some Error occurred while checking the email. Please, try after sometime!!"/>
	            </cfcatch>
			</cftry>
            <cfif qProductNameDetails.recordCount EQ 1 AND arguments.productName NEQ arguments.previousProductName>
                <cfset errorMessages.productName = "This Product is already present!!">
            </cfif>
		</cfif>

		<!---Validate Product description--->
		<cfif len(arguments.productDescription) gt 100>
			<cfset errorMessages.productDescription = "Value entered is too long , limit is of 100 characters " />
		<cfelseif arguments.productDescription NEQ '' AND NOT isValid("regex", arguments.productDescription, "[a-zA-Z0-9\s,'/.-]*")>
			<cfset errorMessages.productDescription = "Please provide a valid Product Description" />
		</cfif>

		<!---Validate productType --->
		<cfif len(arguments.productType) gt 25>
			<cfset errorMessages.productType = "Value entered is too long , limit is of 25 characters " />
		<cfelseif arguments.productType EQ ''OR NOT isValid("regex", arguments.productType, "[a-z|A-Z]+")>
			<cfset errorMessages.productType = "Please provide a valid product Type" />
		</cfif>

		<!---Validate vehicleType --->
		<cfif len(arguments.vehicleType) gt 25>
			<cfset errorMessages.vehicleType = "Value entered is too long , limit is of 25 characters " />
		<cfelseif arguments.vehicleType EQ ''OR NOT isValid("regex", arguments.vehicleType, "[a-z|A-Z]+")>
			<cfset errorMessages.vehicleType = "Please provide a valid vehicle Type" />
		</cfif>

		<!---Validate employeeRequired --->
		<cfif len(arguments.employeeRequired) gt 10>
			<cfset errorMessages.employeeRequired = "Value entered is too long , limit is of 10 characters " />
		<cfelseif arguments.employeeRequired EQ ''OR NOT isValid("regex", arguments.employeeRequired, "[1-9][0-9]*")>
			<cfset errorMessages.employeeRequired = "Please provide a valid value" />
		</cfif>

		<!---Validate productCost --->
		<cfif len(arguments.productCost) gt 10>
			<cfset errorMessages.productCost = "Value entered is too long , limit is of 10 characters " />
		<cfelseif arguments.productCost EQ ''OR NOT isValid("regex", arguments.productCost, "[1-9][0-9]+")>
			<cfset errorMessages.productCost = "Please provide a valid product Cost" />
		</cfif>

		<!---Validate productDuration --->
		<cfif len(arguments.productDuration) gt 360>
			<cfset errorMessages.productDuration = "Value entered is too long , limit is of 360 characters " />
		<cfelseif arguments.productDuration NEQ ''AND NOT isValid("regex", arguments.productDuration, "[1-9][0-9]*")>
			<cfset errorMessages.productDuration = "Please provide a valid product Duration" />
		</cfif>

		<!---checking the validation error message and inserting user data into db --->
		<cfif StructCount(errorMessages) eq 0>
			 <cfset var res = application.dbQuery.updateProductData(arguments.productID,variables.isPart, variables.vehicleTypeID) />
			 <cfif res eq false>
				 <cfset errorMessages.employeeRequired ='Addition of product is not Successful, try again!!'/>
			</cfif>
		</cfif>

		<cfreturn errorMessages />

</cffunction>

<cffunction name="showWorkOrderDetails" access="remote" output="false" return type="query" returnformat="json">
	<cfargument name="workOrderID" type="string" required="true" />
	<cfset var qWorkOrderDetails = application.dbQuery.getWorkOrderByIDFromDb(arguments.workOrderID)>
	<cfreturn qWorkOrderDetails />
</cffunction>

<cffunction name="getWorkOrderStatus" access="remote" output="false" return type="query" returnformat="json">
	<cfset var qWorkOrderStatus = application.dbQuery.getWorkOrderStatusFromDb()>
	<cfreturn qWorkOrderStatus />
</cffunction>

<cffunction name="updateWorkOrderStatus" access="remote" output="false" return type="boolean" returnformat="json">
	<cfargument name="workOrderStatusValue" type="string" required="true" />
	<cfargument name="workOrderID" type="string" required="true" />
	<cfset var isCommit = application.dbQuery.updateWorkOrderStatusToDb(arguments.workOrderStatusValue, arguments.workOrderID)>
	<cfreturn isCommit />
</cffunction>
</cfcomponent>