<!---
  --- authService
  --- -----------
  ---
  --- this component contains methods for validation of registration, login and logout.
  ---
  --- author: mindfire
  --- date:   3/26/20
  --->

<cfcomponent output="false">

<!---
	Function Name: validateRegisterForm
	date: 8th Apr 2020
	author: Sai Venkat Raavi.
	Functionality: This function contains method to validate register form.
--->
	<cffunction name = "validateRegisterForm" description = "validate register form" access = "remote"
				returnformat = "json" output = "false" returntype = "Struct">
		<cfargument name = "firstName" type = "string" required = "true" />
		<cfargument name = "lastName" type = "string" required = "true" />
		<cfargument name = "email" type = "string" required = "true" />
		<cfargument name = "phoneNumber" type = "string" required = "true" />
		<cfargument name = "userName" type = "string" required = "true" />
		<cfargument name = "password" type = "string" required = "true" />
		<cfargument name = "reenterpassword" type = "string" required = "true" />
		<cfargument name = "personType" type = "string" required = "true" />
		<cfargument name = "companyName" type = "string" required = "true" />
		<cfset var errorMessages = {} />
		<cfset var qCompanyID = application.dbQuery.getCompanyID(arguments.companyName) />
		<cfset var qPersonTypeID = application.dbQuery.getPersonTypeID(arguments.personType) />
		<!---Validate firstName--->
		<cfif len(arguments.firstName) gt 50>
			<cfset errorMessages.firstName = "Value entered is too long , limit is of 50 characters " />
		<cfelseif arguments.firstName EQ ''OR NOT isValid("regex", arguments.firstName, "[a-z|A-Z]+[ ]?[a-z|A-Z]*")>
			<cfset errorMessages.firstName = "Please provide a valid First Name" />
		</cfif>

		<!---Validate lastName--->
		<cfif len(arguments.lastName) gt 25>
			<cfset errorMessages.lastName = "Value entered is too long , limit is of 25 characters " />
		<cfelseif arguments.lastName EQ '' OR NOT isValid("regex", arguments.lastName, "[a-z|A-Z]+")>
			<cfset errorMessages.lastName = "Please provide a valid Last Name" />
		</cfif>


		<!---Validate Email--->
		<cfif len(arguments.email) gt 30>
			<cfset errorMessages.email = "Value entered is too long , limit is of 30 characters " />
		<cfelseif arguments.email EQ '' OR NOT isValid('regex',arguments.email,'\w+([\.+-]?\w+)*@(\w)+([\.-]?\w+)*(\.\w{2,3})+') >
			<cfset errorMessages.email = 'Please provide a valid email'/>
		<cfelse>
			<cftry>
				<cfset var qEmailDetails = application.dbQuery.getEmailFromDb(arguments.email)>
	            <cfcatch type="database">
		            <cflog  text="error : #cfcatch.detail#">
            		<cfset errorMessages.email="Some Error occurred while checking the email. Please, try after sometime!!"/>
	            </cfcatch>
			</cftry>
            <cfif qEmailDetails.recordCount EQ 1>
                <cfset errorMessages.email = "This Email Address has already been registered!!">
            </cfif>
		</cfif>


		<!---Validate Password--->
		<cfif len(arguments.password) gt 15>
			<cfset errorMessages.password = "Value entered is too long , limit is of 15 characters " />
		<cfelseif arguments.password EQ '' OR NOT isValid('regex',arguments.password,'(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,15}') >
			<cfset errorMessages.password = 'Please provide a valid password'/>
		</cfif>


		<!---Validate reeneter Password --->
		<cfif len(arguments.reenterpassword) gt 15>
			<cfset errorMessages.reneterpassword = "Value entered is too long , limit is of 15 characters " />
		<cfelseif arguments.reenterpassword EQ '' OR NOT isValid('regex',arguments.reenterpassword,'(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,15}') >
			<cfset errorMessages.reenterpassword = 'Please provide a valid Confirmation Password'/>
		<cfelseif arguments.reenterpassword neq arguments.password>
			<cfset errorMessages.reenterpassword ='The password and rentered password do not match'/>
		</cfif>


		<!---validate phone number--->
		<cfif len(arguments.phoneNumber) gt 10>
			<cfset errorMessages.phoneNumber = "Value entered is too long , limit is of 15 characters " />
		<cfelseif arguments.phoneNumber EQ '' OR NOT isValid('regex',arguments.phoneNumber,'[2-9][0-9]{9}') >
			<cfset errorMessages.phoneNumber = 'Please provide a valid phone number'/>
		<cfelse>
			<cftry>
				<cfset var qPhoneNumberDetails = application.dbQuery.getPhoneNumberFromDb(arguments.phoneNumber)>
	            <cfcatch type="database">
		            <cflog  text="error : #cfcatch.detail#">
            		<cfset errorMessages.phoneNumber="Some Error occurred while checking the phoneNumber. Please, try after sometime!!"/>
	            </cfcatch>
			</cftry>
            <cfif qPhoneNumberDetails.recordCount EQ 1>
                <cfset errorMessages.phoneNumber = "This Phone Number has already been registered!!">
            </cfif>
		</cfif>

		<!---validate person Type --->
		<cfif arguments.personType EQ '' OR NOT isValid("regex", arguments.personType, "[a-z|A-Z]+")>
			<cfset errorMessages.personType = "Please provide a valid Person Type " />
		</cfif>

		<!---validate company Name --->
		<cfif arguments.companyName EQ '' OR NOT isValid("regex", arguments.companyName, "[a-z|A-Z]+[ ]?[a-z|A-Z]*")>
			<cfset errorMessages.companyName = "Please provide a valid Company Name " />
		</cfif>


		<!--- Validate userName--->
		<cfif len(arguments.userName) gt 25>
			<cfset errorMessages.userName = "Value entered is too long , limit is of 25 characters " />
		<cfelseif arguments.userName EQ '' OR NOT isValid('regex',arguments.userName,'\w+([\.+-]?\w+)*') >
			<cfset errorMessages.userName = 'Please provide a valid user Name'/>
		<cfelse>
			<cftry>
				<cfset var qUserNameDetails = application.dbQuery.getUserNameFromDb(arguments.userName)>
	            <cfcatch type="database">
		            <cflog  text="error : #cfcatch.detail#">
            		<cfset errorMessages.userName="Some Error occurred while checking the User Name. Please, try after sometime!!"/>
	            </cfcatch>
			</cftry>
            <cfif qUserNameDetails.recordCount EQ 1>
                <cfset errorMessages.userName = "This user name has already been used!!">
            </cfif>
		</cfif>

		<!---checking the validation error message and inserting user data into db --->
		<cfif StructCount(errorMessages) eq 0>
			 <cfset var res = application.dbQuery.dataInsertion(qCompanyID.companyID, qPersonTypeID.personTypeID) />
			 <cfif res eq false>
				 <cfset errorMessages.reenterpassword ='Registration not Successful, try again!!'/>
			</cfif>
		</cfif>

		<cfreturn errorMessages />
	</cffunction>


<!---
	Function Name: validateLoginForm
	date: 8th Apr 2020
	author: Sai Venkat Raavi.
	Functionality: This functions contains method to validate login form.
--->
	<cffunction name = "validateLoginForm" description = "validate login form" access = "remote"
				returnformat = "json" output = "false" returntype = "struct">
		<cfargument name = "userName" type = "string" required = "true" />
		<cfargument name = "userPassword" type = "string" required = "true" />
		<cfset var errorMessages = StructNew() />
		<cftry>
			<cfset var qLoginUserDetails = application.dbQuery.getUserDetailsFromDb(arguments.userName,arguments.userPassword)>
			<cfcatch type="database">
				<cflog  text="error : #cfcatch.detail#">
            		<cfset errorMessages.userPassword="Some Error occurred while checking the username/ password. Please, try after sometime!!"/>
			</cfcatch>
		</cftry>
		<!--- Validate userName--->
		<cfif len(arguments.userName) gt 25>
			<cfset errorMessages.userName = "Value entered is too long , limit is of 25 characters " />
		<cfelseif arguments.userName EQ '' OR NOT isValid('regex',arguments.userName,'\w+([\.+-]?\w+)*') >
			<cfset errorMessages.userName = 'Please provide a valid user Name'/>
		</cfif>

		<!---Validate Password--->
		<cfif len(arguments.userPassword) gt 15>
			<cfset errorMessages.userPassword = "Value entered is too long , limit is of 15 characters " />
		<cfelseif arguments.userPassword EQ '' OR NOT isValid('regex',arguments.userPassword,'(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,15}') >
			<cfset errorMessages.userpassword = 'Please provide a valid password'/>
		<cfelseif qLoginUserDetails.recordCount NEQ 1>
			<cfset errorMessages.userpassword = 'Invalid User Name/ password'/>
		<cfelseif qLoginUserDetails.recordCount EQ 1>
			<cflogin>
				<cfloginuser name="#qLoginUserDetails.firstName# #qLoginUserDetails.lastName#" password="#qLoginUserDetails.password#" roles="#qLoginUserDetails.personCategory#" >
			</cflogin>
			<cfset session.sLoginUserDetails = {'userFirstName' = qLoginUserDetails.firstName, 'userLastName' = qLoginUserDetails.lastName, 'userID' = qLoginUserDetails.userID, 'role' = qLoginUserDetails.personCategory} />
		</cfif>

		<cfreturn errorMessages />
	</cffunction>


<!---
	Function Name: doLogout
	date: 8th Apr 2020
	author: Sai Venkat Raavi.
	Functionality: This functions contains method to logout the user.
--->
	<cffunction name = "doLogout" access = "remote" output = "false" returntype = "void">
		<cfset structdelete(session,'sLogInUserDetails') />
		<cflogout />
	</cffunction>


</cfcomponent>