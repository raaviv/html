<cfcomponent output="false">
	<cfset this.datasource = 'chaloGarage' />
	<cfset this.sessionManagement = true />
	<cfset this.sessionTimeout = createTimespan(0,1,0,0) />
	<cfset this.applicationTimeout = createtimespan(0,2,0,0) />

	<cffunction name="OnApplicationStart" returntype="boolean">
		<cfset application.authService = CreateObject("component","chaloGarage.employee.components.authService")>
		<cfset application.dbQuery = CreateObject("component","chaloGarage.employee.components.dbQuery")>
		<cfset application.productManagement = CreateObject("component","chaloGarage.employee.components.productManagement")>

		<cfreturn true>
	</cffunction>


	<cffunction name="onRequestStart" returntype="boolean" >

		<cfargument name="targetPage" type="string" required="true" />
		<!---handle some special URL parameters--->
		<cfif isDefined('url.restartApp')>
			<cfset this.onApplicationStart() />
		</cfif>

		<cfif isDefined('url')>
			<cfset this.onApplicationStart() />
		</cfif>
		<!---Implement ressource Access control for the 'admin' folder--->
		<cfreturn true />
	</cffunction>

</cfcomponent>