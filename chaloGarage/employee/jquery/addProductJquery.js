$(document).ready(function(){   //validate every field on click 
    $("#addProductButton").on("click",function(){
    	var arr= [validateProductName(),validateProductDescription(),validateProductType(),validateVehicleType(),
            validateProductCost(),validateProductDuration(),validateEmployeeRequired()];
    	var clientValidationReg = true, serverValidationReg=false;
        for(var i=0; i<arr.length; i++)
        {
            
            if(arr[i]==false){
            	clientValidationReg = false;
            	break;
            }
        }
        if(clientValidationReg == true){
        	$.ajax({
    		    type:"POST",
    		    url:"components/productManagement.cfc",
    		    data: {
    		    	  "method":"addProductValidation",
    		          "productName": $("#productName").val(),
    		          "productDescription": $("#productDescription").val(),
    		          "productType": $("#productTypeDiv select option:selected").val(),
    		          "vehicleType": $("#vehicleTypeDiv select option:selected").val(),
    		          "productCost": $("#productCost").val(),
    		          "productDuration": $("#productDuration").val(),
    		          "employeeRequired": $("#employeeRequired").val(),
    		      	},
    		    error: function(){
                      swal({
                          title: "Product Addition is not Done!!",
                          text: "Some error occured. Please try after sometime",
                          icon: "error",
                          button: "Ok",
                      });
                  },
    		    success: function(errorMessages) {
    			    	if(errorMessages != "{}"){
    				    	var errorMessages = JSON.parse(errorMessages);
    				    	$("#productNameDiv .errorMsg").html(errorMessages.PRODUCTNAME);
    				    	$("#productDescriptionDiv .errorMsg").html(errorMessages.PRODUCTDESCRIPTION);
    				    	$("#productTypeDiv .errorMsg").html(errorMessages.PRODUCTTYPE);
    				    	$("#vehicleTypeDiv .errorMsg").html(errorMessages.VEHICLETYPE);
    				    	$("#productCostDiv .errorMsg").html(errorMessages.PRODUCTCOST);
    				    	$("#productDurationDiv .errorMsg").html(errorMessages.PRODUCTDURATION);
    				    	$("#employeeRequiredDiv .errorMsg").html(errorMessages.EMPLOYEEREQUIRED);
    			    	}
    			    	else if(errorMessages == "{}"){
    			    		serverValidationReg = true;
    			    		window.location.href = 'https://assignmentpractice.com/chaloGarage/employee/addProduct.cfm';	 
    			    	}
    		    }
    		});
        }
        return (serverValidationReg && clientValidationReg);
    });  
    $("#logout").on("click",function(){
    	$.ajax({
		    type:"POST",
		    url:"components/authService.cfc",
		    data: {
		    	  "method":"doLogout",
		      },
		    error: function(){
                  swal({
                      title: "Failed to Logout!!",
                      text: "Some error occured. Please try after sometime",
                      icon: "error",
                      button: "Ok",
                  });
              },
		    success: function() {
		    	window.location.href = 'index.cfm';
		    }
		});
    });  
    $('#productName').on("change keyup",validateProductName); //event handling change and keyup
    $('#productDescription').on("change keyup",validateProductDescription);
    $("#productTypeDiv select ").on("change keyup",validateProductType);
    $("#vehicleTypeDiv select ").on("change keyup",validateVehicleType);
    $("#productCost").on("change keyup",validateProductCost);
    $("#productDuration").on("change keyup",validateProductDuration);
    $("#employeeRequired").on("change keyup",validateEmployeeRequired);
});
function repeatvalidate(temp1,temp2,temp3, msg, color, show,decide){
    temp1.css("visibility",show);
    temp2.css("border-color",color);
    temp3.fadeIn('fast', function(){temp3.html(msg);});
    return decide;
}
//validate function for fields
function validateexpression(regex,content,id, alertClass, errorClass,contentLength,requiredLength){
    var temp2= $("#"+id+" ."+ "similarClassForValidation"),temp1 =$("#"+id+" ."+ alertClass),temp3 =$("#"+id+" ."+ errorClass) ;
    if(contentLength>requiredLength){
        return repeatvalidate(temp1,temp2,temp3,"Input entered too long , limit is of "+requiredLength+ " characters" , "rgb(255,99,71)", "visible",false);
    }
    else if(content == "" || content == undefined){
        return repeatvalidate(temp1,temp2,temp3,"Value cannot be empty", "rgb(255,99,71)", "visible",false);
    }
    else if(content != "" && regex.test(content) == false){
        return repeatvalidate(temp1,temp2,temp3,"Entered value is invalid and is not in the required format", "rgb(255,99,71)", "visible",false);
    }
    else if(content != "" && regex.test(content) == true){
        return repeatvalidate(temp1,temp2,temp3,"", "rgb(204,204,204)", "hidden",true);
    } 
}
function validatealtexpression(regex,content,id, alertClass, errorClass,contentLength,requiredLength){
    var temp2= $("#"+id+" ."+ "similarClassForValidation"),temp1 =$("#"+id+" ."+ alertClass),temp3 =$("#"+id+" ."+ errorClass) ;
    if(contentLength>requiredLength){
        return repeatvalidate(temp1,temp2,temp3,"Input entered too long , limit is of "+requiredLength+ " characters" , "rgb(255,99,71)", "visible",false);
    }
    else if(content == undefined){
        return repeatvalidate(temp1,temp2,temp3,"Value cannot be empty", "rgb(255,99,71)", "visible",false);
    }
    else if(content != "" && regex.test(content) == false){
        return repeatvalidate(temp1,temp2,temp3,"Entered value is invalid and is not in the required format", "rgb(255,99,71)", "visible",false);
    }
    else if(content == "" || regex.test(content) == true){
        return repeatvalidate(temp1,temp2,temp3,"", "rgb(204,204,204)", "hidden",true);
    } 
}
function validateProductName(){ //validate function for first name
    var productNameRegex = /^[a-z|A-Z]+([" "]?[a-z|A-Z])*$/;
    var content = $("#productName").val().trim();
    return validateexpression(productNameRegex, content,"productNameDiv","alertIcon","errorMsg",content.length,30);
}
function validateProductDescription(){ //validate function for last name
    var productDescriptionRegex = /^[a-zA-Z0-9\s,'/.-]*$/;
    var content = $("#productDescription").val().trim();
    return validatealtexpression(productDescriptionRegex, content,"productDescriptionDiv","alertIcon","errorMsg",content.length,100);
}
function validateProductCost(){   //validate function for email
    var productCostRegex = /^[1-9][0-9]+$/;
    var content = $("#productCost").val().trim();
    return validateexpression(productCostRegex,content,"productCostDiv","alertIcon","errorMsg",content.length,10);
}
function validateProductDuration(){   //validate function for password
    var productDurationRegex =/^[1-9][0-9]*$/;
    var content = $("#productDuration").val().trim();
    return validatealtexpression(productDurationRegex,content,"productDurationDiv","alertIcon","errorMsg",content.length,360);
}
function validateEmployeeRequired(){     //validate function for phone number
    var employeeRequiredRegex = /^[1-9][0-9]*$/;
    content = $("#employeeRequired").val().trim();
    return validateexpression(employeeRequiredRegex, content, "employeeRequiredDiv","alertIcon","errorMsg",content.length,10);
}
function validateProductType(){
	var productTypeRegex = /^[a-z|A-Z]+$/;
	var content = $("#productTypeDiv select option:selected").val();
    return validateexpression(productTypeRegex, content,"productTypeDiv","alertIcon","errorMsg",content.length,25);
}
function validateVehicleType(){
	var vehicleTypeRegex = /^[a-z|A-Z]+$/;
	var content = $("#vehicleTypeDiv select option:selected").val();
    return validateexpression(vehicleTypeRegex, content,"vehicleTypeDiv","alertIcon","errorMsg",content.length,25);
}
