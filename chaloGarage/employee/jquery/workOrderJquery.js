$(document).ready(function(){  
	$(document).on("click",".rectangle .viewButton ", function(){
		var workOrderIDString = $(this).parent(".rectangle").find(".workOrderID").text();
		var CustomerString = $(this).parent(".rectangle").find(".Customer").text();
		var VehicleString = $(this).parent(".rectangle").find(".vehicle").text();
		var ProductString = $(this).parent(".rectangle").find(".product").text();
		var workOrderIDStringToArray = workOrderIDString.split(":")
		var workOrderID = workOrderIDStringToArray[1];
		var CustomerStringToArray = CustomerString.split(":")
		var CustomerName = CustomerStringToArray[1];
		var VehicleStringToArray = VehicleString.split(":")
		var VehicleName = VehicleStringToArray[1];
		var ProductStringToArray = ProductString.split(":")
		var product = ProductStringToArray[1];
		$(".displayWorkOrders").css("display","none");
		$(".viewWorkOrderDetail").css("display","block");
		$.ajax({
			type:"POST",
		    url:"components/productManagement.cfc",
		    data: {
		    	  "method":"showWorkOrderDetails",
		          "workOrderID": workOrderID
		      	},
		    error: function(){
                  swal({
                      title: "Failed to Load Details!!",
                      text: "Some error occured. Please try after sometime",
                      icon: "error",
                      button: "Ok",
                  });
              },
		    success: function(query) {
		    	$(".viewWorkOrderDetail #employeeDiv").find(".headingValue").html("");
		    	console.log(query);
		    	var query = JSON.parse(query);
		    	console.log(query);
		    	for(var i = 0 ; i < query.DATA.length; i++){
		    		if(i == query.DATA.length - 1 ){
		    			$(".viewWorkOrderDetail #employeeDiv").find(".headingValue").append( "<br>"+query.DATA[i][0]+ ' '+ query.DATA[i][1]);
		    		}
		    		else{
		    			$(".viewWorkOrderDetail #employeeDiv").find(".headingValue").append( "<br>"+ query.DATA[i][0]+ ' '+ query.DATA[i][1] + ' , ');
		    		}
		    	}
		    	$(".viewWorkOrderDetail #workOrderStatusDiv").find(".headingValue").html("<br>"+query.DATA[0][2]);
		    	$(".viewWorkOrderDetail #addressDiv").find(".headingValue").html( " <br>  "+query.DATA[0][3] + " <br> " + 
		    			query.DATA[0][4] + " <br> " + query.DATA[0][5] + " , " + query.DATA[0][6] + " , " + query.DATA[0][7] + " <br> " +
		    			query.DATA[0][8] );
		    	$(".viewWorkOrderDetail #workOrderIDDiv").find(".headingValue").html("<br>"+ workOrderID);
		    	$(".viewWorkOrderDetail #CustomerNameDiv").find(".headingValue").html("<br>"+ CustomerName);
		    	$(".viewWorkOrderDetail #vehicleDiv").find(".headingValue").html("<br>"+ VehicleName);
		    	$(".viewWorkOrderDetail #productDiv").find(".headingValue").html("<br>"+ product);
		    	
		    }
		})
	})
	$("#workOrderStatusDiv select").on("focus",function(){
			$.ajax({
	    		type:"POST",
			    url:"components/productManagement.cfc",
			    data: {
			    	  "method":"getWorkOrderStatus"
			      },
			    error: function(){
	                  swal({
	                      title: "Work Order Status Fetch Failed!!",
	                      text: " Please try after sometime",
	                      icon: "error",
	                      button: "Ok",
	                  });
	              },
			    success: function(query) {
			    	var query = JSON.parse(query);
			    	console.log(query);
			    	$("#workOrderStatusDiv select").html("<option value=''> --- Select --- </option>");
			    	for(i=0; i<query.DATA.length; i++){
			    		$("#workOrderStatusDiv select").append("<option value='"+query.DATA[i]+"'>"+query.DATA[i]+"</option>");
			    	}
			    }
	    	})
		})
		
		$("#UpdateStatusButton").on("click",function(){
			var workOrderID = $("#workOrderIDDiv .headingValue").text();
			console.log(workOrderID);
			$.ajax({
	    		type:"POST",
			    url:"components/productManagement.cfc",
			    data: {
			    	  "method":"updateWorkOrderStatus",
			    	  "workOrderStatusValue" : $("#workOrderStatusDiv select option:selected").val(),
			    	  "workOrderID": workOrderID
			      },
			    error: function(){
	                  swal({
	                      title: "Update Work Order Status Failed!!",
	                      text: " Please try after sometime",
	                      icon: "error",
	                      button: "Ok",
	                  });
	              },
			    success: function(isCommit) {
			    	var isCommit = JSON.parse(isCommit);
			    	console.log(isCommit);
			    	if(isCommit == false){
			    		swal({
		                      title: "Update Work Order Status Failed!!",
		                      text: " Please try after sometime",
		                      icon: "error",
		                      button: "Ok",
		                  });
			    	}
			    	else{
			    		swal({
			    			  title: "Work Order Status Completed!",
			    			  text: "Successfully updated Work Order Status!",
			    			  icon: "success",
			    			  button: "Ok",
			    			});
			    	}
			    }
	    	})
		})
	$("#backButton").on("click", function(){
		$(".displayWorkOrders").css("display","block");
		$(".viewWorkOrderDetail").css("display","none");
	})
    $("#logout").on("click",function(){
    	$.ajax({
		    type:"POST",
		    url:"components/authService.cfc",
		    data: {
		    	  "method":"doLogout",
		      },
		    error: function(){
                  swal({
                      title: "Failed to Logout!!",
                      text: "Some error occured. Please try after sometime",
                      icon: "error",
                      button: "Ok",
                  });
              },
		    success: function() {
		    	window.location.href = 'https://assignmentpractice.com/chaloGarage/common/index.cfm';
		    }
		});
    });  
});
