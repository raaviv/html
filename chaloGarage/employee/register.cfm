<cfif isDefined("session.sLogInUserDetails") AND StructKeyExists(session.sLogInUserDetails,"userID")>
	<cflocation url = "https://assignmentpractice.com/chaloGarage/common/index.cfm">
</cfif>
<cfset variables.company = application.dbQuery.getCompanyDetails()>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/registerLoginStyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script type = "text/javascript"
         src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
      <script src="jquery/registerLoginJquery.js"></script>
    </head>
    <body>
        <cfinclude  template="includes/header.cfm">
        <div id="boxdiv">
            <div id="header">Employee Sign Up Form</div>
            <div id="formdiv">
                <!-- register form -->
                <form name="registerform" id="registerForm" action="https://assignmentpractice.com/chaloGarage/common/login.cfm" target="" method="POST">
                    <div class="namediv" id="firstnamediv">
                        <label class="sideheading">First Name<span class="asterik">*</span></label>
                        <span class="tooltip">1) should contain only alphabets, middle name can be included in first name.
                        <br>2) should not contain any special characters (ex:"-" , "  '  " , "." , ",")</span>
                        <input class="similarclassforvalidation" id="firstname" type="text" name="firstname" value=""
                        placeholder="First name"  >
                        <i  class="fa fa-exclamation-triangle alerticon"></i>
						<span class="errormsg"></span>
                    </div>
                        <!--last name block-->
                    <div class="namediv" id="lastnamediv">
                        <label class="sideheading">Last Name<span class="asterik">*</span></label>
                        <span class="tooltip">1) should contain only alphabets.<br>
                        2) should not contain any special characters (ex:"-" , "  '  " , "." , ",")</span>
                        <input class="similarclassforvalidation" id="lastname" type="text" name="lastname" value=""
                        placeholder="Last name" >
                        <i  class="fa fa-exclamation-triangle alerticon"></i>
                        <span class="errormsg"></span>
                    </div>
                    <!--Email block-->
                    <div class="namediv" id="emaildiv">
                        <label class="sideheading">E-mail<span class="asterik">*</span></label>
                        <input class="similarclassforvalidation" id="email" type="text" name="email" value=""
                        placeholder="E-mail"  >
                        <i  class="fa fa-exclamation-triangle alerticon"></i>
                        <span class="tooltip">1) should contain value before and after '@' <br>
                        2) should contain value after '.' (Ex:abc123@xyz.com)</span>
                        <span class="errormsg"></span>
                    </div>
                    <!--phone number block-->
                    <div class="namediv" id="phonenumberdiv">
                        <label class="sideheading"> Phone No<span class="asterik">*</span></label>
                        <input class="similarclassforvalidation" id="phonenumber" type="text" name="phonenumber"
                        value="" placeholder="Phone Number" >
                        <i  class="fa fa-exclamation-triangle alerticon"></i>
                        <span class="tooltip">should contain 10 digits and should not start with 0 or 1.</span>
                        <span class="errormsg"></span>
                    </div>
					<!--employee type-->
                    <div class="namediv" id="employeetypediv">
                        <label class="sideheading"> Employee Type<span class="asterik">*</span></label>
                        <select name="employeetype" class="similarclassforvalidation">
							<option value="" >--- Select --- </option>
							<option value="staff" >Staff</option>
							<option value="worker" >Worker</option>
                        </select>
                        <i  class="fa fa-exclamation-triangle alerticon"></i>
                        <span class="tooltip">should input the type of employee.</span>
                        <span class="errormsg"></span>
                    </div>
					<!--employee type-->
                    <div class="namediv" id="companydiv">
                        <label class="sideheading">Company Name<span class="asterik">*</span></label>
                        <select name="company" class="similarclassforvalidation">
							<option value="" >--- Select ---</option>
							<cfoutput query="company">
							<option VALUE="#company.companyName#">#company.companyName#</option>
							</cfoutput>
                        </select>
                        <i  class="fa fa-exclamation-triangle alerticon"></i>
                        <span class="tooltip">should input the type of employee.</span>
                        <span class="errormsg"></span>
                    </div>
					<!--User name block -->
					<div class="namediv" id="usernamediv">
                        <label class="sideheading">User Name<span class="asterik">*</span></label>
                        <span class="tooltip">user name is same as email entered for registering, except username contains the value before '@' in email </span>
                        <input class="similarclassforvalidation" id="username" type="text" name="username" value=""
                        placeholder="User name"  >
                        <i  class="fa fa-exclamation-triangle alerticon"></i>
                        <span class="errormsg"></span>
                    </div>
                    <!--password block-->
                    <div class="namediv" id="passworddiv">
                        <label class="sideheading">Password<span class="asterik">*</span></label>
                        <input class="similarclassforvalidation" id="password" type="password" name="password"
                        value="" placeholder="Create Your Password" >
                        <i  class="fa fa-exclamation-triangle alerticon"></i>
                        <span class="tooltip">password length should be b/w 6-15 and should contain at
                        least one lowercase letter, one uppercase letter and one numeric digit.</span>
                        <span class="errormsg"></span>
                    </div>
                    <!--re-enter password block-->
                    <div class="namediv" id="reenterpassworddiv">
                        <label class="sideheading"> Re-enter Password<span class="asterik">*</span></label>
                        <input class="similarclassforvalidation" id="reenterpassword" type="password"
                        name="reenterpassword" value="" placeholder="Re-enter Password" >
                        <i  class="fa fa-exclamation-triangle alerticon"></i>
                        <span class="tooltip">1) password length should be b/w 6-15 and should contain at
                            least one lowercase letter, one uppercase letter and one numeric digit.<br>
                            2) enter the same above filled password.</span>
                        <span class="errormsg"></span>
                    </div>
                    <!--Submit button-->
                    <button type="submit" name="registerbutton" id="registerbutton">Create Account</a></button>
                    <!--Required details Block-->
                    <div class="requireddetails">
                        <p>(<span class="asterik">*</span>) - Value is required.
                        Hover over <span class="asterik"> * </span>present fields to know details about them</p>
                        <p>Already Have An Account? <a href="login.cfm">Sign In</a></p>
                    </div>
                </form>
            </div>
        </div>
        <cfinclude  template="includes/footer.cfm">
    </body>
</html>