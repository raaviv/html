<cfif NOT isDefined("session.sLoginUserDetails") OR NOT session.sLoginUserDetails["role"] EQ 'staff'>
	<cflocation url = "https://assignmentpractice.com/chaloGarage/common/index.cfm">
</cfif>
<cfset variables.productDetails = application.dbQuery.getProductFromDb()>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/editProductStyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script type = "text/javascript"
         src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>
      <script src="jquery/editProductJquery.js"></script>
    </head>
    <body>
		<cfinclude  template="includes/header.cfm">
		<div class="imageContainer" style="background-image: url(../common/images/bike_cars.jpg); height: auto;">
		<div class="container">
		<div class="row">
            <div class="column">
                <div class="containerBox">
                    <div class="card cardShadow" id="wizard">
					<div class="displayProducts">
						<div class="headingDiv">
							<h3 class="mainHeading"><span><img class="icon" src="../common/images/icons/editIcon.png"></span>Edit Products
							<span class="searchBar"><input type="text" id="searchBar" name="productName" value="" placeholder = " Search for Product Names" >
							<span class="searchIcon"><img id="searchIcon" src="../common/images/icons/searchIcon.png"></span>
							</span>
							</h3>
						</div>
						<div class="miniHeading"><h3>Product Details</h3></div>
						<div class="displayContent">
							<cfoutput query="productDetails" maxRows = "5">
							<div class="rectangle">
								<h4 >Product Name: #productDetails.productName#</h4>
								<div class="fieldDiv">
								<h4 class="productID"> Product ID: #productDetails.productID#</h4>
								<h4 class="productCost"> Product Cost: #productDetails.price# Rupees</h4>
								</div>
								<div class="fieldDiv">
								<cfif #productDetails.isPart# EQ 1>
									<h4 class="productType"> Product Type: Part</h4>
								<cfelse>
									<h4 class="productType"> Product Type: Service</h4>
								</cfif>
								<h4 class="vehicleType"> vehicle Type: 	#productDetails.vehicleCategory#</h4>
								</div>
								<button type="button" class="updateProductButton"> Update Product</button>
								<button type="button" class="deleteProductButton"> Delete Product</button>
							</div>
							</cfoutput>
					    </div>
					</div>
					<div class="updateProduct">
						<div class="headingDiv">
							<h3 class="mainHeading"><span><img class="icon" src="../common/images/icons/editIcon.png"></span>Update Product</span>
							</h3>
						</div>
						<cfoutput></cfoutput>
						<form name="updateProductForm" id="updateProductForm" action="" target="" method="POST">
		                    <div class="nameDiv" id="productNameDiv">
		                        <label class="sideHeading">Product Name <span class="asterik">*</span></label>
		                        <span class="toolTip">1) should contain only alphabets.
		                        <br>2) should not contain any special characters (ex:"-" , "  '  " , "." , ",")</span>
		                        <input class="similarClassForValidation" id="productName" type="text" name="productName"
								value="" placeholder="Product Name" >
		                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
								<span class="errorMsg"></span>
		                    </div>
		                        <!--last name block-->
		                    <div class="nameDiv" id="productDescriptionDiv">
		                        <label class="sideHeading">Product Description</label>
		                        <span class="toolTip">1) should contain only alphabets.<br>
		                        2) should not contain any special characters (ex:"-" , "  '  " , "." , ",")</span>
		                        <input class="similarClassForValidation" id="productDescription" type="text" name="productDescription"
								value="" placeholder="Product Description" >
		                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
		                        <span class="errorMsg"></span>
		                    </div>
		                    <!--Email block-->
		                    <div class="nameDiv" id="productTypeDiv">
		                        <label class="sideHeading">Product Type<span class="asterik">*</span></label>
		                        <select name="productType" class="similarClassForValidation">
									<option value="" >--- Select --- </option>
									<option value="service" >Service</option>
									<option value="part" >Part</option>
		                        </select>
		                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
		                        <span class="toolTip"> Select the type of product.</span>
		                        <span class="errorMsg"></span>
		                    </div>

		                    <div class="nameDiv" id="vehicleTypeDiv">
		                        <label class="sideHeading">Vehicle Type<span class="asterik">*</span></label>
		                        <select name="vehicleType" class="similarClassForValidation">
									<option value="" >--- Select --- </option>
									<option value="car" >Car</option>
									<option value="bike" >Bike</option>
		                        </select>
		                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
		                        <span class="toolTip"> Select the type of product.</span>
		                        <span class="errorMsg"></span>
		                    </div>

		                    <!--phone number block-->
		                    <div class="nameDiv" id="productCostDiv">
		                        <label class="sideHeading"> Product Cost <span class="asterik">*</span></label>
		                        <input class="similarClassForValidation" id="productCost" type="text" name="productCost"
		                        value="" placeholder="Product Cost " >
		                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
		                        <span class="toolTip">should contain only digits</span>
		                        <span class="errorMsg"></span>
		                    </div>
							<!--User name block -->
							<div class="nameDiv" id="productDurationDiv">
		                        <label class="sideHeading">Product Duration </label>
		                        <span class="toolTip"> duration should be in hours </span>
		                        <input class="similarClassForValidation" id="productDuration" type="text" name="productDuration"
								value="" placeholder="Product Duration (in hours)"  >
		                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
		                        <span class="errorMsg"></span>
		                    </div>
		                    <!--password block-->
		                    <div class="nameDiv" id="employeeRequiredDiv">
		                        <label class="sideHeading">No of Employee Required<span class="asterik">*</span></label>
		                        <input class="similarClassForValidation" id="employeeRequired" type="text" name="employeeRequired"
		                        value="" placeholder="Number of Employee Required" >
		                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
		                        <span class="toolTip">Should be mentioned in digits.</span>
		                        <span class="errorMsg"></span>
		                    </div>
		                    <button type="submit" id="updateProductButton">Update Product Details</button>
						</form>
					</div>
					</div>
				</div>
			</div>
		</div>
		</div>
		</div>
			<cfinclude  template="includes/footer.cfm">
			</body>
