<!---
Author: R sai Venkat
Date: 4/5/2020
Description: Book Services page
--->
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/bookServiceStyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<script type = "text/javascript"
         src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>
      <script src="jquery/bookServiceJquery.js"></script>
    </head>

    <body>
        <cfinclude  template="includes/header.cfm">
		<div class="imageContainer" style="background-image: url(images/bike_cars.jpg); height: auto;">
		<div class="container" id="body">
          <div class="row">
            <div class="column">
                <div class="containerBox">
                    <div class="card cardShadow" id="wizard">
                      <form id="bookServicesForm" name="bookServicesForm" action="#checkoutForm" target="" method="POST">
                          <div class="headingDiv">
                              <h3 class="mainHeading">
                                Book a Service
                              </h3>
                  			  <h5 id="mainHeading">
                   				 Please select your vehicle type to get it started.
						      </h5>
                          </div>
                		<div class="cardNavigation" id="cardNavigation">
                 			<ul class="nav navPills">
                                  <li  class=" vehicleTab active"><a href="#" target = "_self">Vehicle</a></li>
                                  <li  class=" modelTab "><a href="#" target = "_self">Model</a></li>
                                  <li  class="servicesTab"><a href="#" target = "_self">Service</a></li>
                              </ul>
						</div>
                        <div class="tabContent">
                                <div class="tabPane active" id="vehicle">
                                    <h4 class="infoText">What type of vehicle do you have? </h4>
                                    <div class="row">
										<span class="errorMsg"></span>
                                                <div class="choice ">
													<label id="bikeLabel">
                                                    <input type="radio" name="vehicleType" id="bike" value="bike">
                                                    <div class="icon">
                                                        <img src="images/icons/bike1.jpg">
                                                    </div>
                                                    <h4>Bike</h4>
													</label>
                                                </div>
                                                <div class="choice ">
												    <label id="carLabel">
                                                    <input type="radio" name="vehicleType" id="car" value="car">
                                                    <div class="icon">
                                                        <img src="images/icons/car1.png">
                                                    </div>
                                                    <h4>Car</h4>
													</label>
                                                </div>
												<button type="button" class="next">Next</button>
                                    </div>
                                </div>
                                <div class="tabPane" id="model">
                                    <h4 class="infoText">Describe Your Vehicle and Product. </h4>
                                    <div class="row">
                                        <div class="nameDiv">
                                          <div class="vehicleCompanyDiv">
                                              <label class="sideHeading">Vehicle Company</label>
                                              <select class="selectDiv" name="vehicleCompany" id="vehicleCompany" >
                                                  <option value="">--- Select ---</option>
                                              </select>
										  </div>
                                        </div>

                                        <div class="nameDiv">
                                          <div class="vehicleModelDiv">
                                              <label class="sideHeading">Vehicle Model</label>
                                              <select class="selectDiv" name="vehicleModel" id="vehicleModel" >
                                              	<option value="">--- Select ---</option>
                                              </select>
                                          </div>
                                        </div>

										<div class="nameDiv">
                                          <div class="productTypeDiv">
                                              <label class="sideHeading">Product Type</label>
                                              <select class="selectDiv" name="productType" id="productType" >
                                              	<option value="">--- Select ---</option>
												<option value="part">Part</option>
												<option value="service">Service</option>
                                              </select>
                                          </div>
                                        </div>
										<div class="nameDiv">
										<button type="button" class="previous">Previous</button>
										<button type="button" class="next">Next</button>
										</div>
                                    </div>
                                </div>
                                <div class="tabPane" id="service">
                    			<div class="row">
                      				<h4 class="infoText"> What services are you looking for ?</h4>
                      				<div class="row">
                        			<div class="col-xs-offset-1 col-sm-offset-2 col-sm-8 col-xs-10" id="servicesContainer">
                                    <div class="hidden checkbox">
										<label>
                              			<input type="checkbox" name="productCheckbox" value="other">
  										 Others
										</label>
										<span class="productCost"></span>
                          			</div>
                        			</div>
									<span class="errorMsg"></span>
									</div>
                      				</div>
								<button type="button" class="previous">Previous</button>
								<button type="submit" class="next" id="checkOutButton">Check Out</button>
                  			   </div>
						</div>
					</form>

					<form id="checkoutForm" name="checkoutForm" action="" target="" method="POST">
                          <div class="headingDiv">
                              <h3 class="mainHeading">
                                Checkout Page
                              </h3>
                  			  <h5 id="mainHeading">
                   				 Please verify the services and proceed to buy.
						      </h5>
                          </div>
                		<div class="nameDiv leftMargin vehicleName">
							<label>Vehicle:</label>
							<span class="labelValue"></span>
							<span class="errorMsg"></span>
						</div>
						<div class="nameDiv leftMargin vehicleNumberPlate">
							<label>Vehicle Number:</label>
							<select class="selectDiv" name="vehicleNumber" id="vehicleNumber" >
                                <option value="">--- Select ---</option>
                            </select>
							<span class="errorMsg"></span>
						</div>
						<div class="nameDiv leftMargin productsBrought">
							<label>Products Brought:</label>
						</div>
						<div class="displayContent">
						</div>
						<div class="nameDiv leftMargin totalCost">
							<label> Total Cost: </label>
							<span class="labelValue"></span>
							<span class="errorMsg"></span>
						</div>
						<div class = "nameDiv leftMargin addressDiv">
						<label>Deliver to:</label>
						<select class="selectDiv" name="addressType" id="addressType" >
                                <option value="primary">Primary Address</option>
								<option value="alternate">Alternate Address</option>
                            </select>
							<span class="errorMsg"></span>
						</div>
						<div class="nameDiv">
						<button class="buyButton" type="submit">Buy Products</button>
						</div>
					</form>
                  </div>
                </div>
            </div>
          </div>
      </div>
		</div>
		<cfinclude  template="includes/footer.cfm">
    </body>
</html>
