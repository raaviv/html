<!DOCTYPE html>
<html lang = "en" dir="ltr">
  <head>
    <meta charset = "utf-8">
    <title></title>
    <link rel = "stylesheet" href = "css/missingTemplateStyle.css">
  </head>
  <body>
    <div class = "container">
      <h2>Oops! Page not found.</h2>
      <h3>Error:404</h3>
      <p>We can't find the page you're looking for. Check URL for any mistakes</p>
      <a href = "Index.cfm">Go back to Home Page</a>
    </div>
  </body>
</html>