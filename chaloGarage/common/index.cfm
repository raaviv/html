<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/indexStyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script type = "text/javascript"
         src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>
      <script src="jquery/registerLoginJquery.js"></script>
    </head>

    <body>
        <cfinclude  template="includes/header.cfm">
		<div class="container">
		  	<img src="images/bike_cars.jpg" alt="bike cars photo" class="image">
		 	<div class="centered">
			<h2 style="font-size:30px;">Chalo Garage</h2>
	        <p style="font-size:20px;">Get your bike or car serviced</p>
			<button type="button" class="bookServiceButton"><a href="bookService.cfm">Book a Service</a></button>
		    </div>
		    <cfif isDefined("session.sLoginUserDetails") AND session.sLoginUserDetails["role"] EQ 'staff'>
				<div class="dashBoard">
					<div class="container">
						<div class="boxDiv">
							<div class="addProductDiv">
								<div class="imageContainer"><img src="images/icons/addIcon.png"></div>
								<button class="btn addProductBtn" type="button"> <a href="../employee/addProduct.cfm">Add Product</a>
								</button>
							</div>
						</div>
						<div class="boxDiv">
							<div class="editProductDiv">
								<div class="imageContainer"><img src="images/icons/editIcon.png"></div>
								<button class="btn editProductBtn" type="button"> <a href="../employee/editProduct.cfm">Edit Product</a>
								</button>
							</div>
						</div>
						<div class="boxDiv">
							<div class="workOrderDiv">
								<div class="imageContainer"><img src="images/icons/viewIcon.png"></div>
								<button class="btn viewWorkOrdersBtn" type="button"> <a href="../employee/workOrder.cfm">View Work Orders</a>
								</button>
							</div>
						</div>
					</div>
				</div>
			<cfelseif isDefined("session.sLoginUserDetails") AND session.sLoginUserDetails["role"] EQ 'worker'>
				<div class="dashBoard">
					<div class="container">
						<div class="boxDiv">
							<div class="workOrderDiv">
								<div class="imageContainer"><img src="images/icons/updateIcon.png"></div>
								<button class="btn viewWorkOrdersBtn" type="button"> <a href="../employee/updateWorkOrderStatus.cfm">Update Work Order Status</a>
								</button>
							</div>
						</div>
					</div>
				</div>
			</cfif>
		</div>
<cfinclude  template="includes/footer.cfm">
    </body>
</html>