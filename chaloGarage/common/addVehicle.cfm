<cfif NOT isDefined("session.sLoginUserDetails")>
	<cflocation url = "https://assignmentpractice.com/chaloGarage/common/index.cfm">
</cfif>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/addVehicleStyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script type = "text/javascript"
         src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>
      <script src="jquery/addVehicleJquery.js"></script>
    </head>
    <body>
		<cfinclude  template="includes/header.cfm">
		<div class="imageContainer" style="background-image: url(../common/images/bike_cars.jpg); height: auto;">
		<div class="container">
		<div class="row">
            <div class="column">
                <div class="containerBox">
                    <div class="card cardShadow" id="wizard">
						<div class="headingDiv">
							<h3 class="mainHeading"><span><img class="icon" src="../common/images/icons/addIcon.png"></span>Add My Vehicle</h3>
						</div>
						<form name="addVehicleForm" id="addVehicleForm" action="" target="" method="POST">

							<div class="nameDiv" id="vehicleTypeDiv">
		                        <label class="sideHeading">Vehicle Type<span class="asterik">*</span></label>
		                        <select name="vehicleType" class="similarClassForValidation">
									<option value="" >--- Select --- </option>
									<option value="car" >Car</option>
									<option value="bike" >Bike</option>
		                        </select>
		                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
		                        <span class="toolTip"> Select the type of vehicle.</span>
		                        <span class="errorMsg"></span>
		                    </div>

							<div class="nameDiv" id="vehicleCompanyDiv">
		                        <label class="sideHeading">Vehicle Company<span class="asterik">*</span></label>
		                        <select name="vehicleCompany" class="similarClassForValidation">
									<option value="" >--- Select --- </option>
		                        </select>
		                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
		                        <span class="toolTip"> Select the type of vehicle Company</span>
		                        <span class="errorMsg"></span>
		                    </div>

		                    <div class="nameDiv" id="vehicleModelDiv">
		                        <label class="sideHeading">Vehicle Model<span class="asterik">*</span></label>
		                        <select name="vehicleModel" class="similarClassForValidation">
									<option value="" >--- Select --- </option>
		                        </select>
		                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
		                        <span class="toolTip"> Select the type of vehicle Model</span>
		                        <span class="errorMsg"></span>
		                    </div>


							<div class="nameDiv" id="vehicleDescriptionDiv">
		                        <label class="sideHeading">vehicle Description</label>
		                        <span class="toolTip">1) should contain only alphabets.<br>
		                        2) should not contain any special characters (ex:"-" , "  '  " , "." , ",")</span>
		                        <input class="similarClassForValidation" id="vehicleDescription" type="text" name="vehicleDescription"
								value="" placeholder="Vehicle Description" >
		                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
		                        <span class="errorMsg"></span>
		                    </div>


		                    <div class="nameDiv" id="vehicleNumberPlateDiv">
		                        <label class="sideHeading"> Vehicle Number Plate <span class="asterik">*</span></label>
		                        <input class="similarClassForValidation" id="vehicleNumberPlate" type="text" name="vehicleNumberPlate"
		                        value="" placeholder="Vehicle Number Plate" >
		                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
		                        <span class="toolTip"> Eg: OD 29 BA 9898 </span>
		                        <span class="errorMsg"></span>
		                    </div>
		                    <button type="submit" id="addVehicleButton">Add vehicle</button>
						</form>
					</div>
				</div>
			</div>
		</div>
		</div>
		</div>
			<cfinclude  template="includes/footer.cfm">
			</body>
