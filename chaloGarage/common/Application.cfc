<cfcomponent output="false">
	<cfset this.datasource = 'chaloGarage' />
	<cfset this.sessionManagement = true />
	<cfset this.sessionTimeout = createTimespan(0,1,0,0) />
	<cfset this.applicationTimeout = createtimespan(0,2,0,0) />

	<cffunction name="OnApplicationStart" returntype="boolean">
		<cfset application.authService = CreateObject("component","chaloGarage.common.components.authService")>
		<cfset application.updateProfile = CreateObject("component","chaloGarage.common.components.updateProfile")>
		<cfset application.dbQuery = CreateObject("component","chaloGarage.common.components.dbQuery")>
		<cfset application.vehicleManagement = CreateObject("component","chaloGarage.common.components.vehicleManagement")>
		<cfset application.bookServiceManagement = CreateObject("component","chaloGarage.common.components.bookServiceManagement")>

		<cfreturn true>
	</cffunction>

	<cffunction name="onRequestStart" returntype="boolean" >

		<cfargument name="targetPage" type="string" required="true" />
		<!---handle some special URL parameters--->
		<cfif isDefined('url.restartApp')>
			<cfset this.onApplicationStart() />
		</cfif>

		<cfif isDefined('url')>
			<cfset this.onApplicationStart() />
		</cfif>
		<!---Implement ressource Access control for the 'admin' folder--->
		<cfreturn true />
	</cffunction>

	<cffunction name="onMissingTemplate" returntype="boolean" >
		<cfargument name="targetPage" type="string" required=true/>
		<cftry>
			 <cflog type="error" text="Missing template: #arguments.targetPage#">
			<cfinclude  template="missingTemplate.cfm">
			<cfreturn true />
			<cfcatch>
				<cfreturn false />
			</cfcatch>
		</cftry>
	</cffunction>

</cfcomponent>