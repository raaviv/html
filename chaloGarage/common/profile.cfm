
<!---
Author: R sai Venkat
Date: 15/4/2020
Description: Profile page
--->
<cfif not isDefined("session.sLoginUserDetails")>
	<cflocation url = "index.cfm">
</cfif>
<cflog text = "#session.sLogInUserDetails['userID']#">
<cfset variables.user = application.dbQuery.getUserProfileDetailsFromDb(session.sLoginUserDetails['userID'])>
<cfset variables.userAddress = application.dbQuery.getUserAddressDetailsFromDb(session.sLoginUserDetails['userID'])>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/profileStyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script type = "text/javascript"
         src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>
      <script src="jquery/profileJquery.js"></script>
    </head>
    <body>
        <cfinclude  template="includes/header.cfm">
		<div class="updateProfileDiv">
			<div class="headingDiv">
				<h3 class="mainHeadingUpdate">Update Profile:</h3>
			</div>
		<div class="row">
			<div class="column">
			<cfoutput>
			<form name="updateForm" id="updateForm" action="profile.cfm" target="" method="POST">
					<p class="miniHeading"> Basic Details</p>
                    <div class="nameDiv" id="firstNameDiv">
                        <label class="sideHeadingUpdate">First Name<span class="asterik">*</span></label>
                        <span class="toolTip">1) should contain only alphabets, middle name can be included in first name.
                        <br>2) should not contain any special characters (ex:"-" , "  '  " , "." , ",")</span>
                        <input class="similarClassForValidation" id="firstName" type="text" name="firstName"
						value="#variables.user.firstName#" placeholder="First name"  >
                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
						<span class="errorMsg"></span>
                    </div>
                        <!--last name block-->
                    <div class="nameDiv" id="lastNameDiv">
                        <label class="sideHeadingUpdate">Last Name<span class="asterik">*</span></label>
                        <span class="toolTip">1) should contain only alphabets.<br>
                        2) should not contain any special characters (ex:"-" , "  '  " , "." , ",")</span>
                        <input class="similarClassForValidation" id="lastName" type="text" name="lastName"
						value="#variables.user.lastName#"
                        placeholder="Last name" >
                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
                        <span class="errorMsg"></span>
                    </div>
                    <!--Email block-->
                    <div class="nameDiv" id="emailDiv">
                        <label class="sideHeadingUpdate">E-mail<span class="asterik">*</span></label>
                        <input class="similarClassForValidation" id="email" type="text" name="email"
						value="#variables.user.email#"
                        placeholder="E-mail"  >
                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
                        <span class="toolTip">1) should contain value before and after '@' <br>
                        2) should contain value after '.' (Ex:abc123@xyz.com)</span>
                        <span class="errorMsg"></span>
                    </div>
					<!--AltEmail block-->
                    <div class="nameDiv" id="altEmailDiv">
                        <label class="sideHeadingUpdate">Alt E-mail</label>
                        <input class="similarClassForValidation" id="altEmail" type="text" name="altEmail"
						value="#variables.user.altEmail#"
                        placeholder="Alternate E-mail"  >
                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
                        <span class="toolTip">1) should contain value before and after '@' <br>
                        2) should contain value after '.' (Ex:abc123@xyz.com)</span>
                        <span class="errorMsg"></span>
                    </div>
                    <!--phone number block-->
                    <div class="nameDiv" id="phoneNumberDiv">
                        <label class="sideHeadingUpdate"> Phone Number<span class="asterik">*</span></label>
                        <input class="similarClassForValidation" id="phoneNumber" type="text" name="phoneNumber"
                        value="#variables.user.phoneNumber#" placeholder="Phone Number" >
                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
                        <span class="toolTip">should contain 10 digits and should not start with 0 or 1.</span>
                        <span class="errorMsg"></span>
                    </div>
					<!--Alt phone number block-->
                    <div class="nameDiv" id="altPhoneNumberDiv">
                        <label class="sideHeadingUpdate">Alt Phone Number</label>
                        <input class="similarClassForValidation" id="altPhoneNumber" type="text" name="altPhoneNumber"
                        value="#variables.user.altPhoneNumber#" placeholder="Alternate Phone Number" >
                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
                        <span class="toolTip">should contain 10 digits and should not start with 0 or 1.</span>
                        <span class="errorMsg"></span>
                    </div>
					<!--User name block -->
					<div class="nameDiv" id="userNameDiv">
                        <label class="sideHeadingUpdate">User Name<span class="asterik">*</span></label>
                        <span class="toolTip">user name is same as email entered for registering, except username contains the value before '@' in email </span>
                        <input class="similarClassForValidation" id="userName" type="text" name="userName"
						value="#variables.user.userName#"
                        placeholder="User name"  >
                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
                        <span class="errorMsg"></span>
                    </div>
                    <!--password block-->
                    <div class="nameDiv" id="passwordDiv">
                        <label class="sideHeadingUpdate">Password<span class="asterik">*</span></label>
                        <input class="similarClassForValidation" id="password" type="password" name="password"
                        value="#variables.user.password#" placeholder="Create Your Password" >
                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
                        <span class="toolTip">password length should be b/w 6-15 and should contain at
                        least one lowercase letter, one uppercase letter and one numeric digit.</span>
                        <span class="errorMsg"></span>
                    </div>
			</form>
			</cfoutput>
			</div>

			<div class="column">
			<cfoutput>
			<form name="updateForm" id="updateForm" action="profile.cfm" target="" method="POST">
                    <div class="primaryAddressDiv staticAddressDiv addressDiv">
						<p class="miniHeading"> Primary Address</p>
	                    <div class="nameDiv inputFieldDiv addressDiv1" id="addressDiv1">
	                        <label class="sideHeadingUpdate">Address Line 1<span class="asterik">*</span></label>
	                        <input class="similarClassForValidation" id="addressLine1" type="text" name="addressLine1"
							value="#variables.userAddress.addressLane1#" placeholder=" Address line 1"  >
	                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
	                        <span class="toolTip">Enter your address in form of characters(except : !@$%^*()=""etc )</span>
	                        <span class="errorMsg"></span>
	                    </div>
	                    <div class="nameDiv inputFieldDiv addressDiv2" id="addressDiv2">
	                        <label class="sideHeadingUpdate">Address Line 2</label>
	                        <input class="similarClassForValidation" id="addressLine2" type="text" name="addressLine2"
							value="#variables.userAddress.addressLane2#" placeholder=" Address line 2"  >
	                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
	                        <span class="toolTip">Enter your address in form of characters(except : !@$%^*()=""etc )</span>
	                        <span class="errorMsg"></span>
	                    </div>
	                    <div class="nameDiv inputFieldDiv countryDiv" id="countryDiv">
	                        <label class="sideHeadingUpdate">Country <span class="asterik">*</span></label>
	                        <select name="country" class="similarClassForValidation">
		                        <cfif variables.userAddress.country eq ''>
									<option value="" >Select Country</option>
								<cfelse>
									<option value="#variables.userAddress.country#" >#variables.userAddress.country#</option>
								</cfif>
                            </select>
	                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
	                        <span class="toolTip">Select your Country</span>
	                        <span class="errorMsg"></span>
	                    </div>
	                    <div class="nameDiv inputFieldDiv stateDiv" id="stateDiv">
	                        <label class="sideHeadingUpdate">State <span class="asterik">*</span></label>
	                        <select name"state" class="similarClassForValidation">
                               <cfif variables.userAddress.state eq ''>
									<option value="" >Select State</option>
								<cfelse>
									<option value="#variables.userAddress.state#" >#variables.userAddress.state#</option>
								</cfif>
                            </select>
	                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
	                        <span class="toolTip">Select your State</span>
	                        <span class="errorMsg"></span>
	                    </div>
	                    <div class="nameDiv inputFieldDiv cityDiv" id="cityDiv">
	                        <label class="sideHeadingUpdate">City <span class="asterik">*</span></label>
	                        <select name="city" class="similarClassForValidation">
                                <cfif variables.userAddress.city eq ''>
									<option value="" >Select City</option>
								<cfelse>
									<option value="#variables.userAddress.city#" >#variables.userAddress.city#</option>
								</cfif>
                            </select>
	                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
	                        <span class="toolTip">Select your City</span>
	                        <span class="errorMsg"></span>
	                    </div>
	                    <div class="nameDiv inputFieldDiv pinDiv" id="pinDiv">
	                        <label class="sideHeadingUpdate">Pin Code <span class="asterik">*</span></label>
	                        <input class="similarClassForValidation" id="pin" type="text" name="pin"
							value="#variables.userAddress.pinCode#" placeholder=" Pin Code">
	                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
	                        <span class="toolTip">Enter your Pin Code ( should be six numbers )</span>
	                        <span class="errorMsg"></span>
	                    </div>
                    </div>

					<div class="fakeAddressDiv addressDiv" id="fakeAddressDiv">
						<p class="miniHeading"> Alternate Address <span><button type="button" class="delAddressButton">x</button></span></p>
	                    <div class="nameDiv inputFieldDiv addressDiv1">
	                        <label class="sideHeadingUpdate">Address Line 1<span class="asterik">*</span></label>
	                        <input class="similarClassForValidation" id="addressLine1" type="text" name="addressLine1"
							value="#variables.userAddress.addressLane1[2]#" placeholder=" Address line 1"  >
	                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
	                        <span class="toolTip">Enter your address in form of characters(except : !@$%^*()=""etc )</span>
	                        <span class="errorMsg"></span>
	                    </div>
	                    <div class="nameDiv inputFieldDiv addressDiv2">
	                        <label class="sideHeadingUpdate">Address Line 2</label>
	                        <input class="similarClassForValidation" id="addressLine2" type="text" name="addressLine2"
							value="#variables.userAddress.addressLane2[2]#" placeholder=" Address line 2"  >
	                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
	                        <span class="toolTip">Enter your address in form of characters(except : !@$%^*()=""etc )</span>
	                        <span class="errorMsg"></span>
	                    </div>
	                    <div class="nameDiv inputFieldDiv countryDiv">
	                        <label class="sideHeadingUpdate">Country <span class="asterik">*</span></label>
	                        <select name="country" class="similarClassForValidation">
                                 <cfif variables.userAddress.country[2] eq ''>
									<option value="" >Select Country</option>
								<cfelse>
									<option value="#variables.userAddress.country[2]#" >#variables.userAddress.country[2]#</option>
								</cfif>
                            </select>
	                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
	                        <span class="toolTip">Select your Country</span>
	                        <span class="errorMsg"></span>
	                    </div>
	                    <div class="nameDiv inputFieldDiv stateDiv">
	                        <label class="sideHeadingUpdate">State <span class="asterik">*</span></label>
	                        <select name"state" class="similarClassForValidation">
                                <cfif variables.userAddress.state[2] eq ''>
									<option value="" >Select State</option>
								<cfelse>
									<option value="#variables.userAddress.state[2]#" >#variables.userAddress.state[2]#</option>
								</cfif>
                            </select>
	                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
	                        <span class="toolTip">Select your State</span>
	                        <span class="errorMsg"></span>
	                    </div>
	                    <div class="nameDiv inputFieldDiv cityDiv">
	                        <label class="sideHeadingUpdate">City <span class="asterik">*</span></label>
	                        <select name="city" class="similarClassForValidation">
                                <cfif variables.userAddress.city[2] eq ''>
									<option value="" >Select City</option>
								<cfelse>
									<option value="#variables.userAddress.city[2]#" >#variables.userAddress.city[2]#</option>
								</cfif>
                            </select>
	                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
	                        <span class="toolTip">Select your City</span>
	                        <span class="errorMsg"></span>
	                    </div>
	                    <div class="nameDiv inputFieldDiv pinDiv">
	                        <label class="sideHeadingUpdate">Pin Code <span class="asterik">*</span></label>
	                        <input class="similarClassForValidation" id="pin" type="text" name="pin"
							value="#variables.userAddress.pinCode[2]#" placeholder=" Pin Code"  >
	                        <i  class="fa fa-exclamation-triangle alertIcon"></i>
	                        <span class="toolTip">Enter your Pin Code ( should be six numbers )</span>
	                        <span class="errorMsg"></span>
	                    </div>
                    </div>
					<cfif variables.userAddress.recordCount EQ 2>
						<button type="button" class="addAddressButton">Show Alternate Address</button>
					<cfelse>
						<button type="button" class="addAddressButton">Add Alternate Address</button>
					</cfif>
			</form>
			</cfoutput>
			</div>
		</div>
		<button type="submit" id="submitProfileButton">Update Profile</button>
		</div>
		<div class="profileDiv">
			<div class="headingDiv">
				<h3 class="mainHeading">My Profile:</h3>
				<button type="button" class="updateProfileButton" id="updateProfileButton">Update My Profile</button>
			</div>
			<cfoutput>
			<div class="boxDiv">
			  	<div class="fieldDiv">
				  	<label class="sideHeading">Name:</label>
				  	<span class="sideHeadingValue">#variables.user.firstName# #variables.user.lastName#</span>
			  	</div>

			  	<div class="fieldDiv">
				  	<label class="sideHeading">User Name:</label>
				  	<span class="sideHeadingValue">#variables.user.userName#</span>
			  	</div>

			  	<div class="fieldDiv">
				  	<label class="sideHeading">Email:</label>
				  	<span class="sideHeadingValue">#variables.user.email#</span>
			  	</div>

			  	<cfif #variables.user.altEmail# NEQ "">
				  	<div class="fieldDiv">
					  	<label class="sideHeading">Alternate Email</label>
					  	<span class="sideHeadingValue">#variables.user.altEmail#</span>
				  	</div>
			  	</cfif>

			  	<div class="fieldDiv">
				  	<label class="sideHeading">Phone Number:</label>
				  	<span class="sideHeadingValue">#variables.user.phoneNumber#</span>
			  	</div>

			  	<cfif #variables.user.altPhoneNumber# NEQ "">
				  	<div class="fieldDiv">
					  	<label class="sideHeading">Alternate Phone Number</label>
					  	<span class="sideHeadingValue">#variables.user.altPhoneNumber#</span>
				  	</div>
			  	</cfif>

			  	<cfif variables.userAddress.recordCount NEQ 0>

			  	</cfif>
			</div>
		</div>

		</cfoutput>
		<cfinclude  template="includes/footer.cfm">
    </body>
</html>