<cfif isDefined("session.sLogInUserDetails") AND StructKeyExists(session.sLogInUserDetails,"userID")>
	<cflocation url = "index.cfm">
</cfif>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/registerLoginStyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <script type = "text/javascript"
         src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>
      <script src="jquery/registerLoginJquery.js"></script>
    </head>
    <body>
        <cfinclude  template="includes/header.cfm">
        <div id="boxdiv">
            <div id="header">Log In</div>
            <div id="formdiv">
                <!-- Login form -->
                <form name="loginform" action="index.cfm" target="" method="POST" >
                    <div class="namediv" id="usernamediv">
                        <label class="sideheading">User Name<span class="asterik">*</span></label>
                        <span class="tooltip">user name is same as email entered for registering, except username contains the value before '@' in email </span>
                        <input class="similarclassforvalidation" id="username" type="text" name="username" value=""
                        placeholder="User name"  >
                        <i  class="fa fa-exclamation-triangle alerticon"></i>
                        <span class="errormsg"></span>
                    </div>
                    <!--password block-->
                    <div class="namediv" id="passworddiv">
                        <label class="sideheading">Password<span class="asterik">*</span></label>
                        <input class="similarclassforvalidation" id="password" type="password" name="password"
                        value="" placeholder="Password" >
                        <i  class="fa fa-exclamation-triangle alerticon"></i>
                        <span class="tooltip">password length should be b/w 6-15 and should contain at
                        least one lowercase letter, one uppercase letter and one numeric digit.</span>
                        <span class="errormsg"></span>
                    </div>
                    <!--Submit button-->
                    <button type="submit" id="loginbutton">Login Now</button>
                    <div class="requireddetails">
                        <p>(<span class="asterik">*</span>) - Value is required.
                        Hover over <span class="asterik"> * </span>present fields to know details about them</p>
                        <p>Don't have an account? <a href="register.cfm">Sign Up</a></p>
                    </div>
                </form>
            </div>
        </div>
        <cfinclude  template="includes/footer.cfm">
    </body>
</html>