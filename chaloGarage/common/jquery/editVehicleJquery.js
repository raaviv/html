var previousVehicleNumber,vehicleID;
$(document).ready(function(){   
	$(document).on("click",".rectangle .updateVehicleButton ", function(){
		vehicleID = $(this).parents(".rectangle").find(".hidden .vehicleID").text();
		$(".displayContent").css("display","none");
		$(".updateVehicle").css("display","block");
		$.ajax({
			type:"POST",
		    url:"components/vehicleManagement.cfc",
		    data: {
		    	  "method":"getVehicleByID",
		          "vehicleID": vehicleID
		      	},
		    error: function(){
                  swal({
                      title: "Failed to Fetch Vehicle Details!!",
                      text: "Some error occured. Please try after sometime",
                      icon: "error",
                      button: "Ok",
                  });
              },
		    success: function(query) {
		    	var query = JSON.parse(query);
		    	previousVehicleNumber = query.DATA[0][4];
		    	$("#vehicleTypeDiv select").val(query.DATA[0][0]);
		    	$("#vehicleCompanyDiv select").html(
		    			'<option value="'+query.DATA[0][1]+'">'+query.DATA[0][1]+'</option>'
		    	)
		    	$("#vehicleModelDiv select").html(
		    			'<option value="'+query.DATA[0][2]+'">'+query.DATA[0][2]+'</option>'
		    	)
		    	$("input#vehicleDescription").val(query.DATA[0][3]);
		    	$("input#vehicleNumberPlate").val(query.DATA[0][4]);
		    }
		})
	})
    $("#updateVehicleButton").on("click",function(){
    	var arr= [validateVehicleNumberPlate(),validateVehicleDescription(),validateVehicleCompany(),validateVehicleModel(),
    		validateVehicleType()];
    	var clientValidationReg = true, serverValidationReg=false;
        for(var i=0; i<arr.length; i++)
        {
            
            if(arr[i]==false){
            	clientValidationReg = false;
            	break;
            }
        }
        if(clientValidationReg == true){
        	$.ajax({
    		    type:"POST",
    		    url:"components/vehicleManagement.cfc",
    		    data: {
    		    	  "method":"updateVehicleValidation",
    		    	  "vehicleCompany": $("#vehicleCompanyDiv select option:selected").val(),
    		    	  "vehicleModel": $("#vehicleModelDiv select option:selected").val(),
    		          "vehicleType": $("#vehicleTypeDiv select option:selected").val(),
    		          "vehicleDescription": $("#vehicleDescription").val(),
    		          "vehicleNumberPlate": $("#vehicleNumberPlate").val(),
    		          "previousVehicleNumberPlate": previousVehicleNumber,
    		          "vehicleID": vehicleID
    		      	},
    		    error: function(){
                      swal({
                          title: "Failed to Update!!",
                          text: "Some error occured. Please try after sometime",
                          icon: "error",
                          button: "Ok",
                      });
                  },
    		    success: function(errorMessages) {
    			    	if(errorMessages != "{}"){
    				    	var errorMessages = JSON.parse(errorMessages);
    				    	$("#vehicleCompanyDiv .errorMsg").html(errorMessages.VEHICLECOMPANY);
    				    	$("#vehicleModelDiv .errorMsg").html(errorMessages.VEHICLEMODEL);
    				    	$("#vehicleTypeDiv .errorMsg").html(errorMessages.VEHICLETYPE);
    				    	$("#vehicleDescriptionDiv .errorMsg").html(errorMessages.VEHICLEDESCRIPTION);
    				    	$("#vehicleNumberPlateDiv .errorMsg").html(errorMessages.VEHICLENUMBERPLATE);
    			    	}
    			    	else if(errorMessages == "{}"){
    			    		serverValidationReg = true;
    			    		window.location.href = 'https://assignmentpractice.com/chaloGarage/common/editVehicle.cfm';	 
    			    	}
    		    }
    		});
        }
        return (serverValidationReg && clientValidationReg);
    });  
    $("#vehicleTypeDiv select").on("blur",function(){
    	$.ajax({
    		type:"POST",
		    url:"components/vehicleManagement.cfc",
		    data: {
		    	  "method":"getVehicleCompany",
		    	  "vehicleType": $("#vehicleTypeDiv select option:selected").val()
		      },
		    error: function(){
                  swal({
                      title: "Vehicle Company Fetch Failed!!",
                      text: "Some error occured. Please try after sometime",
                      icon: "error",
                      button: "Ok",
                  });
              },
		    success: function(query) {
		    	var query = JSON.parse(query);
		    	$("#vehicleCompanyDiv select").html("<option value='' >--- Select --- </option>");
		    	for(i=0; i<query.DATA.length; i++){
		    		$("#vehicleCompanyDiv select").append("<option value='"+query.DATA[i]+"'>"+query.DATA[i]+"</option>");
		    	}
		    }
    	})
    })
    $("#vehicleCompanyDiv select").on("blur",function(){
    	$.ajax({
    		type:"POST",
		    url:"components/vehicleManagement.cfc",
		    data: {
		    	  "method":"getVehicleModel",
		    	  "vehicleCompany": $("#vehicleCompanyDiv select option:selected").val()
		      },
		    error: function(){
                  swal({
                      title: "Vehicle Model Fetch Failed!!",
                      text: "Some error occured. Please try after sometime",
                      icon: "error",
                      button: "Ok",
                  });
              },
		    success: function(query) {
		    	var query = JSON.parse(query);
		    	var temp, noSpaceVar;
		    	$("#vehicleModelDiv select").html("<option value='' >--- Select --- </option>");
		    	for(i=0; i<query.DATA.length; i++){
		    		$("#vehicleModelDiv select").append("<option value='"+query.DATA[i]+"'>"+query.DATA[i]+"</option>");
		    	}
		    }
    	})
    })
    $("#logout").on("click",function(){
    	$.ajax({
		    type:"POST",
		    url:"components/authService.cfc",
		    data: {
		    	  "method":"doLogout",
		      },
		    error: function(){
                  swal({
                      title: "Failed to Logout!!",
                      text: "Some error occured. Please try after sometime",
                      icon: "error",
                      button: "Ok",
                  });
              },
		    success: function() {
		    	window.location.href = 'index.cfm';
		    }
		});
    });  
    $('#vehicleNumberPlate').on("change keyup",validateVehicleNumberPlate); //event handling change and keyup
    $('#vehicleDescription').on("change keyup",validateVehicleDescription);
    $("#vehicleCompanyDiv select ").on("change keyup",validateVehicleCompany);
    $("#vehicleModelDiv select ").on("change keyup",validateVehicleModel);
    $("#vehicleTypeDiv select ").on("change keyup",validateVehicleType);
    $("#vehicleTypeDiv select ").on("change keyup",function(){
    	$("#vehicleCompanyDiv select").html("<option value='' >--- Select --- </option>");
    	$("#vehicleModelDiv select").html("<option value='' >--- Select --- </option>");
    });
    $("#vehicleCompanyDiv select ").on("change keyup",function(){
    	$("#vehicleModelDiv select").html("<option value='' >--- Select --- </option>");
    });
});
function repeatvalidate(temp1,temp2,temp3, msg, color, show,decide){
    temp1.css("visibility",show);
    temp2.css("border-color",color);
    temp3.fadeIn('fast', function(){temp3.html(msg);});
    return decide;
}
//validate function for fields
function validateexpression(regex,content,id, alertClass, errorClass,contentLength,requiredLength){
    var temp2= $("#"+id+" ."+ "similarClassForValidation"),temp1 =$("#"+id+" ."+ alertClass),temp3 =$("#"+id+" ."+ errorClass) ;
    if(contentLength>requiredLength){
        return repeatvalidate(temp1,temp2,temp3,"Input entered too long , limit is of "+requiredLength+ " characters" , "rgb(255,99,71)", "visible",false);
    }
    else if(content == "" || content == undefined){
        return repeatvalidate(temp1,temp2,temp3,"Value cannot be empty", "rgb(255,99,71)", "visible",false);
    }
    else if(content != "" && regex.test(content) == false){
        return repeatvalidate(temp1,temp2,temp3,"Entered value is invalid and is not in the required format", "rgb(255,99,71)", "visible",false);
    }
    else if(content != "" && regex.test(content) == true){
        return repeatvalidate(temp1,temp2,temp3,"", "rgb(204,204,204)", "hidden",true);
    } 
}
function validatealtexpression(regex,content,id, alertClass, errorClass,contentLength,requiredLength){
    var temp2= $("#"+id+" ."+ "similarClassForValidation"),temp1 =$("#"+id+" ."+ alertClass),temp3 =$("#"+id+" ."+ errorClass) ;
    if(contentLength>requiredLength){
        return repeatvalidate(temp1,temp2,temp3,"Input entered too long , limit is of "+requiredLength+ " characters" , "rgb(255,99,71)", "visible",false);
    }
    else if(content == undefined){
        return repeatvalidate(temp1,temp2,temp3,"Value cannot be empty", "rgb(255,99,71)", "visible",false);
    }
    else if(content != "" && regex.test(content) == false){
        return repeatvalidate(temp1,temp2,temp3,"Entered value is invalid and is not in the required format", "rgb(255,99,71)", "visible",false);
    }
    else if(content == "" || regex.test(content) == true){
        return repeatvalidate(temp1,temp2,temp3,"", "rgb(204,204,204)", "hidden",true);
    } 
}
function validateVehicleNumberPlate(){ //validate function for first name
    var VehicleNumberPlateRegex = /^[A-Z]{2}[ -][0-9]{1,2}(?: [A-Z])?(?: [A-Z]*)? [0-9]{4}$/;
    var content = $("#vehicleNumberPlate").val().trim();
    return validateexpression(VehicleNumberPlateRegex, content,"vehicleNumberPlateDiv","alertIcon","errorMsg",content.length,30);
}
function validateVehicleDescription(){ //validate function for last name
    var vehicleDescriptionRegex = /^[a-zA-Z0-9\s,'/.-]*$/;
    var content = $("#vehicleDescription").val().trim();
    return validatealtexpression(vehicleDescriptionRegex, content,"vehicleDescriptionDiv","alertIcon","errorMsg",content.length,100);
}
function validateVehicleCompany(){
	var vehicleCompanyRegex = /^[a-z|A-Z]+([" "]?[a-z|A-Z])*$/;
	var content = $("#vehicleCompanyDiv select option:selected").val();
    return validateexpression(vehicleCompanyRegex, content,"vehicleCompanyDiv","alertIcon","errorMsg",content.length,30);
}
function validateVehicleModel(){
	var vehicleModelRegex = /^[a-zA-Z0-9\s,'/.-]*$/;
	var content = $("#vehicleModelDiv select option:selected").val();
    return validateexpression(vehicleModelRegex, content,"vehicleModelDiv","alertIcon","errorMsg",content.length,30);
}
function validateVehicleType(){
	var vehicleTypeRegex = /^[a-z|A-Z]+$/;
	var content = $("#vehicleTypeDiv select option:selected").val();
    return validateexpression(vehicleTypeRegex, content,"vehicleTypeDiv","alertIcon","errorMsg",content.length,25);
}
