var serverSide = false, addressArr=[];
$(document).ready(function(){
	$("#submitProfileButton").on("click", function(){
		var hasAlternateAddress = false;
		if(addressArr.length >= 1){
			hasAlternateAddress = true;
		}
		var updateProfileArr= [validatefirstname(),validatelastname(),validateemail(),
			validatealtemail(),validatephonenumber(),validatealtphonenumber(),
            validateusername(),validatepassword(),dynamicValidate(),validatecountry(), validatestate(),validatecity(), 
            validateaddress(),validatepincode()];
		var clientValidationUpdateProfile = true, serverValidationUpdateProfile = false;
        for(var i=0; i<updateProfileArr.length; i++)
        {
            
            if(updateProfileArr[i]==false){
            	clientValidationUpdateProfile = false;
            	break;
            }
        }
        if(clientValidationUpdateProfile == true){
        	$.ajax({
    		    type:"POST",
    		    url:"components/updateProfile.cfc",
    		    data: {
    		    	  "method":"validateUpdateProfileForm",
    		          "firstName": $("#firstName").val(),
    		          "lastName": $("#lastName").val(),
    		          "email": $("#email").val(),
    		          "altEmail": $("#altEmail").val(),
    		          "phoneNumber": $("#phoneNumber").val(),
    		          "altPhoneNumber": $("#altPhoneNumber").val(),
    		          "userName": $("#userName").val(),
    		          "password": $("#password").val(),
    		          "primaryAddressLine1":$("#addressDiv1 input").val(),
    		          "primaryAddressLine2":$("#addressDiv2 input").val(),
    		          "primaryCountry":$("#countryDiv select option:selected").val(),
    		          "primaryState":$("#stateDiv select option:selected").val(),
    		          "primaryCity":$("#cityDiv select option:selected").val(),
    		          "primaryPincode":$("#pinDiv input").val(),
    		          "hasAlternateAddress": hasAlternateAddress,
    		          "alternateAddressLine1": hasAlternateAddress == true ? $("."+addressArr[0]).find('.addressDiv1 input').val().trim() : '',
    		          "alternateAddressLine2": hasAlternateAddress == true ? $("."+addressArr[0]).find('.addressDiv2 input').val().trim() : '',
    		          "alternateCountry": hasAlternateAddress == true ? $("."+addressArr[0]).find('.countryDiv select option:selected').val() : '',
    		          "alternateState": hasAlternateAddress == true ? $("."+addressArr[0]).find('.stateDiv select option:selected').val() : '',
    		          "alternateCity": hasAlternateAddress == true ? $("."+addressArr[0]).find('.cityDiv select option:selected').val() : '',
    		          "alternatePincode": hasAlternateAddress == true ? $("."+addressArr[0]).find('.pinDiv input').val().trim() : ' '     						  
    		      	},
    		    error: function(jqXHR, status, err ){
    		    	swal("Sorry!", "Please try again", "warning");
		     	},
    		    success: function(errorMessages) {
    			    	if(errorMessages != "{}"){
    				    	var errorMessages = JSON.parse(errorMessages);
    				    	$("#firstNameDiv .errorMsg").html(errorMessages.FIRSTNAME);
    				    	$("#lastNameDiv .errorMsg").html(errorMessages.LASTNAME);
    				    	$("#emailDiv .errorMsg").html(errorMessages.EMAIL);
    				    	$("#altEmailDiv .errorMsg").html(errorMessages.ALTEMAIL);
    				    	$("#passwordDiv .errorMsg").html(errorMessages.PASSWORD);
    				    	$("#phoneNumberDiv .errorMsg").html(errorMessages.PHONENUMBER);
    				    	$("#altPhoneNumberDiv .errorMsg").html(errorMessages.ALTPHONENUMBER);
    				    	$("#userNameDiv .errorMsg").html(errorMessages.USERNAME);
    				    	$("#addressDiv1 .errorMsg").html(errorMessages.PRIMARYADDRESSLINE1);
    				    	$("#addressDiv2 .errorMsg").html(errorMessages.PRIMARYADDRESSLINE2);
    				    	$("#countryDiv .errorMsg").html(errorMessages.PRIMARYCOUNTRY);
    				    	$("#stateDiv .errorMsg").html(errorMessages.PRIMARYSTATE);
    				    	$("#cityDiv .errorMsg").html(errorMessages.PRIMARYCITY);
    				    	$("#pinDiv .errorMsg").html(errorMessages.PRIMARYPINCODE);
    				    	$(".fakeAddressDiv .addressDiv1 .errorMsg").html(errorMessages.ALTERNATEADDRESSLINE1);
    				    	$(".fakeAddressDiv .addressDiv2 .errorMsg").html(errorMessages.ALTERNATEADDRESSLINE2);
    				    	$(".fakeAddressDiv .countryDiv .errorMsg").html(errorMessages.ALTERNATECOUNTRY);
    				    	$(".fakeAddressDiv .stateDiv .errorMsg").html(errorMessages.ALTERNATESTATE);
    				    	$(".fakeAddressDiv .cityDiv .errorMsg").html(errorMessages.ALTERNATECITY);
    				    	$(".fakeAddressDiv .pinDiv .errorMsg").html(errorMessages.ALTERNATEPINCODE);
    			    	}
    			    	else if(errorMessages == "{}"){
    			    		serverValidationUpdateProfile = true;
    			    		window.location.href = 'profile.cfm';
    			    	}
    		    }
    		});
        }
        return (serverValidationUpdateProfile && clientValidationUpdateProfile);
	})
	$(".addAddressButton").on("click",addressAdd);
	$("body").on("click",".delAddressButton",function(){deleteAddress(this);})
	$(document).on("focus", ".addressDiv .countryDiv", function(){
        if($(this).find('select').val()!=undefined){
        var tempid = $(this).parents(".addressDiv").attr("class").split(" ");
        loadJsonData('country',this);
        $("."+tempid[1]).find(".stateDiv select").html('<option value="" >Select State</option>'); 
        $("."+tempid[1]).find(".cityDiv select").html('<option value="" >Select City</option>'); 
        }
    })
	$(document).on('change',".countryDiv select", function(){dynamicState(this);})
    $(document).on('change', ".stateDiv select", function(){dynamicCity(this);})
	$("#updateProfileButton").on("click", function(){
		$(".profileDiv").css("display","none");
		$(".updateProfileDiv").css("display","block");
	})
    $("#logout").on("click",function(){
    	$.ajax({
		    type:"POST",
		    url:"components/authService.cfc",
		    data: {
		    	  "method":"doLogout",
		      },
		    error: function(){
                  swal({
                      title: "Failed to Logout!!",
                      text: "Some error occured. Please try after sometime",
                      icon: "error",
                      button: "Ok",
                  });
              },
		    success: function() {
		    	window.location.href = 'index.cfm';
		    }
		});
    });
	$('#firstName').on("change keyup",validatefirstname); //event handling change and keyup
    $('#lastName').on("change keyup",validatelastname);
    $("#email").on("change keyup",validateemail);
    $("#altEmail").on("change keyup",validatealtemail);
    $("#password").on("change keyup",validatepassword);
    $("#phoneNumber").on("change keyup",validatephonenumber);
    $("#altPhoneNumber").on("change keyup",validatealtphonenumber);
    $("#userName").on("change keyup",validateusername);
    
});
function repeatvalidate(temp1,temp2,temp3, msg, color, show,decide){
    temp1.css("visibility",show);
    temp2.css("border-color",color);
    temp3.fadeIn('fast', function(){temp3.html(msg);});
    return decide;
}
function validateexpression(regex,content,id, alertclass, errorclass,contentlength,requiredlength){
    var temp2= $("#"+id+" ."+ "similarClassForValidation"),temp1 =$("#"+id+" ."+ alertclass),temp3 =$("#"+id+" ."+ errorclass) ;
    if(contentlength>requiredlength){
        return repeatvalidate(temp1,temp2,temp3,"Input entered too long , limit is of "+requiredlength+ " characters" , "rgb(255,99,71)", "visible",false);
    }
    else if(content == "" || content == undefined){
        return repeatvalidate(temp1,temp2,temp3,"Value cannot be empty", "rgb(255,99,71)", "visible",false);
    }
    else if(content != "" && regex.test(content) == false){
        return repeatvalidate(temp1,temp2,temp3,"Entered value is invalid and is not in the required format", "rgb(255,99,71)", "visible",false);
    }
    else if(content != "" && regex.test(content) == true){
        return repeatvalidate(temp1,temp2,temp3,"", "rgb(204,204,204)", "hidden",true);
    } 
}
function validatedynamicexpression(regex, content, id, classname, alertclass, errorclass,contentlength,requiredlength){
	var temp2= $("."+id+" ."+classname+" ."+ "similarClassForValidation");
	var temp1 =$("."+id+" ."+classname+" ."+ alertclass),temp3 =$("."+id+" ."+classname+" ."+ errorclass) ;
	if(contentlength>requiredlength){
        return repeatvalidate(temp1,temp2,temp3,"Input entered too long , limit is of "+requiredlength+ " characters" , "rgb(255,99,71)", "visible",false);
    }
    else if(content == "" || content == undefined){
        return repeatvalidate(temp1,temp2,temp3,"Value cannot be empty", "rgb(255,99,71)", "visible",false);
    }
    else if(content != "" && regex.test(content) == false){
        return repeatvalidate(temp1,temp2,temp3,"Entered value is invalid and is not in the required format", "rgb(255,99,71)", "visible",false);
    }
    else if(content != "" && regex.test(content) == true){
        return repeatvalidate(temp1,temp2,temp3,"", "rgb(204,204,204)", "hidden",true);
    } 	
}
function validatedynamicaltexpression(regex, content, id, classname, alertclass, errorclass,contentlength,requiredlength){
	var temp2= $("."+id+" ."+classname+" ."+ "similarClassForValidation");
	var temp1 =$("."+id+" ."+classname+" ."+ alertclass),temp3 =$("."+id+" ."+classname+" ."+ errorclass) ;
	 if(contentlength>requiredlength){
	        return repeatvalidate(temp1,temp2,temp3,"Input entered too long , limit is of "+requiredlength+ " characters" , "rgb(255,99,71)", "visible",false);
	    }
	    else if(content != "" && regex.test(content) == false){
	        return repeatvalidate(temp1,temp2,temp3,"Entered value is invalid and is not in the required format", "rgb(255,99,71)", "visible",false);
	    }
	    else if(content != "" && regex.test(content) == true || content == ""){
	        return repeatvalidate(temp1,temp2,temp3,"", "rgb(204,204,204)", "hidden",true);
	    } 
}
function validatealtexpression(regex,content,id, alertclass, errorclass,contentlength,requiredlength){
    var temp2= $("#"+id+" ."+ "similarClassForValidation"),temp1 =$("#"+id+" ."+ alertclass),temp3 =$("#"+id+" ."+ errorclass) ;
    if(contentlength>requiredlength){
        return repeatvalidate(temp1,temp2,temp3,"Input entered too long , limit is of "+requiredlength+ " characters" , "rgb(255,99,71)", "visible",false);
    }
    else if(content != "" && regex.test(content) == false){
        return repeatvalidate(temp1,temp2,temp3,"Entered value is invalid and is not in the required format", "rgb(255,99,71)", "visible",false);
    }
    else if(content != "" && regex.test(content) == true || content == ""){
        return repeatvalidate(temp1,temp2,temp3,"", "rgb(204,204,204)", "hidden",true);
    } 
}
function validatefirstname(){ //validate function for first name
    var firstNameRegex = /^[a-z|A-Z]+[" "]?[a-z|A-Z]*$/;
    var content = $("#firstName").val().trim();
    return validateexpression(firstNameRegex, content,"firstNameDiv","alertIcon","errorMsg",content.length,50);
}
function validatelastname(){ //validate function for last name
    var lastNameRegex = /^[a-z|A-Z]+$/;
    var content = $("#lastName").val().trim();
    return validateexpression(lastNameRegex, content,"lastNameDiv","alertIcon","errorMsg",content.length,25);
}
function validateemail(){   //validate function for email
    var emailRegex = /^\w+([\.+-]?\w+)*@(\w)+([\.-]?\w+)*(\.\w{2,3})+$/;
    var content = $("#email").val().trim();
    return validateexpression(emailRegex,content,"emailDiv","alertIcon","errorMsg",content.length,30);
}
function validatepassword(){   //validate function for password
    var passwordRegex =/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,15}$/;
    var content = $("#password").val().trim();
    return validateexpression(passwordRegex,content,"passwordDiv","alertIcon","errorMsg",content.length,15);
}
function validatephonenumber(){     //validate function for phone number
    var phoneRegex = /^[2-9][0-9]{9}$/;
    content = $("#phoneNumber").val().trim();
    return validateexpression(phoneRegex, content, "phoneNumberDiv","alertIcon","errorMsg",content.length,10);
}
function validateusername(){
    var userNameRegex = /^\w+([\.+-]?\w+)*$/;
    var content = $("#userName").val().trim();
    return validateexpression(userNameRegex,content,"userNameDiv","alertIcon","errorMsg",content.length,25);
}
function validatealtemail(){
	var altEmailRegex = /^\w+([\.+-]?\w+)*@(\w)+([\.-]?\w+)*(\.\w{2,3})+$/;
    var content = $("#altEmail").val().trim();
    return validatealtexpression(altEmailRegex,content,"altEmailDiv","alertIcon","errorMsg",content.length,30);
}
function validatealtphonenumber(){
	var altPhoneRegex = /^[2-9][0-9]{9}$/;
    content = $("#altPhoneNumber").val().trim();
    return validatealtexpression(altPhoneRegex, content, "altPhoneNumberDiv","alertIcon","errorMsg",content.length,10);
}
function ajaxError(){
	swal({
        title: "Sorry!",
        text: "Something went wrong please try again",
        icon: "warning"
		})
}
function addressAdd(){
    var addressFieldCode = $('#fakeAddressDiv').html();
    var newID = generateID();
    var codeString = "<div class='fakeAddressDiv"+" "+ newID +" "+"addressDiv'>" + addressFieldCode +"</div>";
    $(".addAddressButton").before(codeString);
    addressContentChange(newID);
    addressArr.push(newID);
    $(".addAddressButton").css("display", "none");
}
function generateID() {
    var result ='id';
    var d = new Date();
    var milsec = d.getMilliseconds().toString();
    var day = d.getDate().toString(), month = (d.getMonth()+1).toString(), year = d.getFullYear().toString(),
        hours = d.getHours().toString(), minutes= d.getMinutes().toString(), seconds = d.getSeconds().toString();
    var time =  hours + minutes + seconds + milsec + day + month + year;
    return result += time;
 }
function addressContentChange(newID){
    $("."+ newID).find('.addressDiv1 input').attr('name','addressLine1'+ newID);
    $("."+ newID).find('.addressDiv2 input').attr('name','addressLine2'+ newID);
    $("."+ newID).find('.countryDiv select').attr('name','country'+ newID);
    $("."+ newID).find('.stateDiv select').attr('name','state'+ newID);
    $("."+ newID).find('.cityDiv select').attr('name','city'+ newID);
    $("."+ newID).find('.pinDiv input').attr('name','pin'+ newID);
}
function deleteAddress(caller){
    var temp=$(caller).parents(".fakeAddressDiv").attr('class').split(" ");
    for(var i =0;i< addressArr.length;i++)
    {
        if(temp[1] == addressArr[i])
        {
            addressArr.splice(i,1);
        }
    }
    $(caller).parents(".fakeAddressDiv").remove();
    $(".addAddressButton").css("display", "block");
}
function loadJsonData(id,caller, parent_name)
{
    var htmlCode = '';
    if(caller == undefined){var tempID = null; }
    else{var tempID = $(caller).parents(".addressDiv").attr("class").split(" ");}
    $.getJSON('json/countryStateCity.json', function(data){
    htmlCode += '<option value="" >Select '+id+'</option>';
    $.each(data, function(key, value){
        if(id == 'country')
        {
            if(value.parent_name == 'null')
            {
                htmlCode += '<option value="'+value.name+'">'+value.name+'</option>';
                if(tempID == null){$("."+id+"Div select").html(htmlCode);}
                else{$("."+tempID[1]).find("."+id+"Div select").html(htmlCode);}
            }
        }
        else 
        {
            if(value.parent_name == parent_name)
            {
                htmlCode += '<option value="'+value.name+'">'+value.name+'</option>';
                $("."+tempID[1]).find("."+id+"Div select").html(htmlCode);
            }
        }
    });
    });
 }
function dynamicCity(caller){
    var tempID = $(caller).parents(".addressDiv").attr("class").split(" ");
    var parent_name = $(caller).val();
    if(parent_name != ""){loadJsonData('city',caller, parent_name);}
    else if(parent_name == ""){$("."+tempID[1]).find(".cityDiv select").html('<option value="" >Select City</option>');}
}
function dynamicState(caller){
    var tempID = $(caller).parents(".addressDiv").attr("class").split(" ");
    var parent_name = $(caller).val();
    if(parent_name != ""){
        loadJsonData('state',caller, parent_name);
        $("."+tempID[1]).find(".cityDiv select").html('<option value="" >Select City</option>');   
    }
    else if(parent_name == ""){
        $("."+tempID[1]).find(".stateDiv select").html('<option value="" >Select State</option>');
        $("."+tempID[1]).find(".cityDiv select").html('<option value="" >Select City</option>');
    } 
}
function dynamicValidate(){
    var temp1=[], temp2=[],temp3=[];
    var dynamicAddressReturn, dynamicPincodeReturn , dynamicCountryStateCityReturn  ;
   for( var j=0; j < addressArr.length; j++)
   {
       temp1[j]=dynamicAddress(addressArr[j]);
       temp2[j]=dynamicPincode(addressArr[j]);
       temp3[j]=dynamicCountryStateCity(addressArr[j]);
       if(temp1[j]==false){dynamicAddressReturn = false;}
       if(temp2[j]==false){dynamicPincodeReturn = false;}
       if(temp3[j]==false){dynamicCountryStateCityReturn = false;}
   }
   if(dynamicAddressReturn == false || dynamicPincodeReturn == false || dynamicCountryStateCityReturn == false){
       return false;
   }
   else{
       return true;
   }
}
function dynamicAddress(id){
    var dynamicAddressRegex = /^[a-zA-Z0-9\s,'/.-]*$/;
    var contentLine1 = $("."+id).find('.addressDiv1 input').val().trim();
    var contentLine2 = $("."+id).find('.addressDiv2 input').val().trim();
    var temp1 = validatedynamicexpression(dynamicAddressRegex,contentLine1,id,"addressDiv1","alertIcon","errorMsg",contentLine1.length,100);
    var temp2 = validatedynamicaltexpression(dynamicAddressRegex,contentLine2,id, "addressDiv2","alertIcon","errorMsg",contentLine2.length,100);
    if(temp1 == false || temp2 == false){
    	return false;
    }
    else{
    	return true;
    }
 }
 function dynamicCountryStateCity(id,caller){
    var dynamicRegex = /^[a-z|A-Z]+([" "]?[a-z|A-Z]+)*$/;
    if($(caller).attr("class")=="inputFieldDiv countryDiv" || caller == undefined){
    var content1 = $("."+id).find('.countryDiv select').val().trim();
    var temp1 = validatedynamicexpression(dynamicRegex,content1, id, "countryDiv","alertIcon","errorMsg",content1.length,40);
    }
    if($(caller).attr("class")=="inputFieldDiv stateDiv"  || caller == undefined){
    var content2 = $("."+id).find('.stateDiv select').val().trim();
    var temp2 = validatedynamicexpression(dynamicRegex,content2, id, "stateDiv","alertIcon","errorMsg",content2.length,40);
    }
    if($(caller).attr("class")=="inputFieldDiv cityDiv" || caller == undefined){
    var content3 = $("."+id).find('.cityDiv select').val().trim();
    var temp3 = validatedynamicexpression(dynamicRegex,content3, id, "cityDiv", "alertIcon","errorMsg",content3.length,40);
    }
    if(temp1 == false || temp2 == false || temp3 == false){return false;}
    else{return true;}
 }
 function dynamicPincode(id){
    var dynamicPincodeRegex = /^[0-9]{6}$/;
    var content = $("."+id).find('.pinDiv input').val().trim();
    return validatedynamicexpression(dynamicPincodeRegex,content, id, "pinDiv","alertIcon","errorMsg",content.length,6);
 }
 function validatecountry(){
	    var content = $(".primaryAddressDiv .countryDiv select option:selected").val();
	    var countryRegex = /^[a-z|A-Z]+([" "]?[a-z|A-Z]+)*$/
	    return validateexpression(countryRegex, content, "countryDiv", "alertIcon","errorMsg",content.length,40);
	}
	function validatestate(){
	    var content = $(".primaryAddressDiv .stateDiv select option:selected").val();
	    var stateRegex = /^[a-z|A-Z]+([" "]?[a-z|A-Z]+)*$/
	    return validateexpression(stateRegex, content, "stateDiv", "alertIcon","errorMsg",content.length,40);
	}
	function validatecity(){
	    var content = $(".primaryAddressDiv .cityDiv select option:selected").val();
	    var cityRegex = /^[a-z|A-Z]+([" "]?[a-z|A-Z]+)*$/
	    return validateexpression(cityRegex, content, "cityDiv", "alertIcon","errorMsg",content.length,40);
	}
	function validateaddress(){
	    var addressRegex =/^[a-zA-Z0-9\s,'/.-]*$/;
	    var content1 = $(".primaryAddressDiv .addressDiv1 input").val().trim();
	    var content2 = $(".primaryAddressDiv .addressDiv2 input").val().trim();
	    var temp1 = validateexpression(addressRegex, content1, "addressDiv1", "alertIcon","errorMsg",content1.length,100);
	    var temp2 = validatealtexpression(addressRegex, content2, "addressDiv2", "alertIcon","errorMsg",content2.length,100);
	    if(temp1 == false || temp2 == false){
	    	return false;
	    }
	    else{
	    	return true;
	    }
	}
	function validatepincode(){
	    var pincodeRegex =/^[0-9]{6}$/;
	    var content = $(".primaryAddressDiv .pinDiv input").val().trim();
	    return validateexpression(pincodeRegex, content, "pinDiv", "alertIcon", "errorMsg",content.length,6);
	}