var res=0;
$(document).ready(function(){   //validate every field on click 
    $("#registerbutton").on("click",function(){
    	var registerarr= [validatefirstname(),validatelastname(),validateemail(),validatephonenumber(),
            validateusername(),validatepassword(),validatereenterpassword()];
    	var clientValidationReg = true, serverValidationReg=false;
        for(var i=0; i<registerarr.length; i++)
        {
            
            if(registerarr[i]==false){
            	clientValidationReg = false;
            	break;
            }
        }
        if(clientValidationReg == true){
        	$.ajax({
    		    type:"POST",
    		    url:"components/authService.cfc",
    		    data: {
    		    	  "method":"validateRegisterForm",
    		          "firstName": $("#firstname").val(),
    		          "lastName": $("#lastname").val(),
    		          "email": $("#email").val(),
    		          "phoneNumber": $("#phonenumber").val(),
    		          "userName": $("#username").val(),
    		          "password": $("#password").val(),
    		          "reenterpassword": $("#reenterpassword").val()
    		      	},
    		    error: function(){
                      swal({
                          title: "Failed to Register!!",
                          text: "Some error occured. Please try after sometime",
                          icon: "error",
                          button: "Ok",
                      });
                  },
    		    success: function(errorMessages) {
    			    	if(errorMessages != "{}"){
    				    	var errorMessages = JSON.parse(errorMessages);
    				    	$("#firstnamediv .errormsg").html(errorMessages.FIRSTNAME);
    				    	$("#lastnamediv .errormsg").html(errorMessages.LASTNAME);
    				    	$("#emaildiv .errormsg").html(errorMessages.EMAIL);
    				    	$("#passworddiv .errormsg").html(errorMessages.PASSWORD);
    				    	$("#reenterpassworddiv .errormsg").html(errorMessages.REENTERPASSWORD);
    				    	$("#phonenumberdiv .errormsg").html(errorMessages.PHONENUMBER);
    				    	$("#usernamediv .errormsg").html(errorMessages.USERNAME);
    			    	}
    			    	else if(errorMessages == "{}"){
    			    		serverValidationReg = true;
    			    		window.location.href = 'login.cfm';	 
    			    	}
    		    }
    		});
        }
        return (serverValidationReg && clientValidationReg);
    });  
    $("#loginbutton").on("click",function(){
    	var clientValidationLogin = true, serverValidationLogin = false;
    	var loginarr= [validateusername(),validatepassword()];
        for(var i=0; i<loginarr.length; i++)
        {
            if(loginarr[i]==false){
            	clientValidationLogin = false;
            	break;
            }
        } 
        if(clientValidationLogin == true){
        	$.ajax({
    		    type:"POST",
    		    url:"components/authService.cfc",
    		    data: {
    		    	  "method":"validateLoginForm",
    		          "userName": $("#username").val(),
    		          "userPassword": $("#password").val()
    		      },
    		    error: function(){
                      swal({
                          title: "Failed to loggedIn!!",
                          text: "Some error occured. Please try after sometime",
                          icon: "error",
                          button: "Ok",
                      });
                  },
    		    success: function(errorMessages) {
    			    	if(errorMessages != "{}"){
    				    	var errorMessages = JSON.parse(errorMessages);
    				    	$("#usernamediv .errormsg").html(errorMessages.USERNAME);
    				    	$("#passworddiv .errormsg").html(errorMessages.USERPASSWORD);
    			    	}
    			    	else if(errorMessages == "{}"){
    			    		serverValidationLogin = true;
    			    		window.location.href = 'index.cfm';
    			    	}
    		    }
    		});
        }
    	return (serverValidationLogin && clientValidationLogin);
    });  
    $("#logout").on("click",function(){
    	$.ajax({
		    type:"POST",
		    url:"components/authService.cfc",
		    data: {
		    	  "method":"doLogout",
		      },
		    error: function(){
                  swal({
                      title: "Failed to Logout!!",
                      text: "Some error occured. Please try after sometime",
                      icon: "error",
                      button: "Ok",
                  });
              },
		    success: function() {
		    	window.location.href = 'index.cfm';
		    }
		});
    }); 
    $('#firstname').on("change keyup",validatefirstname); //event handling change and keyup
    $('#lastname').on("change keyup",validatelastname);
    $("#email").on("change keyup",validateemail);
    $("#password").on("change keyup",validatepassword);
    $("#phonenumber").on("change keyup",validatephonenumber);
    $("#reenterpassword").on("change keyup",validatereenterpassword);
    $("#username").on("change keyup",validateusername);
});
function repeatvalidate(temp1,temp2,temp3, msg, color, show,decide){
    temp1.css("visibility",show);
    temp2.css("border-color",color);
    temp3.fadeIn('fast', function(){temp3.html(msg);});
    return decide;
}
//validate function for fields
function validateexpression(regex,content,id, alertclass, errorclass,contentlength,requiredlength){
    var temp2= $("#"+id+" ."+ "similarclassforvalidation"),temp1 =$("#"+id+" ."+ alertclass),temp3 =$("#"+id+" ."+ errorclass) ;
    if(contentlength>requiredlength){
        return repeatvalidate(temp1,temp2,temp3,"Input entered too long , limit is of "+requiredlength+ " characters" , "rgb(255,99,71)", "visible",false);
    }
    else if(content == "" || content == undefined){
        return repeatvalidate(temp1,temp2,temp3,"Value cannot be empty", "rgb(255,99,71)", "visible",false);
    }
    else if(content != "" && regex.test(content) == false){
        return repeatvalidate(temp1,temp2,temp3,"Entered value is invalid and is not in the required format", "rgb(255,99,71)", "visible",false);
    }
    else if(content != "" && regex.test(content) == true){
        return repeatvalidate(temp1,temp2,temp3,"", "rgb(204,204,204)", "hidden",true);
    } 
}
function validatefirstname(){ //validate function for first name
    var firstnameregex = /^[a-z|A-Z]+[" "]?[a-z|A-Z]*$/;
    var content = $("#firstname").val().trim();
    return validateexpression(firstnameregex, content,"firstnamediv","alerticon","errormsg",content.length,50);
}
function validatelastname(){ //validate function for last name
    var lastnameregex = /^[a-z|A-Z]+$/;
    var content = $("#lastname").val().trim();
    return validateexpression(lastnameregex, content,"lastnamediv","alerticon","errormsg",content.length,25);
}
function validateemail(){   //validate function for email
    var emailregex = /^\w+([\.+-]?\w+)*@(\w)+([\.-]?\w+)*(\.\w{2,3})+$/;
    var content = $("#email").val().trim();
    return validateexpression(emailregex,content,"emaildiv","alerticon","errormsg",content.length,30);
}
function validatepassword(){   //validate function for password
    var passwordregex =/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,15}$/;
    var content = $("#password").val().trim();
    return validateexpression(passwordregex,content,"passworddiv","alerticon","errormsg",content.length,15);
}
function validatereenterpassword(){    //validate function for re-enter password
    var content = $("#password").val().trim();
    var content1 = $("#reenterpassword").val().trim();
    var temp1= $("#reenterpassworddiv .alerticon"), temp2 = $("#reenterpassworddiv .similarclassforvalidation"), temp3 = $("#reenterpassworddiv .errormsg");
    if(content1 == "")
    {
        return repeatvalidate(temp1,temp2,temp3,"Value cannot be empty", "rgb(255,99,71)", "visible",false);
    }
    else if(content != content1)
    {
        return repeatvalidate(temp1,temp2,temp3,"Re-entered-password does not match with the entered password", "rgb(255,99,71)", "visible",false);
    }
    else if(content == content1)
    {
        return repeatvalidate(temp1,temp2,temp3,"", "rgb(204,204,204)", "hidden",true);
    }
}
function validatephonenumber(){     //validate function for phone number
    var phoneregex = /^[2-9][0-9]{9}$/;
    content = $("#phonenumber").val().trim();
    return validateexpression(phoneregex, content, "phonenumberdiv","alerticon","errormsg",content.length,10);
}
function validateusername(){
    var usernameregex = /^\w+([\.+-]?\w+)*$/;
    var content = $("#username").val().trim();
    return validateexpression(usernameregex,content,"usernamediv","alerticon","errormsg",content.length,25);
}
