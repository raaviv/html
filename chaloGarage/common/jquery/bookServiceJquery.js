var productsArr=[],pricesArr=[],productIDArr=[];
var vehicleName, vehicleNumberPlate, totalPrice, vehicleCompanyName, vehicleModelName;
$(document).ready(function(){
	if($("label input[type='radio']:checked").val() == 'bike'){
		$("#bikeLabel").find(".icon img").css("border-color","rgba(255,133,0,0.85)");
		$("#carLabel").find(".icon img").css("border-color","rgba(0,0,0,0.15)");
		$("#vehicle .row span.errorMsg").html("");
	}
	else if($("label input[type='radio']:checked").val() == 'car'){
		$("#carLabel").find(".icon img").css("border-color","rgba(255,133,0,0.85)");
		$("#bikeLabel").find(".icon img").css("border-color","rgba(0,0,0,0.15)");
		$("#vehicle .row span.errorMsg").html("");
	}
	$("#vehicleCompany").on("change keyup",function(){
		validateVehicleCompany($(".vehicleCompanyDiv select option:selected").val());
	})
	$("#vehicleModel").on("change keyup",function(){
		validateVehicleModel($(".vehicleModelDiv select option:selected").val());
	})
	$("#productType").on("change keyup",function(){
		validateProductType($(".productTypeDiv select option:selected").val());
	})
	$(".buyButton").on("click", function(){
		$.ajax({
			type:"POST",
			    url:"components/bookServiceManagement.cfc",
			    
			    data: {
			    	  "method":"createWorkOrder",
			    	  "vehicleNumber": $(".vehicleNumberPlate select option:selected").val(),
			    	  "addressType": $(".addressDiv select option:selected").val(),
			    	  "vehicleCompany": $(".vehicleCompanyDiv select option:selected").val(),
			    	  "vehicleModel" : $(".vehicleModelDiv select option:selected").val(),
			    	  "productIDArr": productIDArr.join()
			      },
			    error: function(){
		              swal({
		                  title: "Unable to Place order!!",
		                  text: "Please try after some time",
		                  icon: "error",
		                  button: "Ok",
		              });
		          },
			    success: function(errorMessages) {
			    	console.log(errorMessages);
			    	if(errorMessages != "{}"){
				    	var errorMessages = JSON.parse(errorMessages);
				    	$(".vehicleName span.errorMsg").html(errorMessages.VEHICLECOMPANY);
				    	$(".vehicleName span.errorMsg").append(errorMessages.VEHICLEMODEL);
				    	$(".vehicleNumberPlate span.errorMsg").html(errorMessages.VEHICLENUMBER);
				    	$(".totalCost span.errorMsg").html(errorMessages.TOTALCOST);
				    	$(".addressDiv span.errorMsg").html(errorMessages.ADDRESSTYPE);
			    	}
			    	else if(errorMessages == "{}"){
			    		swal({
			    			  title: "Work Order Placed Successfully!",
			    			  text: "Visit frequently to check your status!",
			    			  icon: "success",
			    			  button: "Ok",
			    			});
			    	}
			    	}
		})
		return false;
	})
	$("#vehicle .row .next").on("click",function(){
		var vehicleType = $("label input[type='radio']:checked").val();
		console.log(vehicleType);
		if(validateVehicleType(vehicleType)){
			$(".nav li.vehicleTab").removeClass("active");
			$(".tabContent #vehicle").removeClass("active");
			$(".nav li.modelTab").addClass("active");
			$("#model").addClass("active");
			$.ajax({
	    		type:"POST",
			    url:"components/vehicleManagement.cfc",
			    data: {
			    	  "method":"getVehicleCompany",
			    	  "vehicleType": $("label input[type='radio']:checked").val()
			      },
			    error: function(){
	                  swal({
	                      title: "Vehicle Company Fetch Failed!!",
	                      text: "Select type of Vehicle to get company details. If not, Please try after sometime",
	                      icon: "error",
	                      button: "Ok",
	                  });
	              },
			    success: function(query) {
			    	var query = JSON.parse(query);
			    	$(".vehicleCompanyDiv select").html("<option value='' >--- Select --- </option>");
			    	for(i=0; i<query.DATA.length; i++){
			    		$(".vehicleCompanyDiv select").append("<option value='"+query.DATA[i]+"'>"+query.DATA[i]+"</option>");
			    	}
			    }
	    	})
		}
	})
	$(".vehicleCompanyDiv select").on("change keyup",function(){
    	$.ajax({
    		type:"POST",
		    url:"components/vehicleManagement.cfc",
		    data: {
		    	  "method":"getVehicleModel",
		    	  "vehicleCompany": $(".vehicleCompanyDiv select option:selected").val()
		      },
		    error: function(){
                  swal({
                      title: "Vehicle Model Fetch Failed!!",
                      text: "Some error occured. Please try after sometime",
                      icon: "error",
                      button: "Ok",
                  });
              },
		    success: function(query) {
		    	var query = JSON.parse(query);
		    	var temp, noSpaceVar;
		    	$(".vehicleModelDiv select").html("<option value='' >--- Select --- </option>");
		    	for(i=0; i<query.DATA.length; i++){
		    		$(".vehicleModelDiv select").append("<option value='"+query.DATA[i]+"'>"+query.DATA[i]+"</option>");
		    	}
		    }
    	})
    })
	$("#model .row .nameDiv .next").on("click", function(){
		var vehicleCompany = $(".vehicleCompanyDiv select option:selected").val();
		var vehicleModel = $(".vehicleModelDiv select option:selected").val();
		var productType = $(".productTypeDiv select option:selected").val();
		var validateVehicleCompanyResult = validateVehicleCompany(vehicleCompany);
		var validateVehicleModelResult = validateVehicleModel(vehicleModel);
		var validateProductTypeResult = validateProductType(productType);
		if(validateVehicleCompanyResult && validateVehicleModelResult && validateProductTypeResult)
		{
				$(".nav li.servicesTab").addClass("active");
				$("#service").addClass("active");
				$(".nav li.modelTab").removeClass("active");
				$("#model").removeClass("active");
				$.ajax({
		    		type:"POST",
				    url:"components/bookServiceManagement.cfc",
				    data: {
				    	  "method":"getProductDetailsForDisplay",
				    	  "vehicleType": $("label input[type='radio']:checked").val(),
				    	  "productType": productType
				      },
				    error: function(){
		                  swal({
		                      title: "Product Details Fetch Failed!!",
		                      text: "Some error occured. Please try after sometime",
		                      icon: "error",
		                      button: "Ok",
		                  });
		              },
				    success: function(query) {
				    	var query = JSON.parse(query);
				    	$("#servicesContainer").html("");
				    	console.log(query);
				    	for(i=0; i<query.DATA.length; i++){
				    		$("#servicesContainer").append('<div class="checkbox">'+
									'<label>'+
		                  			'<input type="checkbox" name="productCheckbox[]" value="'+query.DATA[i][0]+'">'+ query.DATA[i][0] +
									'</label>'+
									'<span class="productCost">'+query.DATA[i][1]+'</span>'+
									'<span class="productID notVisible">'+query.DATA[i][2]+'</span>'+
		              			'</div>');
				    	}
				    }
		    	})
			}
		
	})
	$("#model .row .nameDiv .previous").on("click", function(){
		$(".nav li.modelTab").removeClass("active");
		$("#model").removeClass("active");
		$(".nav li.vehicleTab").addClass("active");
		$(".tabContent #vehicle").addClass("active");
	})
	$("#checkOutButton").on("click submit", function(){
		$.each($(".checkbox label input[type='checkbox']:checked"), function(){
			productsArr.push($(this).val());
			pricesArr.push($(this).parents(".checkbox").find("span.productCost").text());
			productIDArr.push(parseInt($(this).parents(".checkbox").find("span.productID").text()));
        });
		var totalCost = parseInt(0);
		for(var i=0; i<pricesArr.length;i++){
			totalCost += parseInt(pricesArr[i]);
		}
		if(!checkLogin()){
			return false;
		}
		if(!checkVehicle()){
			return false;
		}
		if(!checkAddress()){
			return false;
		}
		var arr=[validateVehicleType($("label input[type='radio']:checked").val()),
				validateVehicleCompany($(".vehicleCompanyDiv select option:selected").val()), 
				validateVehicleModel($(".vehicleModelDiv select option:selected").val()), 
				validateProductType($(".productTypeDiv select option:selected").val()),
				validateCheckBox()];
		var clientValidationReg = true
        for(var i=0; i<arr.length; i++)
        {
            if(arr[i]==false){
            	clientValidationReg = false;
            	return false;
            }
        }
		if(clientValidationReg == true){
			$("#bookServicesForm").css("display","none");
			$("#checkoutForm").css("display","block");
			$.ajax({
				type:"POST",
			    url:"components/bookServiceManagement.cfc",
			    
			    data: {
			    	  "method":"getVehicleNumber",
			    	  "vehicleCompany" : $(".vehicleCompanyDiv select option:selected").val(),
			    	  "vehicleModel" : $(".vehicleModelDiv select option:selected").val()
			      },
			    error: function(){
		              swal({
		                  title: "Unable to retrieve vehicle Data!!",
		                  text: "Please try after some time.",
		                  icon: "error",
		                  button: "Ok",
		              });
		          },
			    success: function(query) {
			    	query = JSON.parse(query);
			    	$(".vehicleNumberPlate select").html("");
			    	for(i=0; i<query.DATA.length; i++){
			    		$(".vehicleNumberPlate select").append("<option value='"+query.DATA[i][0]+"'>"+query.DATA[i][0]+"</option>");
			    	}
			    }
			})
			$(".vehicleName span.labelValue").html($(".vehicleCompanyDiv select option:selected").val() + " " +
										$(".vehicleModelDiv select option:selected").val());
			$(".totalCost span.labelValue").html(totalCost);
			$("div.productCostDetails").html("");
			for(var i = 0 ; i < productsArr.length ; i++){
				console.log(productsArr[i]);
				$(".displayContent").append('<div class="nameDiv leftMargin productCostDetails">'+
							'<label>'+ productsArr[i]+'</label>'+
							'<span class="labelValue">'+ pricesArr[i]+'</span>'+
						'</div>');
			}
		}
		return false;
	})
	$("#service .previous").on("click", function(){
		$(".nav li.modelTab").addClass("active");
		$("#model").addClass("active");
		$(".nav li.servicesTab").removeClass("active");
		$("#service").removeClass("active");
	})
	$("#bikeLabel").on("click",function(){
		$("#bikeLabel").find(".icon img").css("border-color","rgba(255,133,0,0.85)");
		$("#carLabel").find(".icon img").css("border-color","rgba(0,0,0,0.15)");
		$("#vehicle .row span.errorMsg").html("");
	})
	$("#carLabel").on("click",function(){
		$("#carLabel").find(".icon img").css("border-color","rgba(255,133,0,0.85)");
		$("#bikeLabel").find(".icon img").css("border-color","rgba(0,0,0,0.15)");
		$("#vehicle .row span.errorMsg").html("");
	})
	$(".nav li.modelTab").on("click", function(){
		$(".nav li.modelTab").addClass("active");
		$("#model").addClass("active");
		if($(".nav li.vehicleTab").hasClass("active"))
		{
			$(".nav li.vehicleTab").removeClass("active");
			$(".tabContent #vehicle").removeClass("active");
		}
		else{
			$(".nav li.servicesTab").removeClass("active");
			$("#service").removeClass("active");
		}
	});
	$(".nav li.vehicleTab").on("click", function(){
		$(".nav li.vehicleTab").addClass("active");
		$(".tabContent #vehicle").addClass("active");
		if($(".nav li.modelTab").hasClass("active"))
		{
			$(".nav li.modelTab").removeClass("active");
			$("#model").removeClass("active");
		}
		else{
			$(".nav li.servicesTab").removeClass("active");
			$("#service").removeClass("active");
		}
	});
	$(".nav li.servicesTab").on("click", function(){
		$(".nav li.servicesTab").addClass("active");
		$("#service").addClass("active");
		if($(".nav li.modelTab").hasClass("active"))
		{
			$(".nav li.modelTab").removeClass("active");
			$("#model").removeClass("active");
		}
		else{
			$(".nav li.vehicleTab").removeClass("active");
			$(".tabContent #vehicle").removeClass("active");
		}
	});
    $("#logout").on("click",function(){
    	$.ajax({
		    type:"POST",
		    url:"components/authService.cfc",
		    data: {
		    	  "method":"doLogout",
		      },
		    error: function(){
                  swal({
                      title: "Failed to Logout!!",
                      text: "Some error occured. Please try after sometime",
                      icon: "error",
                      button: "Ok",
                  });
              },
		    success: function() {
		    	window.location.href = 'index.cfm';
		    }
		});
    });
});
function checkLogin(){
	var retValue = true;
	$.ajax({
		type:"POST",
		async: false,
	    url:"components/authService.cfc",
	    data: {
	    	  "method":"checkLogin"
	      },
	    error: function(){
              swal({
                  title: "Unable to verify User!!",
                  text: "Please try after some time.",
                  icon: "error",
                  button: "Ok",
              });
          },
	    success: function(logIn) {
	    	logIn = JSON.parse(logIn);
	    	if(logIn == false)
	    		{
	    		swal({
	                  title: "Not Logged In!!",
	                  text: "Please Login or Sign Up for purchasing our services.",
	                  icon: "error",
	                  button: "Ok",
	              });
	    		retValue = false;
	    		}
	    }
	})
	return retValue;
}
function checkAddress(){
	var retValue = true;
	$.ajax({
		type:"POST",
		async: false,
	    url:"components/bookServiceManagement.cfc",
	    data: {
	    	  "method":"checkAddress",
	      },
	    error: function(){
              swal({
                  title: "Unable to retrieve Address Data!!",
                  text: "Please try after some time.",
                  icon: "error",
                  button: "Ok",
              });
          },
	    success: function(torf) {
	    	torf = JSON.parse(torf);
	    	if(torf == false)
	    		{
	    		swal({
	                  title: "Your address details are not registered!!",
	                  text: "Please add address details for purchasing our services.",
	                  icon: "error",
	                  button: "Ok",
	              });
	    		retValue = false;
	    		}
	    }
	})
	return retValue;
}
function checkVehicle(){
	var retValue = true;
	$.ajax({
		type:"POST",
	    url:"components/bookServiceManagement.cfc",
	    async: false,
	    data: {
	    	  "method":"checkVehicle",
	    	  "vehicleCompany" : $(".vehicleCompanyDiv select option:selected").val(),
	    	  "vehicleModel" : $(".vehicleModelDiv select option:selected").val()
	      },
	    error: function(){
              swal({
                  title: "Unable to retrieve vehicle Data!!",
                  text: "Please try after some time.",
                  icon: "error",
                  button: "Ok",
              });
          },
	    success: function(torf) {
	    	torf = JSON.parse(torf);
	    	if(torf == false)
	    		{
	    		swal({
	                  title: "Entered Vehicle details do not match with your vehicle details!!",
	                  text: "Please Edit Vehicle Details for purchasing our services.",
	                  icon: "error",
	                  button: "Ok",
	              });
	    		  retValue = false;
	    }
	    }
	})
	return retValue;
}
function validateVehicleType(vehicleType){
	var torf;
	if(vehicleType == '' || vehicleType == undefined){
		$("#vehicle .row span.errorMsg").html("please select your vehicle type");
		torf = false;
	}
	else if(vehicleType != ''){
		torf = true;
	}
	return torf;
}
function validateVehicleCompany(vehicleCompany){
	var torf;

	if(vehicleCompany == ''|| vehicleCompany == undefined){
		$("#vehicleCompany").css("border-color","rgb(255,99,71");
		torf = false;
	}
	else if(vehicleCompany != '' ){
		$("#vehicleCompany").css("border-color","rgb(204,204,204");
		torf = true;
	}
	return torf;
}
function validateVehicleModel(vehicleModel){
	var torf;
	if(vehicleModel == ''|| vehicleModel == undefined){
		$("#vehicleModel").css("border-color","rgb(255,99,71");
		torf= false;
	}
	else if(vehicleModel != '' ){
		$("#vehicleModel").css("border-color","rgb(204,204,204");
		torf = true;
	}
	return torf;
}
function validateProductType(productType){
	var torf;
	if(productType == ''||  productType == undefined){
		$("#productType").css("border-color","rgb(255,99,71");
		torf= false;
	}
	else if(productType != ''){
		$("#productType").css("border-color","rgb(204,204,204");
		torf = true;
	}
	return torf;
}
function validateCheckBox(){
	var torf;
	if(productsArr.length>0){
		torf = true;
		$("#service").find(".errorMsg").html("");
	}
	else if(productsArr.length == 0){
		torf = false;
		$("#service").find(".errorMsg").html("Please Select atleast one check box to checkout");
	}
	return torf;
}