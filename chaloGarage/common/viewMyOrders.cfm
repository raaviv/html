<cfif NOT isDefined("session.sLoginUserDetails")>
	<cflocation url = "https://assignmentpractice.com/chaloGarage/common/index.cfm">
</cfif>
<cfset qWorkOrderDetailsByUserIDFromDb = application.dbQuery.getWorkOrderDetailsByUserIDFromDb(session.sLoginUserDetails['userID'])>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="css/viewMyWorkOrdersStyle.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
        <script type = "text/javascript"
         src = "https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js">
      </script>
      <script src="jquery/viewMyWorkOrderJquery.js"></script>
    </head>
    <body>
		<cfinclude  template="includes/header.cfm">
		<div class="imageContainer" style="background-image: url(../common/images/bike_cars.jpg); height: auto;">
		<div class="container">
		<div class="row">
            <div class="column">
                <div class="containerBox">
                    <div class="card cardShadow" id="wizard">
						<div class="displayWorkOrders">
						<div class="headingDiv">
							<h3 class="mainHeading"><span><img class="icon" src="../common/images/icons/viewIcon.png"></span>My Orders</span>
							</h3>
						</div>
						<div class="miniHeading"><h3>Recent Work Order details</h3></div>
						<div class="displayContent">
							<cfoutput query="qWorkOrderDetailsByUserIDFromDb" group = "workOrderID">
							<div class="rectangle">
								<h4 class = "workOrderID">Work Order ID: #qWorkOrderDetailsByUserIDFromDb.workOrderID#</h4>
								<h4 class = "vehicle"> vehicle : #qWorkOrderDetailsByUserIDFromDb.vehicleCompany# #qWorkOrderDetailsByUserIDFromDb.vehicleModel#</h4>
								<h4 class = "product">Products :<cfoutput>#qWorkOrderDetailsByUserIDFromDb.productName#,</cfoutput>.</h4>
								<button type="button" class="viewButton"> View details</button>
							</div>
							</cfoutput>
					    </div>
					</div>
					<div class="viewWorkOrderDetail">
						<div class="headingDiv">
							<h3 class="mainHeading"><span><img class="icon" src="../common/images/icons/editIcon.png"></span>Work Order Details</span>
							</h3>
						</div>
		                    <div class="nameDiv" id="workOrderIDDiv">

		                        <label class="sideHeading">Work Order ID:</label>
								<label class="headingValue"> </label>
		                    </div>


		                    <div class="nameDiv" id="vehicleDiv">
		                        <label class="sideHeading">Vehicle:</label>
								<label class="headingValue"> </label>
		                    </div>

		                    <div class="nameDiv" id="productDiv">
		                        <label class="sideHeading">Products Brought:</label>
								<label class="headingValue"> </label>
		                    </div>

		                    <div class="nameDiv" id="employeeDiv">
		                        <label class="sideHeading">Employee Assigned: </label>
								<label class="headingValue"> </label>
		                    </div>

							<div class="nameDiv" id="workOrderStatusDiv">
		                        <label class="sideHeading">Work Order Status </label>
		                        <label class="headingValue"> </label>
		                    </div>


		                    <div class="nameDiv" id="addressDiv">
		                        <label class="sideHeading"> Customer Address:</label>
								<label class="headingValue"> </label>
		                    </div>
		                    <button type="submit" id="backButton">Back</button>
					</div>
					</div>
				</div>
			</div>
		</div>
		</div>
		</div>
			<cfinclude  template="includes/footer.cfm">
			</body>
