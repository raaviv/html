<!---
  --- updateProfile
  --- -------------
  ---
  --- this component contains methods for validating the updated data and inserting the updated data into db
  ---
  --- author: mindfire
  --- date:   4/16/20
  --->
<cfcomponent hint="this component contains methods for validating the updated data and inserting the updated data into db" accessors="true" output="false" persistent="false">

<cffunction name = "validateUpdateProfileForm" description = "validate Update profile form" access = "remote"
				returnformat = "json" output = "false" returntype = "Struct">
		<cfargument name = "firstName" type = "string" required = "true" />
		<cfargument name = "lastName" type = "string" required = "true" />
		<cfargument name = "email" type = "string" required = "true" />
		<cfargument name = "altEmail" type = "string" required = "true" />
		<cfargument name = "phoneNumber" type = "string" required = "true" />
		<cfargument name = "altPhoneNumber" type = "string" required = "true" />
		<cfargument name = "userName" type = "string" required = "true" />
		<cfargument name = "password" type = "string" required = "true" />
		<cfargument name = "primaryAddressLine1" type="string" required = "true"/>
		<cfargument name = "primaryAddressLine2" type="string" required = "true"/>
		<cfargument name = "primaryCountry" type="string" required = "true"/>
		<cfargument name = "primaryState" type="string" required = "true"/>
		<cfargument name = "primaryCity" type="string" required = "true"/>
		<cfargument name = "primaryPincode" type="string" required = "true"/>
		<cfargument name = "hasAlternateAddress" type="boolean" required = "true"/>
		<cfargument name = "alternateAddressLine1" type="string" required="false"/>
		<cfargument name = "alternateAddressLine2" type="string" required="false"/>
        <cfargument name = "alternateCountry" type="string" required="false"/>
        <cfargument name = "alternateState" type="string" required="false"/>
        <cfargument name = "alternateCity" type="string" required="false"/>
        <cfargument name = "alternatePincode" type="string" required="false"/>

		<cfset var errorMessages = {} />
		<cfset var userDetails = application.dbQuery.getUserProfileDetailsFromDb(session.sLogInUserDetails.userID)>
		<!---Validate firstName--->
		<cfif len(arguments.firstName) gt 50>
			<cfset errorMessages.firstName = "Value entered is too long , limit is of 50 characters " />
		<cfelseif arguments.firstName EQ ''OR NOT isValid("regex", arguments.firstName, "[a-z|A-Z]+[ ]?[a-z|A-Z]*")>
			<cfset errorMessages.firstName = "Please provide a valid First Name" />
		</cfif>

		<!---Validate lastName--->
		<cfif len(arguments.lastName) gt 25>
			<cfset errorMessages.lastName = "Value entered is too long , limit is of 25 characters " />
		<cfelseif arguments.lastName EQ '' OR NOT isValid("regex", arguments.lastName, "[a-z|A-Z]+")>
			<cfset errorMessages.lastName = "Please provide a valid Last Name" />
		</cfif>


		<!---Validate Email--->
		<cfif len(arguments.email) gt 30>
			<cfset errorMessages.email = "Value entered is too long , limit is of 30 characters " />
		<cfelseif arguments.email EQ '' OR NOT isValid('regex',arguments.email,'\w+([\.+-]?\w+)*@(\w)+([\.-]?\w+)*(\.\w{2,3})+') >
			<cfset errorMessages.email = 'Please provide a valid email'/>
		<cfelse>
			<cftry>
				<cfset var qEmailDetails = application.dbQuery.getEmailFromDb(arguments.email)>
	            <cfcatch type="database">
		            <cflog  text="error in email : #cfcatch.detail#">
            		<cfset errorMessages.email="Some Error occurred while checking the email. Please, try after sometime!!"/>
	            </cfcatch>
			</cftry>
            <cfif qEmailDetails.recordCount EQ 1 AND arguments.email NEQ userDetails.email>
				<cfset errorMessages.email = "This Email Address has already been used!!">
            </cfif>
		</cfif>

		<!---Validate Alt Email--->
		<cfif len(arguments.altEmail) gt 30>
			<cfset errorMessages.altEmail = "Value entered is too long , limit is of 30 characters " />
		<cfelseif arguments.altEmail NEQ '' AND NOT isValid('regex',arguments.altEmail,'\w+([\.+-]?\w+)*@(\w)+([\.-]?\w+)*(\.\w{2,3})+') >
			<cfset errorMessages.altEmail = 'Please provide a valid Alternate email'/>
		<cfelseif arguments.altEmail EQ arguments.email>
			<cfset errorMessages.altEmail = 'Please do not repeat email!!'/>
		</cfif>

		<!---validate Alt phone number--->
		<cfif len(arguments.altPhoneNumber) gt 10>
			<cfset errorMessages.altPhoneNumber = "Value entered is too long , limit is of 15 characters " />
		<cfelseif arguments.altPhoneNumber NEQ '' AND NOT isValid('regex',arguments.altPhoneNumber,'[2-9][0-9]{9}') >
			<cfset errorMessages.altPhoneNumber = 'Please provide a valid Alternate Phone number'/>
		<cfelseif arguments.altPhoneNumber EQ arguments.phoneNumber>
			<cfset errorMessages.altPhoneNumber = 'Please do not repeat phone Number!!'/>
		</cfif>

		<!---Validate Password--->
		<cfif len(arguments.password) gt 15>
			<cfset errorMessages.password = "Value entered is too long , limit is of 15 characters " />
		<cfelseif arguments.password EQ '' OR NOT isValid('regex',arguments.password,'(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,15}') >
			<cfset errorMessages.password = 'Please provide a valid password'/>
		</cfif>


		<!---validate phone number--->
		<cfif len(arguments.phoneNumber) gt 10>
			<cfset errorMessages.phoneNumber = "Value entered is too long , limit is of 15 characters " />
		<cfelseif arguments.phoneNumber EQ '' OR NOT isValid('regex',arguments.phoneNumber,'[2-9][0-9]{9}') >
			<cfset errorMessages.phoneNumber = 'Please provide a valid phone number'/>
		<cfelse>
			<cftry>
				<cfset var qPhoneNumberDetails = application.dbQuery.getPhoneNumberFromDb(arguments.phoneNumber)>
	            <cfcatch type="database">
		            <cflog  text="error in phone number : #cfcatch.detail#">
            		<cfset errorMessages.phoneNumber="Some Error occurred while checking the email. Please, try after sometime!!"/>
	            </cfcatch>
			</cftry>
            <cfif qPhoneNumberDetails.recordCount EQ 1 AND arguments.phoneNumber NEQ userDetails.phoneNumber>
				<cfset errorMessages.phoneNumber = "This Phone Number has already been used!!">
            </cfif>
		</cfif>

		<!--- Validate userName--->
		<cfif len(arguments.userName) gt 25>
			<cfset errorMessages.userName = "Value entered is too long , limit is of 25 characters " />
		<cfelseif arguments.userName EQ '' OR NOT isValid('regex',arguments.userName,'\w+([\.+-]?\w+)*') >
			<cfset errorMessages.userName = 'Please provide a valid user Name'/>
		<cfelse>
			<cftry>
				<cfset var qUserNameDetails = application.dbQuery.getUserNameFromDb(arguments.userName)>
	            <cfcatch type="database">
		            <cflog  text="error in user name : #cfcatch.detail#">
            		<cfset errorMessages.userName="Some Error occurred while checking the User Name. Please, try after sometime!!"/>
	            </cfcatch>
			</cftry>
            <cfif qUserNameDetails.recordCount EQ 1 AND arguments.userName NEQ userDetails.userName>
                <cfset errorMessages.userName = "This user name has already been used!!">
            </cfif>
		</cfif>

		<!--- checking the primary address --->
		<cfif len(arguments.primaryAddressLine1) gt 100>
			<cfset errorMessages.primaryAddressLine1 = "Value entered is too long , limit is of 100 characters " />
		<cfelseif arguments.primaryAddressLine1 EQ ''OR NOT isValid("regex", arguments.primaryAddressLine1, "[a-zA-Z0-9\s,'/.-]*")>
			<cfset errorMessages.primaryAddressLine1 = "Entered value is invalid and is not in the required format." />
		</cfif>

		<cfif len(arguments.primaryAddressLine2) gt 100>
			<cfset errorMessages.primaryAddressLine2 = "Value entered is too long , limit is of 100 characters " />
		<cfelseif arguments.primaryAddressLine2 NEQ '' AND NOT isValid("regex", arguments.primaryAddressLine2, "[a-zA-Z0-9\s,'/.-]*")>
			<cfset errorMessages.primaryAddressLine2 = "Entered value is invalid and is not in the required format." />
		</cfif>

		<cfif len(arguments.primaryCountry) gt 40>
			<cfset errorMessages.primaryCountry = "Value entered is too long , limit is of 40 characters " />
		<cfelseif arguments.primaryCountry EQ '' OR NOT isValid("regex", arguments.primaryCountry, "[a-zA-Z]+([' ']?[a-zA-Z]+)*")>
			<cfset errorMessages.primaryCountry = "Entered value is invalid and is not in the required format." />
		</cfif>

		<cfif len(arguments.primaryState) gt 40>
			<cfset errorMessages.primaryState = "Value entered is too long , limit is of 40 characters " />
		<cfelseif arguments.primaryState EQ ''OR NOT isValid("regex", arguments.primaryState, "[a-z|A-Z]+([' ']?[a-z|A-Z]+)*")>
			<cfset errorMessages.primaryState = "Entered value is invalid and is not in the required format." />
		</cfif>

		<cfif len(arguments.primaryCity) gt 40>
			<cfset errorMessages.primaryCity = "Value entered is too long , limit is of 40 characters " />
		<cfelseif arguments.primaryCity EQ ''OR NOT isValid("regex", arguments.primaryCity, "[a-z|A-Z]+([' ']?[a-z|A-Z]+)*")>
			<cfset errorMessages.primaryCity = "Entered value is invalid and is not in the required format." />
		</cfif>

		<cfif len(arguments.primaryPincode) gt 6>
			<cfset errorMessages.primaryPincode = "Value entered is too long , limit is of 6 characters " />
		<cfelseif arguments.primaryPincode EQ ''OR NOT isValid("regex", arguments.primaryPincode, "[0-9]{6}")>
			<cfset errorMessages.primaryPincode = "Entered value is invalid and is not in the required format." />
		</cfif>

		<!---checking the alternate address --->
		 <cfif arguments.hasAlternateAddress EQ true>
			<cfif len(arguments.alternateAddressLine1) gt 100>
				<cfset errorMessages.alternateAddressLine1 = "Value entered is too long , limit is of 100 characters " />
			<cfelseif arguments.alternateAddressLine1 EQ ''OR NOT isValid("regex", arguments.alternateAddressLine1, "[a-zA-Z0-9\s,'/.-]*")>
				<cfset errorMessages.alternateAddressLine1 = "Entered value is invalid and is not in the required format." />
			<cfelseif arguments.alternateAddressLine1 EQ arguments.primaryAddressLine1>
				<cfset errorMessages.alternateAddressLine1 = " Please provide a different address!!">
			</cfif>

			<cfif len(arguments.alternateAddressLine2) gt 100>
				<cfset errorMessages.alternateAddressLine2 = "Value entered is too long , limit is of 100 characters " />
			<cfelseif arguments.alternateAddressLine2 NEQ '' AND NOT isValid("regex", arguments.alternateAddressLine2, "[a-zA-Z0-9\s,'/.-]*")>
				<cfset errorMessages.alternateAddressLine2 = "Entered value is invalid and is not in the required format." />
			<cfelseif arguments.alternateAddressLine2 NEQ '' AND arguments.alternateAddressLine2 EQ arguments.primaryAddressLine2>
				<cfset errorMessages.alternateAddressLine2 = " Please provide a different address!!">
			</cfif>

			<cfif len(arguments.alternateCountry) gt 40>
				<cfset errorMessages.alternateCountry = "Value entered is too long , limit is of 40 characters " />
			<cfelseif arguments.alternateCountry EQ ''OR NOT isValid("regex", arguments.alternateCountry, "[a-z|A-Z]+([' ']?[a-z|A-Z]+)*")>
				<cfset errorMessages.alternateCountry = "Entered value is invalid and is not in the required format." />
			</cfif>

			<cfif len(arguments.alternateState) gt 40>
				<cfset errorMessages.alternateState = "Value entered is too long , limit is of 40 characters " />
			<cfelseif arguments.alternateState EQ ''OR NOT isValid("regex", arguments.alternateState, "[a-z|A-Z]+([' ']?[a-z|A-Z]+)*")>
				<cfset errorMessages.alternateState = "Entered value is invalid and is not in the required format." />
			</cfif>

			<cfif len(arguments.alternateCity) gt 40>
				<cfset errorMessages.alternateCity = "Value entered is too long , limit is of 40 characters " />
			<cfelseif arguments.alternateCity EQ ''OR NOT isValid("regex", arguments.alternateCity, "[a-z|A-Z]+([' ']?[a-z|A-Z]+)*")>
				<cfset errorMessages.alternateCity = "Entered value is invalid and is not in the required format." />
			</cfif>

			<cfif len(arguments.alternatePincode) gt 6>
				<cfset errorMessages.alternatePincode = "Value entered is too long , limit is of 6 characters " />
			<cfelseif arguments.alternatePincode EQ ''OR NOT isValid("regex", arguments.alternatePincode, "[0-9]{6}")>
				<cfset errorMessages.alternatePincode = "Entered value is invalid and is not in the required format." />
			</cfif>
        </cfif>



		<!---checking the validation error message and inserting user data into db --->
		<cfif StructCount(errorMessages) eq 0>
			 <cfset var res = application.dbQuery.updateProfileIntoDb(session.sLogInUserDetails['userID'], arguments.primaryAddressLine1, arguments.primaryAddressLine2, arguments.primaryCountry, arguments.primaryState, arguments.primaryCity, arguments.primaryPincode,
arguments.hasAlternateAddress, arguments.alternateAddressLine1, arguments.alternateAddressLine2, arguments.alternateCountry, arguments.alternateState,
arguments.alternateCity, arguments.alternatePincode) />
			 <cfif res eq false>
				 <cfset errorMessages.password ='Profile Update not Successful, try again!!'/>
			</cfif>
		</cfif>

		<cfreturn errorMessages />
	</cffunction>


</cfcomponent>