<!---
  --- bookServiceManagement
  --- ---------------------
  ---
  --- contains the methods for validation of book services page
  ---
  --- author: mindfire
  --- date:   5/11/20
  --->
<cfcomponent hint="contains the methods for validation of book services page" accessors="true" output="false" persistent="false">

<cffunction name="getProductDetailsForDisplay" access="remote" output="false" return type="query" returnformat = "json">
	<cfargument name="vehicleType" type="string" required="true"/>
	<cfargument name="productType" type="string" required="true"/>
	<cfset var isPart= -1>
	<cfif arguments.productType EQ 'part'>
		<cfset isPart = 1>
	<cfelseif arguments.productType EQ 'service'>
		<cfset isPart = 0>
	</cfif>
	<cfset qProductDetailsForDisplay = application.dbQuery.getProductDetailsForDisplayFromDB(arguments.vehicleType,isPart)/>
	<cfreturn qProductDetailsForDisplay>
</cffunction>


<cffunction name="checkVehicle" access="remote" output="false" return type="boolean" returnformat = "json">
	<cfargument name="vehicleCompany" type="string" required="true"/>
	<cfargument name="vehicleModel" type="string" required="true"/>
	<cfset var returnValue = false>
	<cfif isDefined("session.sLogInUserDetails")>
		<cfset qcheckVehicle = application.dbQuery.checkVehicleFromDB(arguments.vehicleCompany,arguments.vehicleModel,session.sLoginUserDetails['userID'])/>
	<cfelse>
		<cfreturn true />
	</cfif>

	<cfif qcheckVehicle.recordCount EQ 1 OR qcheckVehicle.recordCount GT 1>
		<cfset returnValue = true>
	<cfelseif qcheckVehicle.recordCount EQ 0>
		<cfset returnValue = false>
	</cfif>
	<cfreturn returnValue>
</cffunction>

<cffunction name="checkAddress" access="remote" output="false" return type="boolean" returnformat = "json">
	<cfif isDefined("session.sLogInUserDetails")>
		<cfset qcheckAddress = application.dbQuery.checkAddressFromDB(session.sLoginUserDetails['userID'])/>
	<cfelse>
		<cfreturn true />
	</cfif>
	<cfset var returnValue = false>
	<cfif qcheckAddress.recordCount EQ 1 OR qcheckAddress.recordCount GT 1>
		<cfset returnValue = true>
	<cfelseif qcheckAddress.recordCount EQ 0>
		<cfset returnValue = false>
	</cfif>
	<cfreturn returnValue>
</cffunction>

<cffunction name="getVehicleNumber" access="remote" output="false" return type="query" returnformat = "json">
	<cfargument name="vehicleCompany" type="string" required="true"/>
	<cfargument name="vehicleModel" type="string" required="true"/>
	<cfset qGetVehicleNumber = application.dbQuery.checkVehicleFromDB(arguments.vehicleCompany,arguments.vehicleModel,session.sLoginUserDetails['userID'])/>
	<cfreturn qGetVehicleNumber>
</cffunction>

<cffunction name = "createWorkOrder" access = "remote" output = "false" return type = "Struct" returnformat = "json">
	<cfargument name = "vehicleNumber" type = "string" required = "true">
	<cfargument name = "addressType" type = "string" required = "true">
	<cfargument name = "vehicleCompany" type = "string" required = "true">
	<cfargument name = "vehicleModel" type = "string" required = "true">
	<cfargument name = "productIDArr" type = "string" required = "true">
	<cfset var errorMessages = {}>
	<cfset var productIDArray = listToArray(arguments.productIDArr,",")>
	<cfif arguments.vehicleNumber EQ '' OR NOT isValid("regex", arguments.vehicleNumber, "[A-Z]{2}[ -][0-9]{1,2}(?: [A-Z])?(?: [A-Z]*)? [0-9]{4}")>
		<cfset errorMessages.vehicleNumber = "Invalid Vehicle Number">
	<cfelse>
		<cfset var qvehicleID = application.dbQuery.getvehicleIDFromDb(session.sLoginUserDetails['userID'],arguments.vehicleNumber)>
		<cfset var vehicleID = qvehicleID.vehicleID>
	</cfif>

	<cfif arguments.addressType EQ '' OR NOT isValid("regex", arguments.addressType, "[a-z|A-Z]+([' ']?[a-z|A-Z])*") >
		<cfset errorMessages.addressType = "Invalid Address Type">
	<cfelse>
		<cfif arguments.addressType EQ 'primary'>
			<cfset var isPrimary = 1>
		<cfelseif arguments.addressType EQ 'alternate'>
			<cfset var isPrimary = 0>
		</cfif>
		<cfset qAddressID = application.dbQuery.getAddressIDFromDb(session.sLoginUserDetails['userID'],isPrimary)>
		<cfset var addressID = qAddressID.addressID>
	</cfif>

	<cfif arguments.vehicleCompany EQ '' OR NOT isValid("regex", arguments.vehicleCompany, "[a-z|A-Z]+([' ']?[a-z|A-Z])*")>
		<cfset errorMessages.vehicleCompany = "Invalid Vehicle Company">
	</cfif>

	<cfif arguments.vehicleModel EQ '' OR NOT isValid("regex", arguments.vehicleModel, "[a-zA-Z0-9\s,'/.-]*")>
		<cfset errorMessages.vehicleModel = "Invalid Vehicle Model">
	</cfif>

	<cfif ArrayLen(productIDArray) EQ 0>
		<cfset errorMessages.totalCost = "No Product Selected">
	</cfif>

	<cfif StructCount(errorMessages) EQ 0>
		<cfset var maxEmpCount = 0>
		<cfloop array = "#productIDArray#" index = "i">
			<cfset qEmpCount = application.dbQuery.getProductByIDFromDb(i)>
			<cfset var empCount = qEmpCount.empRequired>
			<cfset maxEmpCount = max(maxEmpCount, empCount)>
		</cfloop>
		 <cfset var startTime = Now()>
		 <cfset var res = application.dbQuery.createWorkOrder(vehicleID, addressID, arguments.productIDArr, startTime, maxEmpCount ) />
			 <cfif res eq false>
				 <cfset errorMessages.addressType ='Work Order Creation not Successful, try again!!'/>
			</cfif>
	</cfif>

	<cfreturn errorMessages />
</cffunction>

<cffunction name="showWorkOrderDetails" access="remote" output="false" return type="query" returnformat="json">
	<cfargument name="workOrderID" type="string" required="true" />
	<cfset var qWorkOrderDetails = application.dbQuery.getWorkOrderByIDFromDb(arguments.workOrderID)>
	<cfreturn qWorkOrderDetails />
</cffunction>
</cfcomponent>