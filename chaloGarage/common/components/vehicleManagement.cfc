<!---
  --- vehicleManagement
  --- -----------------
  ---
  --- this component contains all the methods involving vehicles
  ---
  --- author: mindfire
  --- date:   5/7/20
  --->
<cfcomponent hint="this component contains all the methods involving vehicles" accessors="true" output="false" persistent="false">

<cffunction name="addVehicleValidation" access="remote" output="false" return type="Struct" returnformat = "json">
		<cfargument name = "vehicleCompany" type = "string" required = "true" />
		<cfargument name = "vehicleModel" type = "string" required = "true" />
		<cfargument name = "vehicleType" type = "string" required = "true" />
		<cfargument name = "vehicleDescription" type = "string" required = "false" />
		<cfargument name = "vehicleNumberPlate" type = "string" required = "true" />
		<cfset var errorMessages = {} />
		<cfif arguments.vehicleType EQ 'bike'>
			<cfset variables.vehicleTypeID = 2>
		<cfelseif arguments.vehicleType EQ 'car'>
			<cfset variables.vehicleTypeID = 1>
		</cfif>

		<!---Validate vehicleCompany --->
		<cfif len(arguments.vehicleCompany) gt 30>
			<cfset errorMessages.vehicleCompany = "Value entered is too long , limit is of 30 characters " />
		<cfelseif arguments.vehicleCompany EQ '' OR NOT isValid("regex", arguments.vehicleCompany, "[a-z|A-Z]+([' ']?[a-z|A-Z])*")>
			<cfset errorMessages.vehicleCompany = "Please provide a valid vehicle Company" />
		</cfif>

		<!---Validate vehicleModel --->
		<cfif len(arguments.vehicleModel) gt 30>
			<cfset errorMessages.vehicleModel = "Value entered is too long , limit is of 30 characters " />
		<cfelseif arguments.vehicleModel EQ '' OR NOT isValid("regex", arguments.vehicleModel, "[a-zA-Z0-9\s,'/.-]*")>
			<cfset errorMessages.vehicleModel = "Please provide a valid vehicle Model" />
		</cfif>


		<!---Validate vehicleType --->
		<cfif len(arguments.vehicleType) gt 25>
			<cfset errorMessages.vehicleType = "Value entered is too long , limit is of 25 characters " />
		<cfelseif arguments.vehicleType EQ '' OR NOT isValid("regex", arguments.vehicleType, "[a-z|A-Z]+")>
			<cfset errorMessages.vehicleType = "Please provide a valid vehicle Type" />
		</cfif>

		<!---Validate vehicleDescription--->
		<cfif len(arguments.vehicleDescription) gt 100>
			<cfset errorMessages.vehicleDescription = "Value entered is too long , limit is of 100 characters " />
		<cfelseif arguments.vehicleDescription NEQ '' AND NOT isValid("regex", arguments.vehicleDescription, "[a-zA-Z0-9\s,'/.-]*")>
			<cfset errorMessages.vehicleDescription = "Please provide a valid Vehicle Description" />
		</cfif>

		<!---Validate vehicleNumberPlate--->
		<cfif len(arguments.vehicleNumberPlate) gt 13>
			<cfset errorMessages.vehicleNumberPlate = "Value entered is too long , limit is of 13 characters " />
		<cfelseif arguments.vehicleNumberPlate EQ '' OR NOT isValid('regex',arguments.vehicleNumberPlate,'[A-Z]{2}[ -][0-9]{1,2}(?: [A-Z])?(?: [A-Z]*)? [0-9]{4}') >
			<cfset errorMessages.vehicleNumberPlate = 'Please provide a valid Vehicle Number plate'/>
		<cfelse>
			<cftry>
				<cfset var qVehicleNumberPlateDetails = application.dbQuery.getVehicleNumberPlateFromDb(arguments.vehicleNumberPlate)>
	            <cfcatch type="database">
		            <cflog  text="error : #cfcatch.detail#">
            		<cfset errorMessages.vehicleNumberPlate="Some Error occurred while checking the Vehicle Number Plate. Please, try after sometime!!"/>
	            </cfcatch>
			</cftry>
            <cfif qVehicleNumberPlateDetails.recordCount EQ 1>
                <cfset errorMessages.vehicleNumberPlate = "This Vehicle Number Plate is already used!!">
            </cfif>
		</cfif>

		<cfset var qvehicleDetailsID = application.dbQuery.getVehicleDetailsIDFromDB(arguments.vehicleCompany, arguments.vehicleModel,variables.vehicleTypeID)>
		!---checking the validation error message and inserting user data into db --->
		<cfif StructCount(errorMessages) eq 0>
			 <cfset var res = application.dbQuery.vehicleDataInsertion(session.sLoginUserDetails["userID"], qvehicleDetailsID.vehicleDetailsID) />
			 <cfif res eq false>
				 <cfset errorMessages.vehicleNumberPlate ='Addition of Vehicle is not Successful, try again!!'/>
			</cfif>
		</cfif>

		<cfreturn errorMessages />

</cffunction>

<cffunction name="getVehicleModel" access="remote" output="false" return type="query" returnformat = "json">
		<cfargument name = "vehicleCompany" type = "string" required = "true" />
		<cfset var qVehicleModel = application.dbQuery.getVehicleModelFromDB(arguments.vehicleCompany)>
		<cfreturn qVehicleModel />
</cffunction>

<cffunction name="getVehicleCompany" access="remote" output="false" return type="query" returnformat = "json">
		<cfargument name = "vehicleType" type = "string" required = "true" />
		<cfset var qVehicleCompany = application.dbQuery.getVehicleCompanyFromDB(arguments.vehicleType)>
		<cfreturn qVehicleCompany />
</cffunction>

<cffunction name="getVehicleByID" access="remote" ouptut="false" returntype="query" returnformat = "json">
	<cfargument name = "vehicleID" type = "string" required = "true" />
	<cfset var qvehicleDetails = application.dbQuery.getVehicleDetailsByIDFromDB(arguments.vehicleID)>
	<cfreturn qvehicleDetails />
</cffunction>

<cffunction name="updateVehicleValidation" access="remote" output="false" return type="Struct" returnformat = "json">
		<cfargument name = "vehicleCompany" type = "string" required = "true" />
		<cfargument name = "vehicleModel" type = "string" required = "true" />
		<cfargument name = "vehicleType" type = "string" required = "true" />
		<cfargument name = "vehicleDescription" type = "string" required = "false" />
		<cfargument name = "vehicleNumberPlate" type = "string" required = "true" />
		<cfargument name = "previousVehicleNumberPlate" type = "string" required = "true" />
		<cfargument name = "vehicleID" type = "string" required = "true" />
		<cfset var errorMessages = {} />
		<cfif arguments.vehicleType EQ 'bike'>
			<cfset variables.vehicleTypeID = 2>
		<cfelseif arguments.vehicleType EQ 'car'>
			<cfset variables.vehicleTypeID = 1>
		</cfif>

		<!---Validate vehicleCompany --->
		<cfif len(arguments.vehicleCompany) gt 30>
			<cfset errorMessages.vehicleCompany = "Value entered is too long , limit is of 30 characters " />
		<cfelseif arguments.vehicleCompany EQ '' OR NOT isValid("regex", arguments.vehicleCompany, "[a-z|A-Z]+([' ']?[a-z|A-Z])*")>
			<cfset errorMessages.vehicleCompany = "Please provide a valid vehicle Company" />
		</cfif>

		<!---Validate vehicleModel --->
		<cfif len(arguments.vehicleModel) gt 30>
			<cfset errorMessages.vehicleModel = "Value entered is too long , limit is of 30 characters " />
		<cfelseif arguments.vehicleModel EQ '' OR NOT isValid("regex", arguments.vehicleModel, "[a-zA-Z0-9\s,'/.-]*")>
			<cfset errorMessages.vehicleModel = "Please provide a valid vehicle Model" />
		</cfif>


		<!---Validate vehicleType --->
		<cfif len(arguments.vehicleType) gt 25>
			<cfset errorMessages.vehicleType = "Value entered is too long , limit is of 25 characters " />
		<cfelseif arguments.vehicleType EQ '' OR NOT isValid("regex", arguments.vehicleType, "[a-z|A-Z]+")>
			<cfset errorMessages.vehicleType = "Please provide a valid vehicle Type" />
		</cfif>

		<!---Validate vehicleDescription--->
		<cfif len(arguments.vehicleDescription) gt 100>
			<cfset errorMessages.vehicleDescription = "Value entered is too long , limit is of 100 characters " />
		<cfelseif arguments.vehicleDescription NEQ '' AND NOT isValid("regex", arguments.vehicleDescription, "[a-zA-Z0-9\s,'/.-]*")>
			<cfset errorMessages.vehicleDescription = "Please provide a valid Vehicle Description" />
		</cfif>

		<!---Validate vehicleNumberPlate--->
		<cfif len(arguments.vehicleNumberPlate) gt 13>
			<cfset errorMessages.vehicleNumberPlate = "Value entered is too long , limit is of 13 characters " />
		<cfelseif arguments.vehicleNumberPlate EQ '' OR NOT isValid('regex',arguments.vehicleNumberPlate,'[A-Z]{2}[ -][0-9]{1,2}(?: [A-Z])?(?: [A-Z]*)? [0-9]{4}') >
			<cfset errorMessages.vehicleNumberPlate = 'Please provide a valid Vehicle Number plate'/>
		<cfelse>
			<cftry>
				<cfset var qVehicleNumberPlateDetails = application.dbQuery.getVehicleNumberPlateFromDb(arguments.vehicleNumberPlate)>
	            <cfcatch type="database">
		            <cflog  text="error : #cfcatch.detail#">
            		<cfset errorMessages.vehicleNumberPlate="Some Error occurred while checking the Vehicle Number Plate. Please, try after sometime!!"/>
	            </cfcatch>
			</cftry>
            <cfif qVehicleNumberPlateDetails.recordCount EQ 1 AND  arguments.vehicleNumberPlate NEQ arguments.previousVehicleNumberPlate>
                <cfset errorMessages.vehicleNumberPlate = "This Vehicle Number Plate is already used!!">
            </cfif>
		</cfif>

		<cfset var qvehicleDetailsID = application.dbQuery.getVehicleDetailsIDFromDB(arguments.vehicleCompany, arguments.vehicleModel,variables.vehicleTypeID)>
		<!---checking the validation error message and updating vehicle data into db --->
		<cfif StructCount(errorMessages) eq 0>
			 <cfset var res = application.dbQuery.vehicleDataUpdate(session.sLoginUserDetails["userID"], qvehicleDetailsID.vehicleDetailsID, arguments.vehicleID) />
			 <cfif res eq false>
				 <cfset errorMessages.vehicleNumberPlate ='Update of Vehicle is not Successful, try again!!'/>
			</cfif>
		</cfif>

		<cfreturn errorMessages />

</cffunction>

</cfcomponent>