<!---
  --- dbQuery
  --- -------
  ---
  --- this component contains the queries required that are used in the rest of components
  ---
  --- author: mindfire
  --- date:   4/15/20
  --->
<cfcomponent hint="this component contains the queries required that are used in the rest of components" accessors="true" output="false" persistent="false">
	<cffunction name="getEmailFromDb" returntype="query" output="false" access="remote"
				description="this function is used to return query with email present in database">
		<cfargument name="inputEmail" type="string" required="true">

		 <cfquery name = "qEmailDetails" datasource = "chaloGarage">
	                SELECT	email
	                FROM 	[dbo].[communication]
	                WHERE 	email=<cfqueryparam value="#arguments.inputEmail#" cfsqltype="CF_SQL_VARCHAR" />
	            </cfquery>
	        <cfreturn qEmailDetails />
	</cffunction>


	<cffunction name="getPhoneNumberFromDb" returntype="query" output="false" access="remote"
				description="this function is used to return query with phone number present in database">
		<cfargument name="inputPhoneNumber" type="string" required="true">

		 <cfquery name = "qPhoneNumberDetails" datasource = "chaloGarage">
                SELECT	phoneNumber
                FROM 	[dbo].[communication]
                WHERE 	phoneNumber=<cfqueryparam value="#arguments.inputPhoneNumber#"
						cfsqltype="CF_SQL_NUMERIC" />
            </cfquery>
	        <cfreturn qPhoneNumberDetails />
	</cffunction>


	<cffunction name="getUserNameFromDb" returntype="query" output="false" access="remote"
				description="this function is used to return query with user name present in database">
		<cfargument name="inputUserName" type="string" required="true">

		 <cfquery name = "qUserNameDetails" datasource = "chaloGarage">
                SELECT	userName
                FROM 	[dbo].[user]
                WHERE 	userName=<cfqueryparam value="#arguments.inputUserName#" cfsqltype="CF_SQL_VARCHAR" />;
            </cfquery>
	        <cfreturn qUserNameDetails />
	</cffunction>


	<cffunction name = "getUserIDFromDb" access = "remote" output = "false" returnType = "query"
				description = "this function is used to retrive the userId based on unique userName">
	 	<cfargument name="inputUserName" type="string" required="true">

        <cfquery name="qGetUserID"  datasource = "chaloGarage">
			SELECT	userID
			FROM 	[dbo].[user]
			WHERE 	userName=<cfqueryparam value = "#arguments.inputUserName#" CFSQLType = "CF_SQL_VARCHAR" />
        </cfquery>
        <cfreturn qGetUserID />
</cffunction>


<cffunction name = "dataInsertion" access = "remote" output = "false" returntype = "boolean"
			description = "this function is used to insert data into database">
	<cfset var isCommit=false />
	<cftransaction>
		<cftry>
	        <cfquery name = "qDataInsert">
				INSERT INTO	[dbo].[user] (firstName, lastName, userName, password, personTypeID)
				VALUES	(
							<cfqueryparam value = "#trim(form.firstname)#" CFSQLType = "CF_SQL_VARCHAR" />,
							<cfqueryparam value = "#trim(form.lastname)#" CFSQLType = "CF_SQL_VARCHAR" />,
							<cfqueryparam value = "#trim(form.username)#" CFSQLType = "CF_SQL_VARCHAR" />,
							<cfqueryparam value = "#trim(form.password)#" CFSQLType = "CF_SQL_VARCHAR" />,
							<cfqueryparam value = 1 CFSQLType = "CF_SQL_BIGINT" />
						)
			</cfquery>
			<cfset var quserID = application.dbQuery.getUserIDFromDb(form.username)>
			<cfquery name= "qInsertCommDetails">
				INSERT INTO	[dbo].[communication] (phoneNumber, email, userID)
				VALUES	(
							<cfqueryparam value = "#trim(form.phonenumber)#" CFSQLType = "CF_SQL_NUMERIC" />,
							<cfqueryparam value = "#trim(form.email)#" CFSQLType = "CF_SQL_VARCHAR" />,
							<cfqueryparam value = "#quserID.userID#" CFSQLType = "CF_SQL_BIGINT" />
						)
	        </cfquery>
			<cftransaction action="commit" />
			<cfset isCommit=true />
			<cfcatch type="database">
				<cftransaction action="rollback" />
				<cfset isCommit=false />
				<cflog  text="error : #cfcatch.detail#">
			</cfcatch>
		</cftry>
		</cftransaction>
		<cfreturn isCommit>
</cffunction>

<cffunction name="getUserDetailsFromDb" returntype="query" output="false" access="remote"
			description="this function is used to return query with user details present in database">
		<cfargument name="inputUserName" type="string" required="true">
		<cfargument name="inputPassword" type="string" required="true">
		<cfquery name="qLoginUserDetails" datasource="chaloGarage">
				SELECT		firstName, lastName, userID, userName, password, personCategory
				FROM 		[dbo].[user]
				INNER JOIN	[dbo].[personType]
				ON 			[user].personTypeID = personType.personTypeID
				WHERE 		[user].userName = <cfqueryparam value = "#arguments.inputUserName#"
							cfsqltype="CF_SQL_VARCHAR" />
				AND 		[user].password = <cfqueryparam value = "#arguments.inputPassword#"
							cfsqltype="CF_SQL_VARCHAR" />
			</cfquery>
	        <cfreturn qLoginUserDetails />
	</cffunction>

<cffunction name="getUserProfileDetailsFromDb" returntype="query" output="false" access="remote"
			description="this function is used to return query with user profile details present in database">
		<cfargument name="userID" type="numeric" required="true">

		<cfquery name="qUserProfileDetails" datasource="chaloGarage">
				SELECT		firstName, lastName, userName, password, phoneNumber, altPhoneNumber, email, altEmail
				FROM 		[dbo].[user]
				INNER JOIN	[dbo].[communication] as comm
				ON 			[user].userID = comm.userID
				WHERE 		[user].userID = <cfqueryparam value = "#arguments.userID#"
							cfsqltype="CF_SQL_BIGINT" />
			</cfquery>
	        <cfreturn qUserProfileDetails />
	</cffunction>

<cffunction name="getUserAddressDetailsFromDb" returntype="query" output="false" access="remote"
			description="this function is used to return query with user address details present in database">
		<cfargument name="userID" type="numeric" required="true">

		<cfquery name="qUserAddressDetails" datasource="chaloGarage">
				SELECT		addressLane1, addressLane2, city, state, country, pinCode, isPrimary
				FROM 		[dbo].[user]
				INNER JOIN	[dbo].[address]
				ON 			[user].userID = address.userID
				WHERE 		[user].userID = <cfqueryparam value = "#arguments.userID#"
							cfsqltype="CF_SQL_BIGINT" />
			</cfquery>
	        <cfreturn qUserAddressDetails />
	</cffunction>

<cffunction name = "updateProfileIntoDb" access = "remote" output = "false" returntype = "boolean" description = "this function is used to update profile data into database">
		<cfargument name = "userID" type = "numeric" required = "true" />
		<cfargument name = "primaryAddressLine1" type="string" required = "true"/>
		<cfargument name = "primaryAddressLine2" type="string" required = "true"/>
		<cfargument name = "primaryCountry" type="string" required = "true"/>
		<cfargument name = "primaryState" type="string" required = "true"/>
		<cfargument name = "primaryCity" type="string" required = "true"/>
		<cfargument name = "primaryPincode" type="string" required = "true"/>
		<cfargument name = "hasAlternateAddress" type="boolean" required = "true"/>
		<cfargument name = "alternateAddressLine1" type="string" required="false"/>
		<cfargument name = "alternateAddressLine2" type="string" required="false"/>
        <cfargument name = "alternateCountry" type="string" required="false"/>
        <cfargument name = "alternateState" type="string" required="false"/>
        <cfargument name = "alternateCity" type="string" required="false"/>
        <cfargument name = "alternatePincode" type="string" required="false"/>
		<cfset variables.userAddress = application.dbQuery.getUserAddressDetailsFromDb(arguments.userID)>
		<cfset var isCommit=false />
		<cftransaction>
		<cftry>
	        <cfquery name = "qProfileUpdate">
				UPDATE 		[dbo].[user]
				SET 		firstName =	<cfqueryparam value = "#trim(form.firstName)#" CFSQLType = "CF_SQL_VARCHAR" />,
							lastName  =	<cfqueryparam value = "#trim(form.lastName)#" CFSQLType = "CF_SQL_VARCHAR" />,
							userName  =	<cfqueryparam value = "#trim(form.userName)#" CFSQLType = "CF_SQL_VARCHAR" />,
							password  =	<cfqueryparam value = "#trim(form.password)#" CFSQLType = "CF_SQL_VARCHAR" />
				WHERE		userID    = <cfqueryparam value = "#arguments.userID#" CFSQLType = "CF_SQL_BIGINT" />
			</cfquery>
			<cfquery name= "qProfileUpdateCommDetails">
				UPDATE 		[dbo].[communication]
				SET			phoneNumber = <cfqueryparam value = "#trim(form.phoneNumber)#" CFSQLType = "CF_SQL_NUMERIC" />,
							email	    = <cfqueryparam value = "#trim(form.email)#" CFSQLType = "CF_SQL_VARCHAR" />,
							altPhoneNumber = <cfif form.altPhoneNumber EQ ''>
												<cfqueryparam value = "#trim(form.altPhoneNumber)#" CFSQLType = "CF_SQL_NUMERIC" null="yes"/>,
											<cfelse>
												<cfqueryparam value = "#trim(form.altPhoneNumber)#" CFSQLType = "CF_SQL_NUMERIC" null="no"/>,
											</cfif>
							altEmail = <cfif form.altEmail EQ ''>
												<cfqueryparam value = "#trim(form.altEmail)#" CFSQLType = "CF_SQL_VARCHAR" null="yes"/>
											<cfelse>
												<cfqueryparam value = "#trim(form.altEmail)#" CFSQLType = "CF_SQL_VARCHAR" null="no"/>
											</cfif>
				WHERE		userID      = <cfqueryparam value = "#arguments.userID#" CFSQLType = "CF_SQL_BIGINT" />
	        </cfquery>
	        <cfif variables.userAddress.recordCount EQ 0>
	        	<cfquery name= "qProfileInsertPrimaryAddressDetails">
				INSERT INTO	[dbo].[address] (addressLane1, addressLane2, country, state, city, pinCode, isPrimary, userID)
				VALUES	(
							<cfqueryparam value = "#arguments.primaryAddressLine1#" CFSQLType = "CF_SQL_VARCHAR">,
							<cfif arguments.primaryAddressLine2 EQ ''>
								<cfqueryparam value = "#arguments.primaryAddressLine2#" CFSQLType = "CF_SQL_VARCHAR" null="yes">,
							<cfelse>
								<cfqueryparam value = "#arguments.primaryAddressLine2#" CFSQLType = "CF_SQL_VARCHAR" null="no">,
							</cfif>
							<cfqueryparam value = "#arguments.primaryCountry#" CFSQLType = "CF_SQL_VARCHAR">,
							<cfqueryparam value = "#arguments.primaryState#" CFSQLType = "CF_SQL_VARCHAR">,
							<cfqueryparam value = "#arguments.primaryCity#" CFSQLType = "CF_SQL_VARCHAR">,
							<cfqueryparam value = "#arguments.primaryPincode#" CFSQLType = "CF_SQL_NUMERIC">,
							<cfqueryparam value = "1" CFSQLType ="CF_SQL_BIT">,
							<cfqueryparam value = "#arguments.userID#" CFSQLType = "CF_SQL_BIGINT" />
						)
	        	</cfquery>
	        	<cfquery name= "qProfileInsertAlternateAddressDetails">
				INSERT INTO	[dbo].[address] (addressLane1, addressLane2, country, state, city, pinCode, isPrimary, userID)
				VALUES	(
							<cfqueryparam value = "#arguments.alternateAddressLine1#" CFSQLType = "CF_SQL_VARCHAR">,
							<cfif arguments.primaryAddressLine2 EQ ''>
								<cfqueryparam value = "#arguments.alternateAddressLine2#" CFSQLType = "CF_SQL_VARCHAR" null="yes">,
							<cfelse>
								<cfqueryparam value = "#arguments.alternateAddressLine2#" CFSQLType = "CF_SQL_VARCHAR" null="no">,
							</cfif>
							<cfqueryparam value = "#arguments.alternateCountry#" CFSQLType = "CF_SQL_VARCHAR">,
							<cfqueryparam value = "#arguments.alternateState#" CFSQLType = "CF_SQL_VARCHAR">,
							<cfqueryparam value = "#arguments.alternateCity#" CFSQLType = "CF_SQL_VARCHAR">,
							<cfqueryparam value = "#arguments.alternatePincode#" CFSQLType = "CF_SQL_NUMERIC">,
							<cfqueryparam value = "0" CFSQLType ="CF_SQL_BIT">,
							<cfqueryparam value = "#arguments.userID#" CFSQLType = "CF_SQL_BIGINT" />
						)
	        	</cfquery>
			<cfelseif variables.userAddress.recordCount EQ 1>
				<cfquery name = "qProfileUpdatePrimaryAddressDetails">
		        UPDATE 		[dbo].[address]
		        SET			addressLane1 = <cfqueryparam value = "#arguments.primaryAddressLine1#" CFSQLType = "CF_SQL_VARCHAR">,
		        			addressLane2 = <cfif arguments.primaryAddressLine2 EQ ''>
												<cfqueryparam value = "#arguments.primaryAddressLine2#" CFSQLType = "CF_SQL_VARCHAR" null="yes">,
											<cfelse>
												<cfqueryparam value = "#arguments.primaryAddressLine2#" CFSQLType = "CF_SQL_VARCHAR" null="no">,
											</cfif>
							country		 = <cfqueryparam value = "#arguments.primaryCountry#" CFSQLType = "CF_SQL_VARCHAR">,
							state		 = <cfqueryparam value = "#arguments.primaryState#" CFSQLType = "CF_SQL_VARCHAR">,
							city		 = <cfqueryparam value = "#arguments.primaryCity#" CFSQLType = "CF_SQL_VARCHAR">,
							pinCode		 = <cfqueryparam value = "#arguments.primaryPincode#" CFSQLType = "CF_SQL_NUMERIC">,
							isPrimary	 = <cfqueryparam value = "1" CFSQLType ="CF_SQL_BIT">
				WHERE		userID		 = <cfqueryparam value = "#arguments.userID#" CFSQLType = "CF_SQL_BIGINT" />
	        	</cfquery>
	        	<cfif arguments.hasAlternateAddress EQ true>
		        	<cfquery name= "qProfileInsertAlternateAddressDetails">
							INSERT INTO	[dbo].[address] (addressLane1, addressLane2, country, state, city, pinCode, isPrimary, userID)
							VALUES	(
										<cfqueryparam value = "#arguments.alternateAddressLine1#" CFSQLType = "CF_SQL_VARCHAR">,
										<cfif arguments.alternateAddressLine2 EQ ''>
											<cfqueryparam value = "#arguments.alternateAddressLine2#" CFSQLType = "CF_SQL_VARCHAR" null="yes">,
										<cfelse>
											<cfqueryparam value = "#arguments.alternateAddressLine2#" CFSQLType = "CF_SQL_VARCHAR" null="no">,
										</cfif>
										<cfqueryparam value = "#arguments.alternateCountry#" CFSQLType = "CF_SQL_VARCHAR">,
										<cfqueryparam value = "#arguments.alternateState#" CFSQLType = "CF_SQL_VARCHAR">,
										<cfqueryparam value = "#arguments.alternateCity#" CFSQLType = "CF_SQL_VARCHAR">,
										<cfqueryparam value = "#arguments.alternatePincode#" CFSQLType = "CF_SQL_NUMERIC">,
										<cfqueryparam value = "0" CFSQLType ="CF_SQL_BIT">,
										<cfqueryparam value = "#arguments.userID#" CFSQLType = "CF_SQL_BIGINT" />
									)
	        		</cfquery>
		        </cfif>

			<cfelseif variables.userAddress.recordCount EQ 2>
				<cfquery name = "qProfileUpdatePrimaryAddressDetails">
		        UPDATE 		[dbo].[address]
		        SET			addressLane1 = <cfqueryparam value = "#arguments.primaryAddressLine1#" CFSQLType = "CF_SQL_VARCHAR">,
		        			addressLane2 = <cfif arguments.primaryAddressLine2 EQ ''>
												<cfqueryparam value = "#arguments.primaryAddressLine2#" CFSQLType = "CF_SQL_VARCHAR" null="yes">,
											<cfelse>
												<cfqueryparam value = "#arguments.primaryAddressLine2#" CFSQLType = "CF_SQL_VARCHAR" null="no">,
											</cfif>
							country		 = <cfqueryparam value = "#arguments.primaryCountry#" CFSQLType = "CF_SQL_VARCHAR">,
							state		 = <cfqueryparam value = "#arguments.primaryState#" CFSQLType = "CF_SQL_VARCHAR">,
							city		 = <cfqueryparam value = "#arguments.primaryCity#" CFSQLType = "CF_SQL_VARCHAR">,
							pinCode		 = <cfqueryparam value = "#arguments.primaryPincode#" CFSQLType = "CF_SQL_NUMERIC">,
							isPrimary	 = <cfqueryparam value = "1" CFSQLType ="CF_SQL_BIT">
				WHERE		userID		 = <cfqueryparam value = "#arguments.userID#" CFSQLType = "CF_SQL_BIGINT" />
				AND 		isPrimary	 = 1;
	        	</cfquery>

	        	<cfquery name = "qProfileUpdateAlternateAddressDetails">
		        UPDATE 		[dbo].[address]
		        SET			addressLane1 = <cfqueryparam value = "#arguments.alternateAddressLine1#" CFSQLType = "CF_SQL_VARCHAR">,
		        			addressLane2 = <cfif arguments.alternateAddressLine2 EQ ''>
												<cfqueryparam value = "#arguments.alternateAddressLine2#" CFSQLType = "CF_SQL_VARCHAR" null="yes">,
											<cfelse>
												<cfqueryparam value = "#arguments.alternateAddressLine2#" CFSQLType = "CF_SQL_VARCHAR" null="no">,
											</cfif>
							country		 = <cfqueryparam value = "#arguments.alternateCountry#" CFSQLType = "CF_SQL_VARCHAR">,
							state		 = <cfqueryparam value = "#arguments.alternateState#" CFSQLType = "CF_SQL_VARCHAR">,
							city		 = <cfqueryparam value = "#arguments.alternateCity#" CFSQLType = "CF_SQL_VARCHAR">,
							pinCode		 = <cfqueryparam value = "#arguments.alternatePincode#" CFSQLType = "CF_SQL_NUMERIC">,
							isPrimary	 = <cfqueryparam value = "0" CFSQLType ="CF_SQL_BIT">
				WHERE		userID		 = <cfqueryparam value = "#arguments.userID#" CFSQLType = "CF_SQL_BIGINT" />
				AND			isPrimary 	 = 0;
	        	</cfquery>

			</cfif>
			<cftransaction action="commit" />
			<cfset isCommit=true />
			<cfcatch type="database">
				<cftransaction action="rollback" />
				<cfset isCommit=false />
				<cflog  text="error : #cfcatch.detail#">
			</cfcatch>
		</cftry>
		</cftransaction>
		<cfreturn isCommit>
</cffunction>

<cffunction name="getVehicleCompanyFromDb" returntype="query" output="false" access="remote"
				description="this function is used to return query with vehicle company present in database">
		 <cfargument name="vehicleType" type="string" required="true">
		 <cfquery name = "qVehicleCompanyDetails" datasource = "chaloGarage">
                SELECT	DISTINCT 	vehicleCompany
                FROM 				[dbo].[vehicleDetail] as vd
				INNER	JOIN		[dbo].[vehicleType] as vt
				ON					vd.vehicleTypeID = vt.vehicleTypeID
				WHERE				vehicleCategory = <cfqueryparam value="#arguments.vehicleType#" cfsqltype="CF_SQL_VARCHAR" />;
         </cfquery>
	     <cfreturn qVehicleCompanyDetails />
</cffunction>

<cffunction name="getVehicleNumberPlateFromDb" access="remote" output="false" returntype="query" >
	<cfargument name="vehicleNumberPlate" type="string" required="true"/>
	<cfquery name="qVehicleNumberPlateDetails">
		SELECT		vehicleNumber
		FROM 		[dbo].[vehicle]
		WHERE		vehicleNumber=<cfqueryparam value="#arguments.vehicleNumberPlate#" cfsqltype="CF_SQL_VARCHAR" />
	</cfquery>
	<cfreturn qVehicleNumberPlateDetails />
</cffunction>

<cffunction name = "vehicleDataInsertion" access = "remote" output = "false" returntype = "boolean"
			description = "this function is used to insert vehicle data into database">
				<cfargument name="userID" type="string" required="true">
				<cfargument name="vehicleDetailsID" type="string" required="true">
	<cfset var isCommit=false />
	<cftransaction>
		<cftry>
	        <cfquery name = "qVehicleDataInsert">
				INSERT INTO	[dbo].[vehicle] (vehicleDetailsID, vehicleDescription, vehicleNumber, userID)
				VALUES	(
							<cfqueryparam value = "#trim(arguments.vehicleDetailsID)#" CFSQLType = "CF_SQL_BIGINT" />,
							<cfqueryparam value = "#trim(form.vehicleDescription)#" CFSQLType = "CF_SQL_VARCHAR" />,
							<cfqueryparam value = "#trim(form.vehicleNumberPlate)#" CFSQLType = "CF_SQL_VARCHAR" />,
							<cfqueryparam value = "#trim(arguments.userID)#" CFSQLType = "CF_SQL_BIGINT" />
						)
			</cfquery>
			<cftransaction action="commit" />
			<cfset isCommit=true />
			<cfcatch type="database">
				<cftransaction action="rollback" />
				<cfset isCommit=false />
				<cflog  text="error : #cfcatch.detail#">
			</cfcatch>
		</cftry>
		</cftransaction>
		<cfreturn isCommit>
</cffunction>

<cffunction name = "vehicleDataUpdate" access = "remote" output = "false" returntype = "boolean"
			description = "this function is used to insert vehicle data into database">
				<cfargument name="userID" type="string" required="true">
				<cfargument name="vehicleDetailsID" type="string" required="true">
				<cfargument name="vehicleID" type="string" required="true">
	<cfset var isCommit=false />
	<cftransaction>
		<cftry>
	        <cfquery name = "qVehicleDataUpdate">
				UPDATE	[dbo].[vehicle]
				SET		vehicleDetailsID	=	<cfqueryparam value = "#trim(arguments.vehicleDetailsID)#" CFSQLType = "CF_SQL_BIGINT" />,
						vehicleDescription	=	<cfqueryparam value = "#trim(form.vehicleDescription)#" CFSQLType = "CF_SQL_VARCHAR" />,
						vehicleNumber		=	<cfqueryparam value = "#trim(form.vehicleNumberPlate)#" CFSQLType = "CF_SQL_VARCHAR" />
				WHERE	userID				=	<cfqueryparam value = "#trim(arguments.userID)#" CFSQLType = "CF_SQL_BIGINT" />
				AND 	vehicleID				=	<cfqueryparam value = "#trim(arguments.vehicleID)#" CFSQLType = "CF_SQL_BIGINT" />
			</cfquery>
			<cftransaction action="commit" />
			<cfset isCommit=true />
			<cfcatch type="database">
				<cftransaction action="rollback" />
				<cfset isCommit=false />
				<cflog  text="error : #cfcatch.detail#">
			</cfcatch>
		</cftry>
		</cftransaction>
		<cfreturn isCommit>
</cffunction>

<cffunction name="getVehicleDetailsIDFromDB" access="remote" output="false" returntype="query" >
	<cfargument name="vehicleCompany" type="string" required="true"/>
	<cfargument name="vehicleModel" type="string" required="true"/>
	<cfargument name="vehicleTypeID" type="string" required="true"/>
	<cfquery name="qVehicleDetailsID">
		SELECT		vehicleDetailsID
		FROM 		[dbo].[vehicleDetail]
		WHERE		vehicleCompany=<cfqueryparam value="#arguments.vehicleCompany#" cfsqltype="CF_SQL_VARCHAR" />
		AND			vehicleModel=<cfqueryparam value="#arguments.vehicleModel#" cfsqltype="CF_SQL_VARCHAR" />
		AND			vehicleTypeID=<cfqueryparam value="#arguments.vehicleTypeID#" cfsqltype="CF_SQL_BIGINT" />
	</cfquery>
	<cfreturn qVehicleDetailsID />
</cffunction>

<cffunction name="getVehicleModelFromDB" returntype="query" output="false" access="remote"
				description="this function is used to return query with vehicle model present in database">
		<cfargument name="vehicleCompany" type="string" required="true">

		 <cfquery name = "qVehicleModel" datasource = "chaloGarage">
                SELECT	DISTINCT	vehicleModel
                FROM 				[dbo].[vehicleDetail]
                WHERE 				vehicleCompany = <cfqueryparam value="#arguments.vehicleCompany#" cfsqltype="CF_SQL_VARCHAR" />;
            </cfquery>
	        <cfreturn qVehicleModel />
	</cffunction>

<cffunction name="getVehicleDetailsFromDb" returntype="Query" output="false" access="remote">
	<cfargument name="userID" type="string" required="true">
	<cfquery name="qVehicleDetailsFromDb" datasource="chaloGarage">
		SELECT		vehicleID,vehicleDescription, vehicleNumber, vehicleCompany, vehicleModel, vehicleCategory
		FROM 		vehicle
		INNER JOIN	vehicleDetail as vd
		ON			vehicle.vehicleDetailsID = vd.vehicleDetailsID
		INNER JOIN	vehicleType as vt
		ON			vd.vehicleTypeID = vt.vehicleTypeID
		WHERE		userID = <cfqueryparam value="#arguments.userID#" cfsqltype="CF_SQL_BIGINT" />
	</cfquery>
	<cfreturn qVehicleDetailsFromDb />
</cffunction>

<cffunction name="getVehicleDetailsByIDFromDB" returntype="Query" output="false" access="remote">
	<cfargument name="vehicleID" type="string" required="true">
	<cfquery name="qVehicleDetailsByIDFromDB" datasource="chaloGarage">
		SELECT		vehicleCategory, vehicleCompany, vehicleModel, vehicleDescription, vehicleNumber
		FROM 		vehicle
		INNER JOIN	vehicleDetail as vd
		ON			vehicle.vehicleDetailsID = vd.vehicleDetailsID
		INNER JOIN	vehicleType as vt
		ON			vd.vehicleTypeID = vt.vehicleTypeID
		WHERE		vehicleID = <cfqueryparam value="#arguments.vehicleID#" cfsqltype="CF_SQL_BIGINT" />
	</cfquery>
	<cfreturn qVehicleDetailsByIDFromDB />
</cffunction>

<cffunction name="getProductDetailsForDisplayFromDB" returntype="query" output="false" access="remote">
	<cfargument name="vehicleType" type="string" required="true"/>
	<cfargument name="ispart" type="string" required="true"/>
	<cfquery name="qProductDetailsForDisplayFromDB" datasource="chaloGarage">
		SELECT		productName, price, productID
		FROM 		[dbo].[product]
		INNER JOIN	[dbo].[vehicleType] as vt
		ON			product.vehicleTypeID = vt.vehicleTypeID
		WHERE		isPart 			= <cfqueryparam value="#arguments.isPart#" cfsqltype="CF_SQL_BIT" />
		AND			vehicleCategory	= <cfqueryparam value="#arguments.vehicleType#" cfsqltype="CF_SQL_VARCHAR" />
	</cfquery>
	<cfreturn qProductDetailsForDisplayFromDB>
</cffunction>

<cffunction name="checkVehicleFromDB" returntype="query" output="false" access="remote">
	<cfargument name="vehicleCompany" type="string" required="true"/>
	<cfargument name="vehicleModel" type="string" required="true"/>
	<cfargument name="userID" type="string" required="true"/>
	<cfquery name="qCheckVehicleFromDB" datasource="chaloGarage">
		SELECT		vehicleNumber, vehicleCompany , vehicleModel
		FROM 		[dbo].[vehicle]
		INNER JOIN	[dbo].[vehicleDetail] as vd
		ON			vehicle.vehicleDetailsID = vd.vehicleDetailsID
		WHERE		vehicleCompany 	= <cfqueryparam value="#arguments.vehicleCompany#" cfsqltype="CF_SQL_VARCHAR" />
		AND			vehicleModel	= <cfqueryparam value="#arguments.vehicleModel#" cfsqltype="CF_SQL_VARCHAR" />
		AND			userID			= <cfqueryparam value="#arguments.userID#" cfsqltype="CF_SQL_BIGINT" />
	</cfquery>
	<cfreturn qCheckVehicleFromDB>
</cffunction>

<cffunction name="checkAddressFromDB" returntype="query" output="false" access="remote">
	<cfargument name="userID" type="string" required="true"/>
	<cfquery name="qCheckAddressFromDB" datasource="chaloGarage">
		SELECT		*
		FROM 		[dbo].[address]
		WHERE		userID	= <cfqueryparam value="#arguments.userID#" cfsqltype="CF_SQL_BIGINT" />
	</cfquery>
	<cfreturn qCheckAddressFromDB>
</cffunction>

<cffunction name="getAddressIDFromDb" returntype="query" output="false" access="remote"
			description="this function is used to return query with user address ID present in database">
		<cfargument name="userID" type="string" required="true">
		<cfargument name= "isPrimary" type="string" required="true">
		<cfquery name="qAddressIDFromDb" datasource="chaloGarage">
				SELECT		addressID
				FROM 		[dbo].[address]
				WHERE 		userID = <cfqueryparam value = "#arguments.userID#"
							cfsqltype="CF_SQL_BIGINT" />
				AND 		isPrimary 	  =  <cfqueryparam value = "#arguments.isPrimary#"
							cfsqltype="CF_SQL_BIT" />
			</cfquery>
	        <cfreturn qAddressIDFromDb />
</cffunction>

<cffunction name="getvehicleIDFromDb" returntype="query" output="false" access="remote"
			description="this function is used to return query with user vehicle ID present in database">
		<cfargument name="userID" type="string" required="true">
		<cfargument name= "vehicleNumber" type="string" required="true">
		<cfquery name="qVehicleIDFromDb" datasource="chaloGarage">
				SELECT		vehicleID
				FROM 		[dbo].[vehicle]
				WHERE 		userID = <cfqueryparam value = "#arguments.userID#"
							cfsqltype="CF_SQL_BIGINT" />
				AND 		vehicleNumber 	  =  <cfqueryparam value = "#arguments.vehicleNumber#"
							cfsqltype="CF_SQL_VARCHAR" />
			</cfquery>
	        <cfreturn qVehicleIDFromDb />
</cffunction>

<cffunction name="getWorkOrderIDFromDb" returntype = "query" output="false" access="remote"
			description="used to get work Order ID">
	<cfargument name = "vehicleID" type = "string" required = "true">
	<cfargument name = "startTime" type = "string" required = "true">
	<cfset var startTimeArr = listToArray(arguments.startTime,"'")>
	<cfquery name="qWorkOrderIDFromDb" datasource="chaloGarage">
		SELECT		wo.workOrderID
		FROM		[dbo].[workOrder] as wo
		WHERE		vehicleID = <cfqueryparam value = "#arguments.vehicleID#" CFSQLTYPE = "CF_SQL_BIGINT">
		AND			startTime = <cfqueryparam value = "#startTimeArr[2]#" CFSQLTYPE = "CF_SQL_TIMESTAMP">
	</cfquery>
	<cfreturn qWorkOrderIDFromDb>
</cffunction>

<cffunction name="getUserIDForWorkOrder" returntype = "query" output="false" access="remote"
			description="used to get user ID for Work Order">
		<cfquery name = "qUserIDForWorkOrder" datasource = "chaloGarage">
			SELECT		[user].userID
			FROM		[dbo].[user]
			left join	[dbo].[userWorkOrder] as uwd
			ON			[user].userID = uwd.userID
			left JOIN	[dbo].[workOrder] as wd
			ON			uwd.workOrderID = wd.workOrderID
			WHERE		personTypeID = 3
			AND			uwd.workOrderID IS	NULL
			OR			personTypeID = 3
			AND			uwd.workOrderID IS	NOT NULL
			AND			wd.endTime IS NOT NULL
			EXCEPT
			SELECT		[user].userID
			FROM		[dbo].[user]
			left join	[dbo].[userWorkOrder] as uwd
			ON			[user].userID = uwd.userID
			left JOIN	[dbo].[workOrder] as wd
			ON			uwd.workOrderID = wd.workOrderID
			WHERE		personTypeID = 3
			AND			uwd.workOrderID IS	NOT NULL
			AND			wd.endTime IS NULL
		</cfquery>
		<cfreturn qUserIDForWorkOrder>
</cffunction>

<cffunction name="getProductByIDFromDb" access="remote" output="false" returntype="query" >
	<cfargument name="productID" type="string" required="true">
	<cfquery name="qProductByIDDetails">
		SELECT		productName, productDescription, isPart, vehicleCategory, price, duration, empRequired
		FROM 		[dbo].[product]
		INNER JOIN	[dbo].[vehicleType]
		ON			product.vehicleTypeID = vehicleType.vehicleTypeID
		WHERE		productID = <cfqueryparam value="#arguments.productID#" cfsqltype="CF_SQL_BIGINT" />
	</cfquery>
	<cfreturn qProductByIDDetails />
</cffunction>

<cffunction name="createWorkOrder" returntype="boolean" output="false" access="remote"
			description="this function is used to create work order in database">
			<cfargument name = "vehicleID" type = "string" required = "true">
			<cfargument name = "addressID" type = "string" required = "true">
			<cfargument name = "productIDArr" type = "string" required = "true">
			<cfargument name = "startTime" type = "string" required = "true">
			<cfargument name = "maxEmpCount" type = "string" required = "true">
			<cfset var isCommit=false />
			<cfset var productIDArray = listToArray(arguments.productIDArr,",")>
			<cftransaction>
			<cftry>
		        <cfquery name = "qCreateWorkOrder">
					INSERT INTO	[dbo].[workOrder] (vehicleID, workOrderStatusID, startTime, addressID)
					VALUES	(
								<cfqueryparam value = "#trim(arguments.vehicleID)#" CFSQLType = "CF_SQL_BIGINT" />,
								<cfqueryparam value = 1 CFSQLType = "CF_SQL_BIGINT" />,
								<cfqueryparam value = "#trim(arguments.startTime)#" CFSQLType = "CF_SQL_TIMESTAMP" />,
								<cfqueryparam value = "#trim(arguments.addressID)#" CFSQLType = "CF_SQL_BIGINT" />
							)
				</cfquery>

				<cfset var qWorkOrderID = application.dbQuery.getWorkOrderIDFromDb(arguments.vehicleID,arguments.startTime)>
				<cfset var workOrderIDVal = "#qWorkOrderID.workOrderID#">
				<cfloop from="1" to="#ArrayLen(productIDArray)#" index = "i">
					<cfquery name= "qProductWorkOrderInsert">
					INSERT INTO		[dbo].[productWorkOrder] (workOrderID, productID)
					VALUES	(
									<cfqueryparam value = "#workOrderIDVal#" CFSQLType = "CF_SQL_BIGINT" />,
									<cfqueryparam value = "#productIDArray[i]#" CFSQLType = "CF_SQL_BIGINT" />
							)
			       	</cfquery>
				</cfloop>
				<cfset var qUserID = getUserIDForWorkOrder()>
				<cfif qUserID.recordCount EQ 0>
					<cfset isCommit = false>
					<cftransaction action="rollback" />
					<cflog  text="error : No workers available">
					<cfreturn isCommit>
				</cfif>
				<cfloop from="1" to="#arguments.maxEmpCount#" index="i">
					<cflog text= "#qUserID.userID[i]#">
					<cfquery name= "qUserWorkOrderInsert">
					INSERT INTO		[dbo].[userWorkOrder] (userID, workOrderID)
					VALUES	(
									<cfqueryparam value = "#qUserID.userID[i]#" CFSQLType = "CF_SQL_BIGINT" />,
									<cfqueryparam value = "#workOrderIDVal#" CFSQLType = "CF_SQL_BIGINT" />
							)
		        	</cfquery>
				</cfloop>
			<cftransaction action="commit" />
			<cfset isCommit=true />
			<cfcatch type="database">
				<cftransaction action="rollback" />
				<cfset isCommit=false />
				<cflog  text="error : #cfcatch.detail#">
			</cfcatch>
			</cftry>
			</cftransaction>
			<cfreturn isCommit>
</cffunction>

<cffunction name = "getWorkOrderDetailsByUserIDFromDb" returntype = "query" output="false" access="remote">
	<cfargument name = "userID" required="true" type="string">
	<cfquery name = "qWorkOrderDetailsByUserIDFromDb">
			select 		wo.workOrderID, pd.productName, vd.vehicleCompany, vd.vehicleModel
			From		[user] as us
			left join 	vehicle as vh
			on			us.userID = vh.userID
			left join	vehicleDetail as vd
			on			vh.vehicleDetailsID = vd.vehicleDetailsID
			left join 	workOrder as wo
			on			vh.vehicleID = wo.vehicleID
			left join	[dbo].[productWorkOrder] as pwo
			on			wo.workOrderID = pwo.workOrderID
			left join	[dbo].[product] as pd
			on			pd.productID = pwo.productID
			where 		us.userID = <cfqueryparam value="#arguments.userID#" cfsqltype="CF_SQL_BIGINT" />
	</cfquery>
	<cfreturn qWorkOrderDetailsByUserIDFromDb>
</cffunction>

<cffunction name="getWorkOrderByIDFromDb" access="remote" output="false" returntype="query" >
	<cfargument name="workOrderID" type="string" required="true" />
	<cfquery name="qWorkOrderByIDFromDb">
			SELECT		usr.firstName, usr.lastName, wos.workOrderStatusValue, ad.addressLane1 , ad.addressLane2, ad.city, ad.state, ad.country, ad.pinCode
			FROM		[dbo].[workOrder] as wo
			LEFT JOIN	[dbo].[workOrderStatus] as wos
			ON			wo.workOrderStatusID = wos.workOrderStatusID
			LEFT JOIN	[dbo].[address] as ad
			ON			wo.addressID = ad.addressID
			LEFT JOIN	[dbo].[userWorkOrder] as uwo
			ON			wo.workOrderID = uwo.workOrderID
			LEFT JOIN	[dbo].[user] as usr
			ON			usr.userID = uwo.userID
			WHERE		wo.workOrderID = <cfqueryparam value="#arguments.workOrderID#" cfsqltype="CF_SQL_BIGINT" />
	</cfquery>
	<cfreturn qWorkOrderByIDFromDb />
</cffunction>

</cfcomponent>