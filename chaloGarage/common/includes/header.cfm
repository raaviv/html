<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/headerStyle.css">
</head>
<body>
<ul>
  <li class="chalogarage"><a href="index.cfm">chaloGarage</a></li>
	<cfif isDefined("session.sLogInUserDetails") AND StructKeyExists(session.sLogInUserDetails,"userID")>
		<li class="links" id="logout"><a href="#Logout">Logout</a></li>
		<li class="links" id="profile"><a href="../common/Profile.cfm"> View My profile</a></li>
		<li class="links" id="viewMyOrder"><a href="../common/viewMyOrders.cfm"> View My Orders</a></li>
		<div class="dropdown">
		<li class="links" id="vehicle"><a href="#"> My Vehicle </a></li>
		<div class="dropdownContent">
	      <a href="addVehicle.cfm">Add Vehicle</a>
	      <a href="editVehicle.cfm">Edit vehicle</a>
   		</div>
		</div>
	<cfelse>
		<li class="links"><a href="login.cfm">Login</a></li>
	</cfif>
  <li class="links"><a href="../common/bookService.cfm">Book a Service</a></li>
  <li class="links"><a href="../common/index.cfm">Home</a></li>
</ul>
</body>
</html>
