var res=0;
$(document).ready(function(){   //validate every field on click and generate captcha on load
    $(window).load(generateCaptcha);  
    $("#registerbutton").click(function(){
        var arr= [validatefirstname(),validatelastname(),validateemail(),validatepassword(),
            validatereenterpassword(),validategender(),validatephonenumber(),validatealtphonenumber(),
            validatecurrentaddress(),validatepermanentaddress(),validatestate(),validatecountry(),validatecaptcha()];
        for(var i=0; i<arr.length; i++)
        {
            console.log(arr[i]);
            if(arr[i]==false){return false;}
        }
    });
    $('#firstname').blur(validatefirstname) .focus(hide);   //event handling for blur and focus
    $("#lastname").blur(validatelastname).focus(hide);
    $("#email").blur(validateemail).focus(hide);
    $("#password").blur(validatepassword).focus(hide);
    $("#reenterpassword").blur(validatereenterpassword).focus(hide);
    $("#genderinnerdiv input").blur(validategender).focus(hide);
    $("#phonenumber").blur(validatephonenumber).focus(hide);
    $("#altphonenumber").blur(validatealtphonenumber).focus(hide);
    $("#currentaddress").blur(validatecurrentaddress).focus(hide);
    $("#permanentaddress").blur(validatepermanentaddress).focus(hide);
    $("#state").blur(validatestate).focus(hide);
    $("#country").blur(validatecountry).focus(hide);
    $(".refreshbutton").click(generateCaptcha);
    $("#result").blur(validatecaptcha).focus(hide);
});
function namehidealert(id)  //name field alert icon, border color and error message hide function
{
    $("#"+id+" .namealerticon").css("visibility","hidden");
    $("#"+id+" .similarclassforvalidation").css("border-color","rgb(204,204,204)");
    $("#"+id+" .nameerrormsg").fadeOut('slow', function(){$("#"+id+" .nameerrormsg").html("");});
}
function hidealert(id){     // fields (other than name and captcha) alert icon, border color and error message hide function
    $("#"+id+" .alerticon").css("visibility","hidden");
    $("#"+id+" .similarclassforvalidation").css("border-color","rgb(204,204,204)");
    $("#"+id+" .errormsg").fadeOut('slow', function(){$("#"+id+" .errormsg").html("");});
}
function captchahidealert(){   //captcha field alert icon, border color and error message hide function
    $(".captchaalerticon").css("visibility","hidden");
    $("#result").css("border-color","rgb(204,204,204)");
}
function hide()     //hide function 
{   
    if(this.name == "firstname" || this.name == "lastname"){namehidealert(this.name+"div");}
    else if(this.name == "captcharesult"){captchahidealert();}
    else {hidealert(this.name+"div");}
}
function repeatvalidate(temp1,temp2,temp3, msg, color, show,decide){
    temp1.css("visibility",show);
    temp2.css("border-color",color);
    temp3.fadeIn('fast', function(){temp3.html(msg);});
    return decide;
}
//validate function for required fields
function validaterequired(regex,content,id, alertclass, errorclass,contentlength){
    var temp2= $("#"+id+" ."+ "similarclassforvalidation"),temp1 =$("#"+id+" ."+ alertclass),temp3 =$("#"+id+" ."+ errorclass) ;
    if(contentlength>50){
        return repeatvalidate(temp1,temp2,temp3,"Input entered too long , maximum limit 50 characters" , "rgb(255,99,71)", "visible",false);
    }
    else if(content == "" || content == undefined){
        return repeatvalidate(temp1,temp2,temp3,"Value cannot be empty", "rgb(255,99,71)", "visible",false);
    }
    else if(content != "" && regex.test(content) == false){
        return repeatvalidate(temp1,temp2,temp3,"Entered value is invalid and is not in the required format", "rgb(255,99,71)", "visible",false);
    }
    else if(content != "" && regex.test(content) == true){
        return repeatvalidate(temp1,temp2,temp3,"", "rgb(204,204,204)", "hidden",true);
    } 
}
//validate function for non-required fields
function validatenonrequired(regex, content, id, alertclass, errorclass,contentlength){
    var temp2= $("#"+id+" ."+ "similarclassforvalidation"),temp1 =$("#"+id+" ."+ alertclass),temp3 =$("#"+id+" ."+ errorclass) ;
    if(contentlength>50){
        return repeatvalidate(temp1,temp2,temp3,"Input entered too long , maximum limit 50 characters" , "rgb(255,99,71)", "visible",false); 
    }
    else if(regex.test(content) == true || content == ""){
        return repeatvalidate(temp1,temp2,temp3,"", "rgb(204,204,204)", "hidden",true);
    }
    else if(regex.test(content) == false){
        return repeatvalidate(temp1,temp2,temp3,"Entered value is invalid and is not in the required format", "rgb(255,99,71)", "visible",false);
    }
}
function validatefirstname(){ //validate function for first name
    var firstnameregex = /^[a-z|A-Z]+[" "]?[a-z|A-Z]*$/;
    var content = $("#firstname").val().trim();
    return validaterequired(firstnameregex, content,"firstnamediv","namealerticon","nameerrormsg",content.length);
}
function validatelastname(){ //validate function for last name
    var lastnameregex = /^[a-z|A-Z]+$/;
    var content = $("#lastname").val().trim();
    return validaterequired(lastnameregex, content,"lastnamediv","namealerticon","nameerrormsg",content.length);
}
function validateemail(){   //validate function for email
    var emailregex = /^\w+([\.+-]?\w+)*@(\w)+([\.-]?\w+)*(\.\w{2,3})+$/;
    var content = $("#email").val().trim();
    return validaterequired(emailregex,content,"emaildiv","alerticon","errormsg",content.length);
}
function validatepassword(){   //validate function for password
    var passwordregex =/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,15}$/;
    var content = $("#password").val().trim();
    return validaterequired(passwordregex,content,"passworddiv","alerticon","errormsg",content.length);
}
function validatereenterpassword(){    //validate function for re-enter password
    var content = $("#password").val().trim();
    var content1 = $("#reenterpassword").val().trim();
    var temp1= $("#reenterpassworddiv .alerticon"), temp2 = $("#reenterpassworddiv .similarclassforvalidation"), temp3 = $("#reenterpassworddiv .errormsg");
    if(content1 == "")
    {
        return repeatvalidate(temp1,temp2,temp3,"Value cannot be empty", "rgb(255,99,71)", "visible",false);
    }
    else if(content != content1)
    {
        return repeatvalidate(temp1,temp2,temp3,"Re-entered-password does not match with the entered password", "rgb(255,99,71)", "visible",false);
    }
    else if(content == content1)
    {
        return repeatvalidate(temp1,temp2,temp3,"", "rgb(204,204,204)", "hidden",true);
    }
}
function validategender(){    //validate function for gender
    var content = $("form input[type=radio][name=gender]:checked").val();
    regex = /^[a-zA-Z]+$/;
    return validaterequired(regex, content, "genderdiv","alerticon","errormsg");
}
function validatephonenumber(){     //validate function for phone number
    var phoneregex = /^[2-9][0-9]{9}$/;
    content = $("form input[name=phonenumber]").val().trim();
    return validaterequired(phoneregex, content, "phonenumberdiv","alerticon","errormsg",content.length);
}
function validatealtphonenumber(){    //validate function for alt phone number
    var altphoneregex = /^[2-9][0-9]{9}$/;
    content = $("form input[name=altphonenumber]").val().trim();
    return validatenonrequired(altphoneregex, content, "altphonenumberdiv","alerticon","errormsg",content.length);
}
function validatecurrentaddress(){     //validate function for current address
    content = $("form textarea[name=currentaddress]").val().trim();
    var currentaddressregex =/^[a-zA-Z0-9\s,'/.-]*$/
    return validaterequired(currentaddressregex, content, "currentaddressdiv","alerticon","errormsg",content.length);
}
function validatepermanentaddress(){     //validate function for alternate address
    content = $("form textarea[name=permanentaddress]").val().trim();
    var permanentaddressregex =/^[a-zA-Z0-9\s,'/.-]*$/
    return validatenonrequired(permanentaddressregex, content, "permanentaddressdiv","alerticon","errormsg",content.length);
}
function validatestate(){     //validate function for state
    var content = $("form select[name=state] option:selected").val();
    var stateregex = /^[a-zA-Z]+$/
    return validaterequired(stateregex, content, "statediv","alerticon","errormsg",);
}
function validatecountry(){    //validate function for country
    var content = $("form select[name=country] option:selected").val();
    var countryregex = /^[a-zA-Z]+$/
    return validaterequired(countryregex, content, "countrydiv","alerticon","errormsg");
}
function captchaGenerator(){    //captcha generate
    var op = ['+', '-', '*', '/'];
    var opindex = Math.floor((Math.random() * 4));
    var operator = op[opindex];
    var first = Math.ceil((Math.random() * 10))+10;
    var second = Math.ceil((Math.random() * 5));
    if (opindex == 3) {
          while (second == 0 || first % second != 0) {
          first = Math.ceil((Math.random() * 10)+20);
          second = Math.ceil((Math.random() * 5));
          }
    }
    switch (opindex) {
       case 0:res = first + second;break;
       case 1:res = first - second;break;
       case 2:res = first * second;break;
       case 3:res = first / second;
    }
    var captchaString = " "+ first +" " + operator +" "+ second + " =  " ;
    var captchaValue = res;
    return {captchaString: captchaString, captchaValue: res};
 }
 function generateCanvas(captchaString){      //canvas generate function
    var canv = $(".captchasideheading")[0];
    canv.width = 250;
    canv.height = 100;
    var ctx = canv.getContext("2d");
    ctx.font = "60px Georgia";
    ctx.fillStyle = "black";
    ctx.fillText(captchaString, 30, 40);
 }
 function generateCaptcha(){    //generate captcha function
    var data = captchaGenerator();
    var captchaString = data.captchaString;
    var captchaValue = data.captchaValue;
    generateCanvas(captchaString);
}
function validatecaptcha(){     //captcha validate function
    var content = $("form input[name=captcharesult] ").val();
    if(content != res ){
        $(".captchaalerticon").css("visibility","visible");
        $("#result").css("border-color","rgb(255,99,71)");
        $(".captchadiv").animate({left: '20px'},75);
        $(".captchadiv").animate({left: '0px'},75);
        $(".captchadiv").animate({left: '20px'},75);
        $(".captchadiv").animate({left: '0px'},75);
        return false;
    }
    else if(content == res){
        $(".captchaalerticon").css("visibility","hidden");
        $("#result").css("border-color","rgb(204,204,204)");
        return true;
    }
}