
/*1. Total number number of employees*/
	use MyPractice2020
	select count(*) as Emp_count,min(moSalary) as min_salary,count(distinct ntLevel) as uniq_ntLevel from tblEmp;

/*2. Correct this query:*/
	use MyPractice2020
	SELECT E.[ntEmpID], E.[vcName],E.[vcMobieNumer] FROM tblEmp E;
	/* (or) */
	use MyPractice2020
	SELECT tblEmp.[ntEmpID], tblEmp.[vcName],tblEmp.[vcMobieNumer] FROM tblEmp;

/*3. Write a single select query which satisfies the following conditions: */
	use MyPractice2020
	select * from tblEmp where (vcMobieNumer is NULL and ntLevel = 1) or ntLevel = 0;


/*4.Write a sql query which displays those employee data first, who knows javascript. */
	use MyPractice2020
	select * from tblEmp
	where vcSkills like '%JavaScript%'
	union all select * from tblEmp
	where vcSkills not like '%JavaScript%'	

	/* or */

	select * from tblEmp
	order by(case
			when vcSkills like '%JavaScript%' then vcName
		end) desc;

/* 6. When I executed this query: */
	use MyPractice2020
	SELECT [vcName],min([vcMobieNumer]) FROM [dbo].[tblEmp] GROUP BY [vcName];
	/* or */
	SELECT vcName, vcMobieNumer FROM tblEmp GROUP BY vcName, vcMobieNumer


/*7. Write a sql query to get the ntLevel of the employees getting salary greater than average salary.*/
	use MyPractice2020
	select ntEmpID,vcName,moSalary,ntLevel from tblEmp
	where moSalary > (select avg(moSalary) from tblEmp);


/*8. Write a query to get the count of employees with a valid Suffix */
	use AdventureWorks2014
	select count(*) as [Suffixed Persons count ]
	from Person.Person
	where Suffix is not null;


/*9. Using BusinessEntityAddress table (and other tables as required), list the full name of people living in the city of Frankfurt.*/
	use AdventureWorks2014
	select FirstName+' '+ISNULL(MiddleName+' ','')+LastName as Fullname
	from Person.Person as PP
	inner join Person.BusinessEntityAddress as BEA
	on PP.BusinessEntityID = BEA.BusinessEntityID
	inner join Person.Address as PA
	on BEA.AddressID = PA.AddressID
	where City = 'Frankfurt';


/*10. "Single Item Order" is a customer order where only one item is ordered. Show the SalesOrderID and the UnitPrice for every Single Item Order.*/
	use AdventureWorks2014
	select SalesOrderID,UnitPrice from Sales.SalesOrderDetail
	where SalesOrderID in 
	(select SOD.SalesOrderID from Sales.SalesOrderDetail as SOD
	inner join sales.SalesOrderHeader as SOH
	on SOD.SalesOrderID = SOH.SalesOrderID
	group by CustomerID,SOD.SalesOrderID
	having count(ProductID)=1) and OrderQty=1


/*11. Show the product description for culture 'fr' for product with ProductID 736.*/
	use AdventureWorks2014
	select Description as [Product Description],CultureID
	from Production.ProductDescription as PD
	inner join Production.ProductModelProductDescriptionCulture as PMPDC
	on PD.ProductDescriptionID = PMPDC.ProductDescriptionID
	inner join Production.Product 
	on PMPDC.ProductModelID = Product.ProductModelID
	where PMPDC.CultureID='fr' and Product.ProductID = 736;


/*12. Show OrderQty, the Name and the ListPrice of the order made by CustomerID 635*/
	use AdventureWorks2014
	select OrderQty ,Name ,ListPrice from Sales.SalesOrderHeader  as SOH
	inner join Sales.SalesOrderDetail as SOD
	on SOH.SalesOrderID = SOD.SalesOrderID 
	inner join	Production.Product
	on SOD.ProductID = Product.ProductID
	where CustomerID = 635;


/*13. 13. How many products in ProductSubCategory 'Cranksets' have been sold to an address in 'London'?*/
	use AdventureWorks2014
	select sum(OrderQty) as [No of Products] 
	from Production.ProductSubCategory as PSC
	inner join	Production.Product
	on PSC.ProductSubcategoryID = Product.ProductSubcategoryID
	inner join	Sales.SalesOrderDetail as SOD
	on Product.ProductID = SOD.ProductID
	inner join	Sales.SalesOrderHeader as SOH
	on SOD.SalesOrderID = SOH.SalesorderID
	inner join	Person.Address
	on SOH.ShipToAddressID = Address.AddressID
	where Address.City = 'London' and PSC.Name = 'Cranksets';